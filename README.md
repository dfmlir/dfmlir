# Dataflow Dialect for MLIR


## Setting up

Run `new_setup.sh` from the parent directory, it will clone LLVM and Polygeist and generate `env.sh`

Make sure the Polygeist binary is generated.

Run `bash text/run_tests.sh` to run all tests. Some tests may have individual
library dependencies, run `bash -x test.sh` from each test directory
to check for errors.

On Ubuntu, you might need  to run `sudo apt install libsdl2-image-dev libsdl2-ttf-dev  libopencv-dev libjsoncpp-dev`.

## Don't forget to `source env.sh` before running tests.

## Source for video tests:

- sobel: https://github.com/preesm/preesm-apps
- lightness: https://github.com/ggauthie/stage_swimmerDetection
