
rm -f env.sh

cd ..

PROJECTS_DIR=$(pwd)

cd dfmlir

cat << EOF > env.sh
export DFMLIR_BIN="$PROJECTS_DIR/dfmlir/build/bin/dfmlir"
export POLYGEIST_MLIR_CLANG_BIN="$PROJECTS_DIR/Polygeist/build/bin/cgeist"
export LLVM_PROJECT="$PROJECTS_DIR/llvm-project"
export CLANG_BIN="\$LLVM_PROJECT/build/bin/clang-15"
export DFMLIR_DIR="$PROJECTS_DIR/dfmlir"
export PATH=\$PATH:\$LLVM_PROJECT/build/bin
export PS1="(dfmlir)\$PS1"
EOF
