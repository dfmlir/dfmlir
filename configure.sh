mkdir build
cd build
rm -rf *
cmake -GNinja .. -DCMAKE_BUILD_TYPE=Debug -DMLIR_DIR=$LLVM_PROJECT/build/lib/cmake/mlir -DLLVM_DIR=$LLVM_PROJECT/lib/cmake/llvm  -DLLVM_EXTERNAL_LIT=$LLVM_PROJECT/llvm/bin/llvm-lit -DCMAKE_EXPORT_COMPILE_COMMANDS=YES
cmake --build .
