#ifndef UNIONFIND_H
#define UNIONFIND_H

#include <cstddef>
#include <llvm/ADT/DenseMap.h>
#include <llvm/ADT/SmallVector.h>

namespace util {

namespace unionfind {

// UnionFind algorithm. Represents disjointed sets (groups) as a forest of
// trees, where the root of each set acts as a representative for all of its
// members.

// Each ID starts as the representative of its own group. Groups can be joined
// by calling join(), which will set the representative of the smallest set to
// the representative of the largest.

// On querying, the current representative is memoized.

// A map from void* to IDs is also provided.

class UnionFind;

using GroupID = size_t;

class UnionFind {

public:
  using GroupID = size_t;
  llvm::SmallVector<GroupID> reprs;
  llvm::SmallVector<size_t> sizes;
  llvm::DenseMap<void *, GroupID> parents;
  UnionFind(size_t size) {
    reprs.reserve(size);
    sizes.reserve(size);
    for (size_t i = 0; i < size; i++) {
      reprs.push_back(i);
      sizes.push_back(1);
    };
  };

  // Joins two groups. Running getGroup on any member will now return the ID
  // of the largest group.
  GroupID join(GroupID a, GroupID b) {
    GroupID p_a = getGroup(a);
    GroupID p_b = getGroup(b);
    if (p_a == p_b)
      return p_a;
    if (sizes[p_a] > sizes[p_b]) {
      reprs[p_b] = p_a;
      sizes[p_a] += sizes[p_b];
      return p_a;
    } else {
      reprs[p_b] = p_a;
      sizes[p_b] += sizes[p_a];
      return p_b;
    }
  }

  // If new_key has no group, add it to group id. Otherwise, join the current
  // group with id.
  void setOrJoin(void *new_key, GroupID id) {
    auto entry = parents.find(new_key);
    if (entry == parents.end()) {
      setGroup(new_key, id);
    } else {
      setGroup(new_key, join(entry->second, id));
    }
  }

  GroupID getGroup(void *key) {
    assert(parents.count(key) == 1);
    return parents[key] = getGroup(parents[key]);
  }

  GroupID getGroup(GroupID &id) {
    auto parent = reprs[id];
    if (parent == id)
      return id;
    return reprs[id] = getGroup(parent);
  }

  void setGroup(void *key, GroupID group) { parents[key] = getGroup(group); }

  bool hasGroup(void *key) { return parents.count(key) == 1; }
};

} // namespace unionfind

using UnionFind = unionfind::UnionFind;

} // namespace util

#endif