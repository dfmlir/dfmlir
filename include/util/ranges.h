#ifndef RANGES_H
#define RANGES_H

#include <llvm/ADT/STLExtras.h>
#include <type_traits>

namespace util {

template <class T>
using ValueOfRange =
    typename std::remove_reference<decltype(*std::declval<T>().begin())>::type;

template <class First, class... RangeTypes>
auto concat(First &&first, RangeTypes &&...ranges) {
  return llvm::concat<ValueOfRange<First>>(std::forward<First>(first),
                                           std::forward<RangeTypes>(ranges)...);
}

// Enables treating an Optional of a Range as a range.
// Useful for mlir::ArrayAttr, which returns as an Optional from TableGen.

template <class T,
          class U = typename std::enable_if<std::is_default_constructible<
              decltype(std::begin(std::declval<T>()))>::value>::type>
struct __optional_range__ {
  static inline auto begin(llvm::Optional<T> attr)
      -> decltype(std::begin(*attr)) {
    if (attr)
      return std::begin(*attr);
    else
      return {};
  }
  static inline auto end(llvm::Optional<T> attr) -> decltype(std::end(*attr)) {
    if (attr)
      return std::end(*attr);
    else
      return {};
  }
};

template <class T>
auto as_range(llvm::Optional<T> attr)
    -> decltype(llvm::make_range(attr->begin(), attr->end())) {
  if (attr) {
    return llvm::make_range(attr->begin(), attr->end());
  }
  return llvm::make_range(nullptr, nullptr);
}

} // namespace util

#endif