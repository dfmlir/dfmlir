#ifndef UTIL_H
#define UTIL_H

#include "mlir/IR/Builders.h"
#include "mlir/IR/BuiltinAttributes.h"
#include "mlir/IR/BuiltinDialect.h"
#include "mlir/IR/BuiltinTypes.h"
#include "mlir/IR/Diagnostics.h"
#include "mlir/IR/Location.h"
#include "mlir/IR/SymbolTable.h"
#include "mlir/Support/MathExtras.h"
#include "llvm/ADT/Optional.h"
#include "llvm/ADT/STLExtras.h"
#include "llvm/ADT/SmallSet.h"
#include "llvm/ADT/SmallVector.h"
#include "llvm/ADT/StringRef.h"
#include "llvm/ADT/iterator_range.h"
#include "llvm/Support/Error.h"
#include "llvm/Support/FileSystem.h"
#include "llvm/Support/FileUtilities.h"
#include "llvm/Support/FormatVariadic.h"
#include "llvm/Support/MathExtras.h"
#include "llvm/Support/Path.h"
#include <cstddef>
#include <filesystem>
#include <fstream>
#include <iterator>
#include <numeric>
#include <type_traits>
#include <utility>

/// Returns if error; else sets x to value held by y
#define UNWRAP(x, y)                                                           \
  {                                                                            \
    auto _y = (y);                                                             \
    if (!_y)                                                                   \
      return _y.takeError();                                                   \
    x = *_y;                                                                   \
  }

// Returns if error; else defines and sets x to value held by y

#define UNWRAP_DEF(x, y)                                                       \
  std::remove_reference_t<decltype((*(y)))> x;                                 \
  {                                                                            \
    auto _y = (y);                                                             \
    if (!_y)                                                                   \
      return _y.takeError();                                                   \
    x = *_y;                                                                   \
  }

namespace util {

inline mlir::LogicalResult error(mlir::InFlightDiagnostic) {
  return mlir::failure();
}

inline llvm::Expected<std::ifstream>
findFileCaseInsensitive(llvm::StringRef dirpath, llvm::StringRef filename) {
  std::error_code err;
  auto dir = llvm::sys::fs::directory_iterator(dirpath, err);
  if (err) {
    auto path = std::string(dirpath);
    return llvm::createStringError(err, "Directory not found: {0}",
                                   path.c_str());
  }
  llvm::sys::fs::directory_entry entry;
  for (llvm::sys::fs::directory_iterator file = dir, end; file != end && !err;
       file.increment(err)) {
    if (file->type() != llvm::sys::fs::file_type::regular_file)
      continue;
    auto entry_filename = llvm::sys::path::filename(file->path());
    auto path = file->path();
    if (filename.lower() == entry_filename.lower()) {
      return std::ifstream(path);
    }
  }

  auto separator = llvm::sys::path::get_separator();

  auto dirpath_with_sep = dirpath.str();
  if (!dirpath.endswith(separator)) {
    dirpath_with_sep += separator;
  }

  std::string path = llvm::formatv("{0}{1}", dirpath_with_sep, filename);
  return llvm::createStringError(std::errc::no_such_file_or_directory,
                                 "No such file: %s", path.c_str());
}

template <class A, class B> std::string join(A separator, B items) {
  auto first = items.begin();
  std::string rv{*first};
  ++first;
  for (auto i = first, end = items.end(); i != end; ++i) {
    rv += separator;
    rv += *i;
  }
  return rv;
}

inline mlir::IntegerAttr stringToIntegerAttr(llvm::StringRef string,
                                             mlir::OpBuilder &builder) {
  llvm::APInt value(64, string, 10);
  return mlir::IntegerAttr::get(builder.getI64Type(), value);
}

inline mlir::DictionaryAttr makeDictAttr(
    mlir::OpBuilder &builder,
    std::initializer_list<std::pair<const char *, mlir::Attribute>> list) {
  mlir::NamedAttrList attrs;
  for (auto pair : list) {
    auto attr = builder.getNamedAttr(pair.first, pair.second);
    attrs.push_back(attr);
  }
  return attrs.getDictionary(builder.getContext());
}

inline mlir::Attribute stringToAttr(llvm::StringRef string, mlir::Type type,
                                    mlir::OpBuilder &builder) {
  if (type.isa<mlir::FloatType>()) {
    double d;
    string.getAsDouble(d);
    return builder.getFloatAttr(type, d);
  }
  if (type.isa<mlir::IntegerType>()) {
    return mlir::IntegerAttr::get(
        type, llvm::APInt(type.getIntOrFloatBitWidth(), string, 10));
  }
  llvm::errs() << "Trying to convert string into attribute of unknown type.\n";
  exit(1);
  return nullptr;
}

// // Converts a pointer-like object into a void* for indexing purposes.
// template <class T> struct AsVoidPtr {};
// template <class T> struct AsVoidPtr<T *> {
//   using type = T *;
//   static void *convert(type &value) { return value; }
// };

// template <> struct AsVoidPtr<mlir::Value> {
//   using type = mlir::Value;
//   static const bool enabled = true;
//   static void *convert(mlir::Value &value) { return value.getImpl(); }
// };

}; // namespace util

#endif
