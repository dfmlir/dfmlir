#ifndef GRAPHUTILS_H
#define GRAPHUTILS_H

#include <algorithm>
#include <cassert>
#include <functional>
#include <set>
#include <string>
#include <type_traits>
#include <vector>

namespace graphutils {

class EdgePointer;
class NodePointer;

using EdgePred = std::function<void(EdgePointer &)>;
using NodePred = std::function<void(NodePointer &)>;

class EdgePointer {
public:
  virtual NodePointer *get_outgoing_node() = 0;
  virtual EdgePointer *get_edge_ptr() = 0;
  virtual ~EdgePointer(){};
};

// Provides inheritors with generic graph-traversal methods.
// Nodes are internally identified by their addresses, and so
// they should not be moved or deleted for the duration of these functions.
class NodePointer {
public:
  virtual ~NodePointer(){};

  // Executes the given predicate on each of the node's outgoing edges.
  virtual void
  for_each_outgoing_edge(std::function<void(EdgePointer &)> do_this) = 0;
  virtual NodePointer *get_node_ptr() = 0;

  // A single-line representation of this node.
  virtual std::string line_repr() = 0;

  virtual size_t get_unique_id() = 0;

  /**
   * @brief Depth-first graph traversal
   *
   *  Walks the graph starting on this node, in a depth-first manner,
   *  and executes given predicates on each edge and node reachable from it.
   *
   * @param on_node_enter Executes when a node is first discovered
   * @param on_node_exit Executes when all of the node's neighbors have
   finished
   *                     executing
   * @param on_edge Executes once for each edge.
   */
  void depth_first_traverse(NodePred on_node_enter, NodePred on_node_exit,
                            EdgePred on_edge) {

    using std::pair;

    std::vector<pair<EdgePointer *, NodePointer *>> stack;
    std::set<size_t> visited;
    auto current = get_node_ptr();
    do {
      auto id = current->get_unique_id();
      if (visited.find(id) == visited.end()) {
        visited.insert(id);
        on_node_enter(*current);

        // push current node to stack, to be called after every neighbor
        stack.emplace_back(nullptr, current->get_node_ptr());

        current->for_each_outgoing_edge([&stack](EdgePointer &edge) {
          auto new_ptr = edge.get_edge_ptr();
          stack.emplace_back(new_ptr, nullptr);
        });
      }

      while (!stack.empty()) {
        // if it's a node, close it
        if (stack.back().second) {
          on_node_exit(*stack.back().second);
          stack.pop_back();
        } else {
          break;
        }
      }

      if (stack.empty())
        break;

      auto next = stack.back();
      stack.pop_back();
      auto next_edge = next.first;
      on_edge(*next_edge);
      auto next_node = next_edge->get_outgoing_node();
      current = next_node;
    } while (true);
  }

  // Assuming that the graph is a rooted tree, prints it in indented form.
  auto tree_print() -> std::string {
    int indent = 0;
    std::string rv;
    this->depth_first_traverse(
        [&indent, &rv](NodePointer &node) {
          // Entering node
          for (int i = 0; i < indent; i++) {
            rv.append("  ");
          }
          rv.append(node.line_repr());
          rv.append("\n");
          indent++;
        },
        [&indent, &rv](NodePointer &node) {
          // Exiting node
          indent--;
        },
        [](EdgePointer &x) {});
    return rv;
  }
};

// Alternate implementation that doesn't require inheritance of the pointers
// themselves.

// NodeRef and EdgeRef must be cheap to copy, implement equality, and be
// pointer-like (can be compared and converted from nullptr)

template <class NodeRef, class EdgeRef> class Graph {

  struct NodeEdgePair {
    NodeRef node;
    EdgeRef edge;
    NodeEdgePair(NodeRef node) : node(node) {}
    NodeEdgePair(EdgeRef edge) : edge(edge) {}
    NodeEdgePair(NodeRef node, EdgeRef edge) : node(node), edge(edge) {}
  };

public:
  using EdgePred = std::function<void(NodeRef, EdgeRef)>;
  using NodePred = std::function<void(NodeRef)>;

  void runOnEachEdge(NodeRef node, EdgePred pred){};

  NodeRef followEdge(NodeRef node, EdgeRef edge) { return nullptr; };

  void depthFirstTraverse(NodeRef start_node, NodePred on_node_enter,
                          NodePred on_node_exit, EdgePred on_edge) {

    using std::pair;

    std::vector<NodeEdgePair> stack;
    std::set<NodeRef> visited;
    auto current = start_node;
    do {
      if (visited.find(current) == visited.end()) {
        visited.insert(current);
        on_node_enter(current);

        // push current node to stack, to be called after every neighbor
        stack.emplace_back(current);

        runOnEachEdge(current,
                      [&](EdgeRef edge) { stack.emplace_back(current, edge); });
      }

      // Close all finished nodes on top of stack
      while (!stack.empty() && stack.back().edge == nullptr) {
        on_node_exit(stack.back().node);
      }

      if (stack.empty())
        break;

      assert(stack.back().edge != nullptr);

      on_edge(stack.back().node, stack.back().edge);
      current = followEdge(stack.back().node, stack.back.edge());
      stack.pop_back();
    } while (true);
  }
};

} // namespace graphutils

#endif
