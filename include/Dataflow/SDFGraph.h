#ifndef SDFGRAPH_H
#define SDFGRAPH_H

#include "Dataflow/Dialect.h"
#include "mlir/Dialect/MemRef/IR/MemRef.h"
#include "util/graphutils.h"
#include "util/ranges.h"
#include "util/rational.h"

namespace mlir::df {

class SDFGraph : graphutils::Graph<NodeDeclOp, EdgeDeclOp> {

public:
  enum Direction { INCOMING, OUTGOING };

  struct EdgeInfo {
    Direction direction;
    llvm::StringRef this_port_name;
    llvm::StringRef other_port_name;
    EdgeDeclOp edge_op;
    NodeDeclOp this_node;
    NodeDeclOp other_node;
    int64_t this_rate;
    int64_t other_rate;
    bool is_unused_output = false;
  };

  struct NaiveBufferData {
    std::map<EdgeDeclOp, mlir::memref::GlobalOp> buffer_global_ops;
    std::map<EdgeDeclOp, mlir::MemRefType> buffer_memref_types;
    std::map<EdgeDeclOp, mlir::RankedTensorType> buffer_tensor_types;
    std::map<EdgeDeclOp, int64_t> queue_start;
    std::map<EdgeDeclOp, int64_t> queue_end;
    std::map<EdgeDeclOp, mlir::memref::GetGlobalOp> get_global_edge_ops;
    std::map<EdgeDeclOp, int64_t> current_fifo_sizes;
    std::map<EdgeDeclOp, int64_t> total_buffer_capacities;
  };
  // A node and a list of arguments for its function call.
  struct Firing {
    NodeDeclOp node;
    SmallVector<Value> args;
  };

private:
  ModuleOp module_op;
  GraphDeclOp graph_op;
  OpBuilder globals_builder;
  OpBuilder module_builder;
  FuncOp run_function;
  OpBuilder run_builder;

  SmallVector<NodeDeclOp> node_ops;
  SmallVector<EdgeDeclOp> edge_ops;
  std::map<ActortypeDeclOp, mlir::FuncOp> actor_impls;
  llvm::SmallVector<ActortypeDeclOp> actortype_ops;
  std::map<NodeDeclOp, util::Rational> activations;
  std::map<NodeDeclOp, llvm::SmallVector<EdgeInfo>> inputs, outputs;
  std::map<EdgeDeclOp, mlir::Type> edge_datatypes;
  std::map<NodeDeclOp, llvm::SmallVector<arith::ConstantOp>>
      parameter_arguments;
  std::map<std::pair<NodeDeclOp, StringRef>, SDFGraph::EdgeInfo> unused_data;
  DenseMap<PortAttr, mlir::memref::GlobalOp> scratch_global_ops;
  DenseMap<PortAttr, mlir::memref::GetGlobalOp> scratch_get_global_ops;

  bool is_meg_bufferizing = false;
  NaiveBufferData naive_buffer;

public:
  void runOnEachEdge(NodeDeclOp node, EdgePred pred) {
    for (EdgeInfo edge : llvm::concat<EdgeInfo>(inputs[node], outputs[node])) {
      pred(node, edge.edge_op);
    }
  };

  NodeDeclOp followEdge(NodeDeclOp node, EdgeDeclOp edge) {
    auto prod_node =
        llvm::dyn_cast<NodeDeclOp>(lookupSymbol(edge, edge.prod()));
    auto cons_node =
        llvm::dyn_cast<NodeDeclOp>(lookupSymbol(edge, edge.cons()));
    if (prod_node == node)
      return cons_node;
    if (cons_node == node)
      return prod_node;
    llvm_unreachable("Edge is not connected to node");
    return nullptr;
  };

  SmallVector<EdgeDeclOp> getEdges(NodeDeclOp node) {
    SmallVector<EdgeDeclOp> rv;
    for (auto edge : util::concat(inputs[node], outputs[node])) {
      rv.push_back(edge.edge_op);
    }
    return rv;
  }

  SDFGraph(GraphDeclOp graph);

  LogicalResult precomputeParameters();

  SmallVector<Value> getParameterArguments(NodeDeclOp node_op);

  // Check if graph is admissible: every port has a constant rate, and the rates
  // are consistent in the case of cycles.
  // TODO: implement delays.
  bool isAdmissible();

  // Calculate buffer sizes for the naive bufferization. Since each firing has
  // its own memory, we will use the number of activations of the producer times
  // the production rate.
  void calculateNaiveBufferSizes();

  // Generate scratch space for unused outputs. Creates GlobalOps and
  // GetGlobalOps.
  void bufferizeUnusedOutputs();

  // Naive bufferization: every firing in a cycle has its own output memory.
  // This function generates the GlobalOps and GetGlobalOps for the buffers.
  LogicalResult naiveBufferize();

  // Get firing arguments for naive bufferization. Each time this is called,
  // the FIFO offsets for the relevant edges are internally updated.
  // This function generates the MemRefOps that will be used by the actor
  // firings.
  SmallVector<Value> getNaiveMemoryArguments(SmallVector<EdgeInfo> &inputs,
                                             SmallVector<EdgeInfo> &outputs);
  // MEG bufferization: Bufferize by reusing memory of past firings. Uses
  // algorithm from Karol Desnos's PHD thesis
  // (https://www.theses.fr/2014ISAR0004, chapter 4),
  // modified to allow non-single-rate SDF graphs.
  LogicalResult megBufferize();

  // Naive schedule: repeatedly find an actor that can execute and update the
  // sizes of simulated fifos. Remove from consideration once it has executed
  // enough times.
  // This function generates the appropriate MemRefs for each firing.
  // Returns the list of nodes to be fired and their function arguments, in
  // order, or None on error.
  Optional<std::vector<Firing>> naiveSchedule();

  // Generates function calls for the actor firings.
  void generateFirings(std::vector<Firing> &schedule);
};

} // namespace mlir::df
#endif