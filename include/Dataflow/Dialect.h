#ifndef DATAFLOW_DIALECT_H
#define DATAFLOW_DIALECT_H

#include "mlir/Dialect/Func/IR/FuncOps.h"
#include "mlir/IR/Builders.h"
#include "mlir/IR/BuiltinOps.h"
#include "mlir/IR/BuiltinTypes.h"
#include "mlir/IR/Dialect.h"
#include "mlir/IR/Types.h"
#include "mlir/Interfaces/CastInterfaces.h"
#include "mlir/Interfaces/SideEffectInterfaces.h"
#include "llvm/ADT/Any.h"
#include "llvm/ADT/TypeSwitch.h"

#include "Dataflow/DataflowDialect.h.inc"

#define GET_TYPEDEF_CLASSES
#include "Dataflow/DataflowTypes.h.inc"

#define GET_ATTRDEF_CLASSES
#include "Dataflow/DataflowAttrs.h.inc"

#define GET_OP_CLASSES
#include "Dataflow/DataflowOps.h.inc"

namespace iara {

using mlir::ArrayAttr;
using mlir::Attribute;
using mlir::Operation;
using mlir::StringRef;
using mlir::SymbolTable;
using mlir::func::FuncOp;

// Finds the operation pointed to by the given symbol on the closest scope that
// encompasses op.
template <class T> inline Operation *lookupSymbol(Operation *op, T symb) {
  if (!op)
    return nullptr;
  auto nearestSymbolTable = SymbolTable::getNearestSymbolTable(op);
  if (!nearestSymbolTable)
    return nullptr;
  auto nearestOp = SymbolTable::lookupSymbolIn(nearestSymbolTable, symb);
  if (nearestOp)
    return nearestOp;
  return lookupSymbol(op->getParentOp(), symb);
}

// Finds a value definition for the given parameter, either on the node,
// actortype or graph declaration. Returns an IntegerAttr if successful, a
// TypeAttr if only the type was found, and nullptr if unsuccessful.
// If

Attribute resolveParameter(NodeOp node, ParameterAttr param_decl);

// Gets the last name in a string of `::` separated names.
llvm::StringRef getNamespaceSuffix(StringRef name);

// Returns the PortAttr of the given name in the node, or null.
llvm::Optional<PortAttr> getPortAttr(NodeOp node, StringRef port_name);

// Gets the rate of a PortAttr.
int64_t getRateOfPort(PortAttr port);

int64_t getRateOfPort(NodeOp node, StringRef port_name);

size_t getTokenSize(mlir::Type type);

// Returns the ParameterAttr in a given ArrayAttr, or null.
ParameterAttr findParamInArray(llvm::Optional<ArrayAttr> array, StringRef name);

inline bool isSimpleBroadcast(mlir::SymbolRefAttr symb) {
  return symb.isa<mlir::FlatSymbolRefAttr>() &&
         symb.getLeafReference() == "broadcast";
}

inline bool isComplexBroadcast(KernelOp kernel) {
  return kernel.getName().endswith("_BROADCAST");
}

// Gets incoming edges, in IR order.
// TODO: get edges in kernel order if there is a kernel
inline mlir::SmallVector<std::pair<mlir::StringRef, iara::EdgeOp>>
getIncomingEdges(iara::NodeOp node_op) {
  mlir::SmallVector<std::pair<mlir::StringRef, iara::EdgeOp>> rv;

  iara::GraphOp graph_op = llvm::cast<iara::GraphOp>(node_op->getParentOp());

  for (auto &op : graph_op) {
    if (auto edge_op = llvm::dyn_cast<iara::EdgeOp>(op)) {
      if (lookupSymbol(edge_op, edge_op.cons()) == node_op) {
        rv.push_back({edge_op.cons_port(), edge_op});
      }
    }
  }

  return rv;
}

PortAttr getSimpleBroadcastPortAttr(NodeOp node_op);

mlir::Type getTokenElementType(mlir::Type token_type);

size_t getTokenElementSize(mlir::Type token_type);

size_t getChunkSize(mlir::Type token_type, int64_t rate);

size_t getNumElementsInToken(mlir::Type token_type);

std::pair<FuncOp, mlir::OpBuilder>
createEmptyFunc(KernelOp kernel, StringRef name, mlir::OpBuilder &opbuilder);

} // namespace iara

#endif // DATAFLOW_DIALECT_H
