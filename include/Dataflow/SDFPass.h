#ifndef SDFGRAPH_H
#define SDFGRAPH_H

#include "Dataflow/Dialect.h"
#include "mlir/Dialect/Arithmetic/IR/Arithmetic.h"
#include "mlir/Dialect/Func/IR/FuncOps.h"
#include "mlir/Dialect/MemRef/IR/MemRef.h"
#include "util/graphutils.h"
#include "util/ranges.h"
#include "util/rational.h"
#include "util/util.h"

using mlir::func::FuncOp;

namespace iara {

using namespace mlir;
LogicalResult sdfPass(GraphOp graph_op);
} // namespace iara
#endif