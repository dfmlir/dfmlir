#include "Dataflow/Dialect.h"
#include "Dataflow/SDFPass.h"
#include "mlir/Dialect/Arithmetic/IR/Arithmetic.h"
#include "mlir/Dialect/ControlFlow/IR/ControlFlow.h"
#include "mlir/Dialect/Func/IR/FuncOps.h"
#include "mlir/Dialect/LLVMIR/LLVMDialect.h"
#include "mlir/Dialect/Math/IR/Math.h"
#include "mlir/Dialect/MemRef/IR/MemRef.h"
#include "mlir/Dialect/SCF/SCF.h"
#include "mlir/IR/BuiltinDialect.h"
#include "mlir/IR/BuiltinOps.h"
#include "mlir/Pass/Pass.h"

namespace iara {
using namespace mlir;

LogicalResult flattenTopLevelGraph(GraphOp graph_op);

class SDFSchedulePass
    : public mlir::PassWrapper<SDFSchedulePass,
                               mlir::OperationPass<mlir::ModuleOp>> {

public:
  enum BufferizationStrategy { POOL };

  Option<BufferizationStrategy> bufferization{
      *this, "bufferization_strategy",
      llvm::cl::desc("Bufferization strategy."
                     "POOL uses a memory pool to reuse memory after tokens are "
                     "consumed.")};

  SDFSchedulePass(
      BufferizationStrategy buf_strat = BufferizationStrategy::POOL) {
    this->bufferization = buf_strat;
  };

  SDFSchedulePass(SDFSchedulePass const &cloned) {
    this->bufferization = cloned.bufferization;
  }

  void getDependentDialects(DialectRegistry &registry) const override {
    registry.insert<iara::DataflowDialect, memref::MemRefDialect,
                    mlir::func::FuncDialect, arith::ArithmeticDialect,
                    func::FuncDialect, LLVM::LLVMDialect,
                    mlir::math::MathDialect>();
  }
  void runOnOperation() final;
};

class SDFScheduleToLLVMLoweringPass
    : public mlir::PassWrapper<SDFSchedulePass, mlir::OperationPass<ModuleOp>> {
  void getDependentDialects(DialectRegistry &registry) const override {
    registry.insert<mlir::BuiltinDialect, iara::DataflowDialect,
                    arith::ArithmeticDialect, memref::MemRefDialect,
                    func::FuncDialect, mlir::cf::ControlFlowDialect,
                    scf::SCFDialect, LLVM::LLVMDialect, mlir::func::FuncDialect,
                    math::MathDialect>();
  }
  void runOnOperation() final;
};

} // namespace iara
