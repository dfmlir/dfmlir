#include "mlir/IR/BuiltinOps.h"
#include "mlir/Support/LogicalResult.h"

// Finds C files for each actortype in the module, generates
// MLIR code for them, renames globals to avoid collisions
// and inserts all of them into a new module
mlir::ModuleOp getCSourceMLIR(mlir::ModuleOp graph_module);

// Merges two modules, replacing function declarations in schedule_graph with
// functions initialized in
mlir::LogicalResult replaceImplDeclsWithDefs(mlir::ModuleOp scheduled_graph,
                                             mlir::ModuleOp c_sources);
