#ifndef DIF_MLIRGEN_H
#define DIF_MLIRGEN_H

#include "DIF/dif.h"
#include "mlir/IR/BuiltinOps.h"
#include "mlir/IR/BuiltinTypes.h"
#include "mlir/IR/OwningOpRef.h"
#include "llvm/ADT/StringRef.h"
#include <memory>

namespace dif {
class MLIRGen;

/// Emit IR for the given DIF graph, returns a newly created MLIR module
/// or nullptr on failure.
mlir::FailureOr<mlir::ModuleOp> mlirGen(mlir::MLIRContext &context,
                                        std::vector<dif::Graph> &ast,
                                        llvm::StringRef working_dir);
} // namespace dif

#endif // DIF_MLIRGEN_H
