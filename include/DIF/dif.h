#ifndef DIF_H
#define DIF_H

#include "util/graphutils.h"
#include "llvm/ADT/StringRef.h"
#include "llvm/Support/CommandLine.h"
#include "llvm/Support/Error.h"
#include "llvm/Support/FormatVariadic.h"
#include "llvm/Support/MemoryBuffer.h"
#include "llvm/Support/raw_ostream.h"
#include <algorithm>
#include <map>
#include <memory>
#include <sstream>
#include <string>
#include <vector>

namespace dif {

// Forward decls
class Lexer;
class Item;
class CodeLocation;

/// @brief Contains filename, line and column information.
class CodeLocation {
public:
  std::shared_ptr<std::string> m_filename;
  int m_line;
  int m_column;

  CodeLocation() : m_filename(nullptr), m_line(0), m_column(0) {}

  CodeLocation(std::shared_ptr<std::string> filename, int line, int column)
      : m_filename(filename), m_line(line), m_column(column) {}

  std::string str() const {
    auto ss = std::ostringstream();
    ss << *m_filename << ":" << m_line << ":" << m_column;
    return ss.str();
  }
};

/// @brief Prints a code location, error message, and exits.
///
/// @param location
/// @param message

inline void fail [[noreturn]] (CodeLocation location, std::string message) {
  llvm::errs() << location.str() << "\t" << message << "\n";
  exit(1);
}

inline void fail [[noreturn]] (std::string message) {
  llvm::errs() << message << "\n";
  exit(1);
}

//
// Tables
//

// Using the X Macro technique to avoid duplication.

#define ENUM_TABLE                                                             \
  ITEM(None)                                                                   \
  /* Blocks */ ITEM(BlocknamesBegin)                                           \
  ITEM(basedon)                                                                \
  ITEM(topology)                                                               \
  ITEM(interface)                                                              \
  ITEM(parameter)                                                              \
  ITEM(datatype)                                                               \
  ITEM(refinement)                                                             \
  ITEM(attribute)                                                              \
  ITEM(msa)                                                                    \
  ITEM(actortype)                                                              \
  ITEM(actor)                                                                  \
  ITEM(delay)                                                                  \
  ITEM(BlocknamesEnd)                                                          \
  /* Keywords */ ITEM(KeywordsBegin)                                           \
  ITEM(codegen)                                                                \
  ITEM(type)                                                                   \
  /* ITEM(input) */                                                            \
  /* ITEM(output) */                                                           \
  ITEM(chain)                                                                  \
  ITEM(ring)                                                                   \
  /* ITEM(broadcast)     */                                                    \
  /* ITEM(merge)     */                                                        \
  ITEM(butterfly)                                                              \
  ITEM(multiedge)                                                              \
  ITEM(graph)                                                                  \
  ITEM(inputs)                                                                 \
  ITEM(outputs)                                                                \
  ITEM(nodes)                                                                  \
  ITEM(edges)                                                                  \
  ITEM(param)                                                                  \
  ITEM(mode)                                                                   \
  ITEM(production)                                                             \
  ITEM(consumption)                                                            \
  ITEM(KeywordsEnd)                                                            \
  /* Punctuation */ ITEM(PunctuationBegin)                                     \
  CHAR(Comma, ',')                                                             \
  CHAR(Colon, ':')                                                             \
  CHAR(Semicolon, ';')                                                         \
  CHAR(Dot, '.')                                                               \
  CHAR(BraceOpen, '{')                                                         \
  CHAR(BraceClose, '}')                                                        \
  CHAR(ParenOpen, '(')                                                         \
  CHAR(ParenClose, ')')                                                        \
  CHAR(BrackOpen, '[')                                                         \
  CHAR(BrackClose, ']')                                                        \
  CHAR(Assign, '=')                                                            \
  CHAR(GreaterThan, '>')                                                       \
  CHAR(LessThan, '<')                                                          \
  CHAR(Bang, '!')                                                              \
  CHAR(Tilde, '~')                                                             \
  CHAR(Question, '?')                                                          \
  CHAR(Add, '+')                                                               \
  CHAR(Subtract, '-')                                                          \
  CHAR(Multiply, '*')                                                          \
  CHAR(Divide, '/')                                                            \
  CHAR(BitwiseAnd, '&')                                                        \
  CHAR(BitwiseOr, '|')                                                         \
  CHAR(Caret, '^')                                                             \
  CHAR(Modulus, '%')                                                           \
  CHAR(At, '@')                                                                \
  STRING(NotEqual, "!=")                                                       \
  STRING(And, "&&")                                                            \
  STRING(Or, "||")                                                             \
  STRING(Increment, "++")                                                      \
  STRING(Decrement, "--")                                                      \
  STRING(GreaterThanOrEqual, ">=")                                             \
  STRING(LessThanOrEqual, "<=")                                                \
  STRING(Equal, "==")                                                          \
  STRING(DoubleColon, "::")                                                    \
  STRING(Arrow, "->")                                                          \
  STRING(AddAssign, "+=")                                                      \
  STRING(SubAssign, "-=")                                                      \
  STRING(MulAssign, "*=")                                                      \
  STRING(DivAssign, "/=")                                                      \
  STRING(AndAssign, "&=")                                                      \
  STRING(OrAssign, "!=")                                                       \
  STRING(XorAssign, "^=")                                                      \
  STRING(ModAssign, "%=")                                                      \
  STRING(ShiftLeftAssign, "<<=")                                               \
  STRING(ShiftRightAssign, ">>=")                                              \
  STRING(URShiftAssign, ">>>=")                                                \
  STRING(Ellipsis, "...")                                                      \
  ITEM(PunctuationEnd)                                                         \
  ITEM(Identifier)                                                             \
  ITEM(LiteralsBegin)                                                          \
  ITEM(Integer)                                                                \
  ITEM(Float)                                                                  \
  ITEM(String)                                                                 \
  ITEM(true)                                                                   \
  ITEM(false)                                                                  \
  ITEM(Char)                                                                   \
  ITEM(null)                                                                   \
  ITEM(LiteralsEnd)                                                            \
  ITEM(Eof) /* Must be last item of the enum */

// clang-format on

#define ITEM(x) Tok_##x,
#define CHAR(x, y) Tok_##x,
#define STRING(x, y) Tok_##x,
enum class TokenKind { ENUM_TABLE };
#undef ITEM
#undef CHAR
#undef STRING

template <TokenKind kind> struct TokenData {};
template <char c> struct TokenCharData {};

#define ITEM(x)                                                                \
  template <> struct TokenData<TokenKind::Tok_##x> {                           \
    static constexpr char const *name = #x;                                    \
    static const TokenKind kind = TokenKind::Tok_##x;                          \
  };
#define CHAR(x, y)                                                             \
  template <> struct TokenData<TokenKind::Tok_##x> {                           \
    static constexpr char const *name = #x;                                    \
    static const char _char = y;                                               \
    static const TokenKind kind = TokenKind::Tok_##x;                          \
  };                                                                           \
  template <> struct TokenCharData<y> {                                        \
    using Type = TokenData<TokenKind::Tok_##x>;                                \
  };
#define STRING(x, y)                                                           \
  template <> struct TokenData<TokenKind::Tok_##x> {                           \
    static constexpr char const *name = #x;                                    \
    static constexpr char const *_string = y;                                  \
    static const TokenKind kind = TokenKind::Tok_##x;                          \
  };

ENUM_TABLE
#undef ITEM
#undef CHAR
#undef STRING

// Contains lookup tables to convert single-char tokens
// to their enums and vice-versa.
// Access it through charToToken, stringToToken, tokenToChar and
// getTokenName functions.

struct TokenTables {
  char m_tokenToChar[256] = {};
  TokenKind m_charToToken[256] = {};
  llvm::StringRef m_tokenNames[(std::size_t)(TokenKind::Tok_Eof) + 1];
  llvm::StringRef m_tokenStrings[(std::size_t)(TokenKind::Tok_Eof) + 1];
  std::vector<std::pair<TokenKind, llvm::StringRef>> m_strings;
  std::vector<std::pair<TokenKind, char>> m_chars;
  TokenTables() {

    // Single-char tokens
#define ITEM(x)
#define CHAR(name, char)                                                       \
  m_charToToken[static_cast<std::size_t>(char)] = TokenKind::Tok_##name;       \
  m_tokenToChar[static_cast<std::size_t>(TokenKind::Tok_##name)] = char;       \
  m_chars.push_back({TokenKind::Tok_##name, char});
#define STRING(name, string)                                                   \
  m_strings.push_back({TokenKind::Tok_##name, string});
    ENUM_TABLE
#undef ITEM
#undef CHAR
#undef STRING

    // Keyword tokens
#define ITEM(name)                                                             \
  m_tokenNames[static_cast<std::size_t>(TokenKind::Tok_##name)] = #name;
#define CHAR(name, char) ITEM(name)
#define STRING(name, string)                                                   \
  ITEM(name)                                                                   \
  m_tokenStrings[static_cast<std::size_t>(TokenKind::Tok_##name)] = string;
    ENUM_TABLE
#undef ITEM
#undef CHAR
#undef STRING

    // Order long operators by decreasing length
    std::sort(m_strings.begin(), m_strings.end(),
              [](const auto &a, const auto &b) {
                return a.second.size() > b.second.size();
              });
  };
};

extern TokenTables g_tables;

TokenKind charToToken(char c);
TokenKind stringToToken(llvm::StringRef s);
char tokenToChar(TokenKind kind);
llvm::StringRef getTokenName(TokenKind kind);

//
// Token
//

/// Contains the token's kind, its code location and, for relevant kinds, a
/// StringRef to its original content
class Token {
public:
  using Kind = TokenKind;
  Kind m_kind;
  CodeLocation m_location;
  llvm::StringRef m_text;

  Token() : m_kind(Kind::Tok_None), m_location(), m_text(llvm::StringRef()) {}
  Token(Kind kind, CodeLocation location)
      : m_kind(kind), m_location(location), m_text(llvm::StringRef()) {}
  Token(Kind kind, CodeLocation location, llvm::StringRef text)
      : m_kind(kind), m_location(location), m_text(text) {}

  bool operator==(Token &rhs) const {
    return m_kind == rhs.m_kind && m_text == rhs.m_text;
  }

  operator bool() const { return m_kind != Kind::Tok_None; }

  /// Returns a short representation of the token.
  std::string str() const;

  /// Returns true if the token is a non-blockname keyword.
  bool isKeyword() const;

  /// Returns true if the token is punctuation (`+`, `-`, `,` etc).
  bool isPunctuation() const;

  /// Returns true if the token is a block header keyword.
  bool isBlockname() const;

  /// Returns true if the token is a literal.
  bool isLiteral() const;
};

/// @brief Basic AST node. Contains space for 2 tokens and a list of children.
///
/// Provides graph traversal utilities.

struct Item : public virtual graphutils::EdgePointer, graphutils::NodePointer {
public:
  Token m_name;
  Token m_type;
  std::vector<Item> m_children;

  Item(){};
  Item(Token token) noexcept : m_name(token){};
  Item(Token name, Token type) noexcept : m_name(name), m_type(type){};
  Item(Token name, Token type, std::vector<Item> &&children) noexcept
      : m_name(name), m_type(type), m_children(children){};
  Item(Token name, std::vector<Item> &&children) noexcept
      : m_name(name), m_children(children){};

  /// Returns first child with matching kind, or nullptr
  Item const *findChildWithType(TokenKind kind) const {
    for (auto &child : m_children) {
      if (child.m_type.m_kind == kind) {
        return &child;
      }
    }
    return nullptr;
  }

  CodeLocation getLocation() const;

  std::string printTree();
  llvm::APInt toAPInt() const;

  // graphutils impl

  virtual NodePointer *get_outgoing_node() override;
  virtual EdgePointer *get_edge_ptr() override;
  virtual void
  for_each_outgoing_edge(std::function<void(EdgePointer &)> do_this) override;
  virtual NodePointer *get_node_ptr() override;
  virtual std::string line_repr() override;
  virtual size_t get_unique_id() override;
};

// EdgeAST

struct EdgeAST : public Item {
  EdgeAST() { m_children = {Item()}; }
  Token const &name() const { return m_name; }
  Token const &producer() const { return m_children[0].m_name; }
  Token const &consumer() const { return m_children[0].m_type; }
};

// Blocks

struct BlockAST : public Item {
  TokenKind type() { return m_type.m_kind; }
  static bool classof(Item const *item) {
    return item->m_type.m_kind > TokenKind::Tok_BlocknamesBegin &&
           item->m_type.m_kind < TokenKind::Tok_BlocknamesEnd;
  }
};

template <TokenKind kind> struct Block : public BlockAST {
public:
  static bool classof(Item const *item) { return item->m_type.m_kind == kind; }
};

using BasedonBlock = Block<TokenKind::Tok_basedon>;
using TopologyBlock = Block<TokenKind::Tok_topology>;
using InterfaceBlock = Block<TokenKind::Tok_interface>;
using DatatypeBlock = Block<TokenKind::Tok_datatype>;
using ParameterBlock = Block<TokenKind::Tok_parameter>;
using RefinementBlock = Block<TokenKind::Tok_refinement>;
using AttributeBlock = Block<TokenKind::Tok_attribute>;
using ModelAttrBlock = Block<TokenKind::Tok_msa>;
using ActorTypeBlock = Block<TokenKind::Tok_actortype>;
using ActorBlock = Block<TokenKind::Tok_actor>;
using DelayBlock = Block<TokenKind::Tok_delay>;
using Graph = Block<TokenKind::Tok_graph>;

//
// Lexer
//

template <class... T> struct list_consumer {};

/// Provides tokens one at a time, allowing LL(1) lookahead
class Lexer {
private:
  using Kind = TokenKind;

  std::shared_ptr<llvm::MemoryBuffer> m_bufferPtr;

  llvm::StringRef m_bufferSlice;
  CodeLocation m_location;
  llvm::StringRef m_tokenText;

  char m_lastChar;

  void skipWhitespaceAndComments();

  Kind readKeywordOrIdentifier();

  TokenKind readNumber();

  TokenKind readString();

  TokenKind readSymbol();

  /// Consumes and returns a character from the buffer, preparing a new one and
  /// keeping track of row and column information.
  int getNextChar();

public:
  enum class ListLastSeparatorBehavior { REQUIRED, OPTIONAL, FORBIDDEN };

  struct AnyToken {
  } ANY;

  Token m_currentToken;

  /// @param buffer Text buffer. Will be StringRef'd by the tokens, and
  /// therefore should be kept alive until the end of code generation.
  /// @param filename Will be held as a shared_ptr by the CodeLocation objects.

  Lexer(std::shared_ptr<llvm::MemoryBuffer> buffer, std::string filename)
      : m_bufferPtr(buffer), m_bufferSlice(buffer->getBuffer()),
        m_location(CodeLocation(std::make_shared<std::string>(filename), 1, 0)),
        m_lastChar(getNextChar()), m_currentToken(getToken()) {}

  // Creates next token from buffer.
  Token getToken();

  /// Consumes and returns the next token, readying a new one.
  Token consumeAny() {
    Token rv(m_currentToken);
    m_currentToken = getToken();
    return rv;
  }

  Token consume() { return consumeAny(); }

  /// Runs lexer.consume() on each argument, returning the last one.
  template <class... T> Token consume(T... t) {
    return list_consumer<T...>::consume(*this, t...);
  };

  Token consumeLiteral();

  /// @brief Matches a list with given separator and terminator.
  ///
  /// @param doThis the predicate to execute for each item
  /// @param separator expected token kind for the separator
  /// @param terminator expected token that marks the end of the list
  /// @param lastSeparator allow, forbid or require an extra separator before
  /// terminator
  ///
  /// @return number of times that the predicate was executed

  int readList(std::function<void()> doThis, TokenKind separator,
               TokenKind terminator,
               ListLastSeparatorBehavior lastSeparator =
                   ListLastSeparatorBehavior::FORBIDDEN);
  int readList(std::function<void()> doThis, char separator, char terminator,
               ListLastSeparatorBehavior lastSeparator =
                   ListLastSeparatorBehavior::FORBIDDEN);

  Item readExpression();

  Item readAssignment();

  std::vector<Graph> readDIF();

  void dumpTokens();
};

template <class T> struct list_consumer<T> {
  static Token consume(Lexer &lexer, T t) { return lexer.consume(t); }
};
template <class A, class... Tail> struct list_consumer<A, Tail...> {
  static Token consume(Lexer &lexer, A a, Tail... t) {
    lexer.consume(a);
    return list_consumer<Tail...>::consume(lexer, t...);
  }
};
template <> struct list_consumer<char> {
  static Token consume(Lexer &lexer, char c) {
    return lexer.consume(charToToken(c));
  }
};

template <> struct list_consumer<TokenKind> {
  static Token consume(Lexer &lexer, TokenKind kind) {
    if (lexer.m_currentToken.m_kind == kind) {
      return lexer.consumeAny();
    }
    fail(lexer.m_currentToken.m_location,
         llvm::formatv("Expected `{0}`, received `{1}`", getTokenName(kind),
                       getTokenName(lexer.m_currentToken.m_kind)));
    return Token();
  }
};

template <> struct list_consumer<Lexer::AnyToken> {
  static Token consume(Lexer &lexer, Lexer::AnyToken any_marker) {
    return lexer.consumeAny();
  }
};

template <class Item> extern Item readItem(Lexer &lexer);

//
// AST classes
//

} // namespace dif

#endif