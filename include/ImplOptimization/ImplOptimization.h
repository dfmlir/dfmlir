#include "Dataflow/Dialect.h"
#include "mlir/IR/BuiltinOps.h"
#include "mlir/IR/Location.h"
#include "mlir/Support/LogicalResult.h"
namespace iara {

using namespace mlir;

// Attempts to find unrelated flows inside of the implementation (outputs whose
// input dependencies don't overlap). If successful, replaces the actortype with
// a subgraph that contains the split flows.
// Takes the op to be split apart and a module that contains all
// actor implementations. Returns the amount of unrelated flows found in the
// function, or -1 if error.
LogicalResult splitImpls(ModuleOp actortype_op, ModuleOp implementations);

// Attempts to transform static parameters of all actors into constants.
// This causes a copy of the implementation to be generated for each used
// combination of parameter values.
LogicalResult bakeParameters(ModuleOp topology, ModuleOp impls);

// Remove actortypes that are not referenced by nodes in the topology. If a
// symbol of the same name is found in impls, remove it as well. Returns the
// number of removed actortypes.
size_t removeUnusedActortypes(ModuleOp topology, ModuleOp impls);

// Removes operations that lead to unused output ports.
LogicalResult eliminateDeadCode(ModuleOp topology, ModuleOp impls);

} // namespace iara
