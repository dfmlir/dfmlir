#!/bin/bash

rm -f *.o
rm -f *.ll

for FUNCTION in div_rem; do
  $POLYGEIST_MLIR_CLANG_BIN --memref-abi --memref-fullrank -S -I$LLVM_PROJECT/clang/lib/Headers --function=${FUNCTION} ${FUNCTION}.c -o ${FUNCTION}.mlir 1>/dev/null 2>/dev/null || exit 10
done

$DFMLIR_BIN --hoistActors --deadCodeElimination --dumpLLVMIR ${ADDITIONAL_MLIR_OPTIONS} dce.dif >schedule.ll || exit 1

# Make sure that there's no function with the original name.
[[ "$(grep -c 'declare .* @div_rem(' schedule.ll)" == 0 ]] || exit 2
$CLANG_BIN -w -g -c schedule.ll -o schedule.o || exit 3
$CLANG_BIN -w -g -c main.c -o main.o || exit 4
$CLANG_BIN -w -g schedule.o main.o -o dce || exit 5
[[ "$(echo 5 3 | ./dce)" == "1" ]] || exit 6
