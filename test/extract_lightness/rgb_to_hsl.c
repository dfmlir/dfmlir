#include <math.h>

void rgb_to_hsl(int width, int height, unsigned char *rgb, float h[307200],
                float s[307200], float l[307200]) {
  unsigned long int num_pixels = width * height;
  for (unsigned long int i = 0; i < num_pixels; i++) {
    float r = (float)rgb[i * 3] / 255.0;
    float g = (float)rgb[i * 3 + 1] / 255.0;
    float b = (float)rgb[i * 3 + 2] / 255.0;
    float cmax = (r >= g && r >= b) ? r : (g >= b ? g : b);
    float cmin = (r <= g && r <= b) ? r : (g <= b ? g : b);
    float delta = cmax - cmin;
    float H = 0.0;
    if (cmax == r) {
      H = (fmod((g - b) / delta, 6.0));
    }
    if (cmax == g) {
      H = (b - r) / delta + 2.0;
    }
    if (cmax == b) {
      H = (r - g) / delta + 4.0;
    }
    H *= 60.0;
    float L = (cmax + cmin) / 2.0;
    float S = 0.0;
    if (delta != 0.0) {
      S = delta / (1 - fabs(2.0 * L - 1));
    }
    h[i] = H;
    s[i] = S;
    l[i] = L;
  }
}
