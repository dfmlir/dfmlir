#include <stdio.h>

FILE* input_file;
FILE* output_file;

void run();

void read_rgb_frame (int width, int height, unsigned char rgb[921600]){
    fread(&rgb[0], sizeof(unsigned char), width * height, input_file);
}

void write_rgb_frame (int width, int height, unsigned char rgb[921600]){
    fwrite(&rgb[0], sizeof(unsigned char), width * height, output_file);
}

int main() {

  input_file = fopen("video.rgb", "r");
  output_file = fopen("lightness.rgb", "w");

  while (!feof(input_file)) {
    run();
  }

  fclose(input_file);
  fclose(output_file);

  return 0;
}
