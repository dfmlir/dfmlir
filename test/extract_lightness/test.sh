#!/bin/bash


rm -f *.o
rm -f *.ll
rm -f *.mlir
rm -f lightness.rgb

if test -z "$BUFF_STRAT"; then
  BUFF_STRAT=meg
fi;

for FUNCTION in l_to_rgb rgb_to_hsl; do
  echo Hoisting $FUNCTION:
  sh -cv "$POLYGEIST_MLIR_CLANG_BIN --memref-abi=0 -S -I$LLVM_PROJECT/clang/lib/Headers --function=${FUNCTION} ${FUNCTION}.c -o ${FUNCTION}.mlir 1>/dev/null 2>/dev/null" || exit 10
done

# No DCE

SUFFIX="_${BUFF_STRAT}_no_dce"

$DFMLIR_BIN --hoistActors --dumpLLVMIR ${ADDITIONAL_MLIR_OPTIONS} extract_lightness.dif >schedule${SUFFIX}.ll || exit 1
$CLANG_BIN -w -O3 -lm schedule${SUFFIX}.ll main.c -o extract_lightness || exit 2
./extract_lightness || exit 3
cmp lightness.rgb lightness_solution.rgb || exit 4

# DCE

SUFFIX="_${BUFF_STRAT}_dce"

$DFMLIR_BIN --hoistActors --deadCodeElimination --dumpLLVMIR ${ADDITIONAL_MLIR_OPTIONS} extract_lightness.dif >schedule${SUFFIX}.ll || exit 5
$CLANG_BIN -w -O3 -lm schedule${SUFFIX}.ll main.c -o extract_lightness || exit 6
./extract_lightness || exit 7
cmp lightness.rgb lightness_solution.rgb || exit 8

