void l_to_rgb(int width, int height, float l[307200], unsigned char *rgb) {
  unsigned long long num_pixels = width * height;
  for (unsigned long long i = 0; i < num_pixels; i++) {
    unsigned char L = (unsigned char)(l[i] * 255.0);
    rgb[i * 3] = rgb[i * 3 + 1] = rgb[i * 3 + 2] = L;
  }
}