LC_ALL="en_US.UTF-8"
TIMEFORMAT="%U	%S	%E"

for prog in $1 $2 ; do

	echo profiling $prog
	
	for i in {1..50} ; do
		time $prog
	done
done
