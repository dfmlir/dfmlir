#!/bin/bash

# run from this folder



rm -f \*\.o
rm -f \*\.ll
$DFMLIR_BIN --dumpLLVMIR ${ADDITIONAL_MLIR_OPTIONS} double.dif >schedule.ll &&
  $CLANG_BIN -w -g -c schedule.ll -o schedule.o &&
  $CLANG_BIN -w -g -c double.c -o double.o &&
  $CLANG_BIN -w -g schedule.o double.o -o double &&
  [[ "$(echo 1 2 3 4 5 6 7 8 9 10 | ./double)" == "2 4 6 8 10 12 14 16 18 20 " ]]
