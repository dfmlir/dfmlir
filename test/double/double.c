#include <stdio.h>

void run();

int main(){
    run();
    return 0;
}

void read(int buf[10]){
    for (int i = 0; i < 10; i++){
        scanf("%d", &buf[i]);
    }
}

void Double(int inp[1], int outp[1]){
    for (int i = 0; i< 10; i++){
        outp[i] = inp[i] * 2;
    }
}

void write(int buf[10]){
    for (int i = 0; i < 10; i++){
        printf("%d ", buf[i]);
    }
    printf("\n");
}