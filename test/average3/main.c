#include <assert.h>
#include <stdio.h>

void run();
void init();

int main() {
  init();
  while (!feof(stdin)) {
    run();
  }
}

void read(int *outp) { scanf("%d", outp); }

void average3(int old[2], int new[1], int fb[2], int o1[0]) {
  fb[0] = old[1];
  fb[1] = new[0];
  o1[0] = (old[0] + old[1] + new[0]) / 3;
  fprintf(stderr, "%d + %d + %d / 3 = %d\n", old[0], old[1], new[0], o1[0]);
}

void write(int *inp) { printf("%d ", inp[0]); }
