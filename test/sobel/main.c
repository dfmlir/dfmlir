#include "sobel.h"
#include "yuvDisplay.h"
#include "yuvRead.h"

int preesmStopThreads = 0;

void run();

int main() {

  initReadYUV(352, 288);
  yuvDisplayInit(0, 352, 288);
  while (!preesmStopThreads) {
    run();
  }

  return 0;
}