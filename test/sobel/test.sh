#!/bin/bash


rm -f *.o
rm -f *.ll

FLAGS="-Wextra -O3 -lSDL2 -lSDL2_ttf -I/usr/include/SDL2 -DPROJECT_ROOT_PATH=\"$(pwd)\""

echo Generating LLVM IR...
$DFMLIR_BIN --dumpLLVMIR ${ADDITIONAL_MLIR_OPTIONS} sobel.dif >schedule.ll ||  exit 1
echo Generation object file...
$CLANG_BIN $FLAGS -w -c -o schedule.o schedule.ll|| exit 2
echo Compiling external actors...
$CLANG_BIN $FLAGS -w -c -O3 *.c || exit 3
echo Linking...
$CLANG_BIN $FLAGS *.o -o sobel || exit 4
# $CLANG_BIN -w -O3 -flto -lSDL -lSDL2 -lSDL2_ttf -l/usr/lib/SDL   *.c schedule.o -o sobel || exit 3
