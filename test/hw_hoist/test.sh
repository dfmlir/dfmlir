#!/bin/bash

rm -f *.o
rm -f *.ll

for FUNCTION in hello world; do
  $POLYGEIST_MLIR_CLANG_BIN --memref-abi --memref-fullrank -S -I$LLVM_PROJECT/clang/lib/Headers --function=${FUNCTION} ${FUNCTION}.c -o ${FUNCTION}.mlir 1>/dev/null 2>/dev/null
done

rm -f *.o
rm -f *.ll
$DFMLIR_BIN --hoistActors --dumpLLVMIR ${ADDITIONAL_MLIR_OPTIONS} helloworld.dif >schedule.ll &&
  $CLANG_BIN -w -g -c schedule.ll -o schedule.o &&
  $CLANG_BIN -w -g -c main.c -o main.o &&
  $CLANG_BIN -w -g *.o -o helloworld &&
  test "Hello World!" = "$(./helloworld)"
