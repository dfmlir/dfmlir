#!/bin/bash

if [[$DFMLIR_DIR == ""]]; then
    source env.sh
fi

cd $DFMLIR_DIR/build

echo "Building:"
echo -n "   "
cmake --build .

cd $DFMLIR_DIR/test

TESTS_PASSED=0
TOTAL_TESTS=0

# for bufferization in naive meg; do
for bufferization in pool; do
    export BUFF_STRAT="$bufferization"
    export ADDITIONAL_MLIR_OPTIONS="--bufferizationStrategy=$bufferization"
    echo ""
    echo Bufferization strategy = $bufferization
    pwd
    echo ""
    cd $DFMLIR_DIR/test
    for x in $(ls -d */) ; do
        TOTAL_TESTS=$(($TOTAL_TESTS + 1))
        cd $DFMLIR_DIR/test/$x
        echo -n "Test: "
        printf '%-40s' $x
        bash ./test.sh >/dev/null 2>&1
        EXIT_CODE=$?
        if [[ $EXIT_CODE == 0 ]]; then
            echo -en "\r\e[32m"
            echo -n "Test: "
            printf '%-40s' $x
            echo -e "\e[32mPassed\e[0m"
            TESTS_PASSED=$(($TESTS_PASSED + 1))
        else
            echo -en "\r\e[31m"
            echo -n "Test: "
            printf '%-40s' $x
            echo -e "\e[31mFailed ( $EXIT_CODE )\e[0m"
        fi
    done
done

echo Tests passed: $TESTS_PASSED/$TOTAL_TESTS
