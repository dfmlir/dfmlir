#include <assert.h>
#include <stdio.h>

void run();

int main() { run(); }

void read(int *outp) { *outp = 42; }

void write(int *inp) { printf("%d ", *inp); }
