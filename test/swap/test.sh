#!/bin/bash

# run from this folder

rm *.o
rm *.ll

for FUNCTION in swap; do
  $POLYGEIST_MLIR_CLANG_BIN --memref-abi --memref-fullrank -S -I$LLVM_PROJECT/clang/lib/Headers --function=${FUNCTION} ${FUNCTION}.c -o ${FUNCTION}.mlir 1>/dev/null 2>/dev/null
done

$DFMLIR_BIN --hoistActors --splitActors --dumpLLVMIR ${ADDITIONAL_MLIR_OPTIONS} split.dif >schedule.ll || exit 1

$CLANG_BIN -w -g -c schedule.ll -o schedule.o || exit 1
$CLANG_BIN -w -g -c split.c -o split.o || exit 2
$CLANG_BIN -w -g schedule.o split.o -o split || exit 3
[[ "$(echo 1 2 | ./split)" == "2 1" ]] || exit 4
