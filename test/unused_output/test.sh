#!/bin/bash

# run from this folder



rm -f \*\.o
rm -f \*\.ll

# $DFMLIR_BIN --hoistActors --splitActors --dumpLLVMIR split.dif >schedule.ll || exit 1

$DFMLIR_BIN --dumpLLVMIR ${ADDITIONAL_MLIR_OPTIONS} unused_output.dif >schedule.ll || exit 1

$CLANG_BIN -w -g -c schedule.ll -o schedule.o || exit 2
$CLANG_BIN -w -g -c main.c -o main.o || exit 3
$CLANG_BIN -w -g schedule.o main.o -o unused_output || exit 4
[[ "$(echo 5 3 | ./unused_output)" == "1" ]] || exit 5
