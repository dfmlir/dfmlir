#include <string.h>

void BroadcastFrame(int width, int height, char *src, char *dest1,
                    char *dest2) {
  unsigned long long size = 3 * width * height * sizeof(char);
  memcpy(dest1, src, size);
  memcpy(dest2, src, size);
}
