void initMp4Read();
void initMp4Display(int width, int height);
void run();

int preesmStopThreads = 0;

int main() {
  initMp4Read();
  initMp4Display(640, 480);
  while (!preesmStopThreads) {
    run();
  }
  return 0;
}
