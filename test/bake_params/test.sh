#!/bin/bash

rm -f \*\.o
rm -f \*\.ll
rm -f \*\.mlir

# Raise file to mlir.

for FUNCTION in addN ; do
  $POLYGEIST_MLIR_CLANG_BIN --memref-abi --memref-fullrank -S -I$LLVM_PROJECT/clang/lib/Headers --function=${FUNCTION} ${FUNCTION}.c -o ${FUNCTION}.mlir 1>/dev/null 2>/dev/null
done

$DFMLIR_DIR/build/bin/dfmlir --hoistActors --bakeParams --dumpLLVMDialect bake_params.dif >schedule.mlir || exit 1

# see if declaration of addN has only 2 arguments
egrep 'llvm\.func @addN.*\([^,]*,[^,]*\) {' schedule.mlir >addn_decl.txt
[ -s addn_decl.txt ] || exit 2

$DFMLIR_DIR/build/bin/dfmlir --hoistActors --bakeParams --dumpLLVMIR bake_params.dif >schedule.ll || exit 3

clang -w -g -c schedule.ll -o schedule.o || exit 4
clang -w -g -c main.c -o main.o || exit 5
clang -w -g schedule.o main.o -o bake_params || exit 6
[[ "$(echo 3 | ./bake_params)" == "7" ]] || exit 7
