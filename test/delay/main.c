#include <assert.h>
#include <stdio.h>

void init();
void run();

int main() {
  init();
  run();
}

void read(int *outp) { scanf("%d", outp); }

void write(int *inp) {
  for (int i = 0; i < 5; i++) {
    printf("%d ", inp[i]);
  }
}
