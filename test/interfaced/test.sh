#!/bin/bash

# run from this folder



rm *.o
rm *.ll
$DFMLIR_BIN --dumpLLVMIR ${ADDITIONAL_MLIR_OPTIONS} interfaced.dif >schedule.ll &&
  $CLANG_BIN -w -g -c schedule.ll -o schedule.o &&
  $CLANG_BIN -w -g -c interfaced.c -o interfaced.o &&
  $CLANG_BIN -w -g schedule.o interfaced.o -o interfaced &&
  [[ "$(echo 1 | ./interfaced)" == "7 " ]]
