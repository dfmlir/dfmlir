#include <stdio.h>

void run();

int main() {
  run();
  return 0;
}

void read(int buf[1]) { scanf("%d", &buf[0]); }

void add(int n, int inp[1], int outp[1]) { outp[0] = inp[0] + n; }

void write(int buf[1]) {
  printf("%d ", buf[0]);
  printf("\n");
}
