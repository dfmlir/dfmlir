void initMp4Read();
void initMp4Display(int width, int height);
void run();
void init();

int main() {
  init();
  run();
  return 0;
}
