#!/bin/bash


# rm -f *.o
# rm -f *.ll
# rm -f *.mlir
# rm -f lightness.rgb

# for FUNCTION in split; do
#   echo Hoisting $FUNCTION:
#   sh -cv "$POLYGEIST_MLIR_CLANG_BIN --memref-abi=0 -S -I$LLVM_PROJECT/clang/lib/Headers --function=${FUNCTION} src/${FUNCTION}.c -o ${FUNCTION}.mlir 1>/dev/null 2>/dev/null"
# done

$DFMLIR_BIN --dumpLLVMIR ${ADDITIONAL_MLIR_OPTIONS} topology.dif >schedule.ll || exit 1
$CLANG_BIN -w -O3 -c schedule.ll -o schedule.o || exit 2
rm CMakeCache.txt
cmake -DSTATIC=ON . || exit 3
make || exit 4
cd Release
./extract || exit 5
exit 0