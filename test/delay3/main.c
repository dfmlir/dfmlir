#include <assert.h>
#include <stdio.h>

void run();
void init();

int main() {
  init();
  run();
}

void read1(int *outp) {
  scanf("%d", outp);
  fprintf(stderr, "read1: %d ", *outp);
}

void read2(int *outp) {
  scanf("%d", outp);
  fprintf(stderr, "read2: %d ", *outp);
}

void f(int *i1, int *i2, int *o1) {
  for (int i = 0; i < 5; i++) {
    o1[i] = i1[i] + 1000 * i2[i];
  }
}

void write(int *inp) { printf("(%d) ", inp[0]); }
