#!/bin/bash

# run from this folder



rm *.o
rm *.ll
$DFMLIR_BIN --dumpLLVMIR ${ADDITIONAL_MLIR_OPTIONS} topology.dif >schedule.ll || exit 1

$CLANG_BIN -w -g schedule.ll main.c -o main || exit 1
echo 4 5 6 7 8 9 10 | ./main || exit 2
exit 0
