#include <assert.h>
#include <stdio.h>
#include <string.h>

void run();

int main() { run(); }

void read(char outp[10]) { memcpy(outp, "abcdefghi", 10); }

void write(int inp[20]) { printf("%s %s", inp, &inp[10]); }
