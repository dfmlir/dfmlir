#!/bin/bash

# run from this folder



rm *.o
rm *.ll
$DFMLIR_BIN --dumpLLVMIR ${ADDITIONAL_MLIR_OPTIONS} helloworld.dif >schedule.ll &&
  $CLANG_BIN -w -g -c schedule.ll -o schedule.o &&
  $CLANG_BIN -w -g -c helloworld.c -o helloworld.o &&
  $CLANG_BIN -w -g schedule.o helloworld.o -o helloworld &&
  test "Hello World!" = "$(./helloworld)"
