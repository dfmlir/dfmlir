#include <stdio.h>
#include <assert.h>

void run();

int main(){
    run();
}

void hello(char* outp){
    *outp = 42;
    printf("Hello ");
}

void world(char* inp){
    assert(*inp == 42);
    printf("World!\n");
}
