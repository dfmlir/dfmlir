#include <stdio.h>
#include <assert.h>

void run();

int main(){
    run();
}

void read(int n, char *out){
    for (int i = 0; i < n; i++){
        int k;
        scanf("%d", &k);
        out[i] = k;
    }
}

void add_n(int n, char *in, char *out){
    for (int i = 0; i < n; i++){
        out[i] = in[i]+1;
    }
}

void write(int n, char *in){
    for (int i = 0; i < n; i++){
        printf("%d ", in[i]);
    }
}
