#!/bin/bash

# run from this folder



rm *.o
rm *.ll
$DFMLIR_BIN --dumpLLVMIR ${ADDITIONAL_MLIR_OPTIONS} meg.dif >schedule.ll || exit 1
$CLANG_BIN -w -g -c schedule.ll -o schedule.o || exit 2
$CLANG_BIN -w -g -c meg.c -o meg.o || exit 3
$CLANG_BIN -w -g schedule.o meg.o -o meg || exit 4
[[ "$(echo 1 2 3 4 5 6 7 8 9 10 | ./meg)" == "11 12 13 14 15 16 17 18 19 20 " ]] || exit 5
