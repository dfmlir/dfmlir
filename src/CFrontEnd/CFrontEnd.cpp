#include "CFrontEnd/CFrontEnd.h"
#include "Dataflow/Dialect.h"
#include "Dataflow/Passes.h"
#include "mlir/Conversion/AffineToStandard/AffineToStandard.h"
#include "mlir/Conversion/ArithmeticToLLVM/ArithmeticToLLVM.h"
#include "mlir/Conversion/ControlFlowToLLVM/ControlFlowToLLVM.h"
#include "mlir/Conversion/FuncToLLVM/ConvertFuncToLLVM.h"
#include "mlir/Conversion/FuncToLLVM/ConvertFuncToLLVMPass.h"
#include "mlir/Conversion/LLVMCommon/ConversionTarget.h"
#include "mlir/Conversion/LLVMCommon/LoweringOptions.h"
#include "mlir/Conversion/LLVMCommon/TypeConverter.h"
#include "mlir/Conversion/MathToLLVM/MathToLLVM.h"
#include "mlir/Conversion/MemRefToLLVM/MemRefToLLVM.h"
#include "mlir/Conversion/ReconcileUnrealizedCasts/ReconcileUnrealizedCasts.h"
#include "mlir/Conversion/SCFToControlFlow/SCFToControlFlow.h"
#include "mlir/Dialect/LLVMIR/LLVMDialect.h"
#include "mlir/IR/Attributes.h"
#include "mlir/IR/Builders.h"
#include "mlir/IR/BuiltinAttributes.h"
#include "mlir/IR/Diagnostics.h"
#include "mlir/IR/OpImplementation.h"
#include "mlir/IR/Operation.h"
#include "mlir/IR/SymbolTable.h"
#include "mlir/Parser/Parser.h"
#include "mlir/Pass/PassManager.h"
#include "mlir/Support/LogicalResult.h"
#include "util/util.h"
#include "llvm/ADT/SmallVector.h"
#include "llvm/ADT/StringMap.h"
#include "llvm/ADT/StringRef.h"
#include "llvm/Support/Casting.h"
#include "llvm/Support/FileSystem.h"
#include "llvm/Support/FormatVariadic.h"
#include "llvm/Support/MemoryBuffer.h"
#include "llvm/Support/Program.h"
#include "llvm/Support/SMLoc.h"
#include "llvm/Support/SourceMgr.h"
#include <cstdlib>

#include <memory>
#include <string>

using namespace iara;

auto replaceUses(llvm::StringRef old_name, mlir::SymbolRefAttr new_name,
                 mlir::Operation *op) {
  op->walk([&](mlir::Operation *nested) {
    llvm::SmallVector<mlir::NamedAttribute> to_check;
    for (auto named_attr : nested->getAttrs()) {
      auto sym_ref =
          named_attr.getValue().dyn_cast_or_null<mlir::FlatSymbolRefAttr>();
      if (!sym_ref || sym_ref.getValue() != old_name)
        continue;
      to_check.push_back(named_attr);
    }
    for (auto attr : to_check) {
      nested->setAttr(attr.getName(), new_name);
    }
  });
};

mlir::ModuleOp getCSourceMLIR(mlir::ModuleOp module) {
  // Assumes that every implementation is in the working dir, in a file called
  // "<funcname.c>".

  std::map<llvm::StringRef, KernelOp> actortypes;

  module.walk([&](KernelOp op) { actortypes[op.sym_name()] = op; });

  auto all_sources_module = mlir::ModuleOp::create(module->getLoc());

  auto builder = mlir::OpBuilder::atBlockEnd(all_sources_module.getBody());

  for (auto [name, op] : actortypes) {
    auto func_name = name;
    // llvm::outs() << "Looking for function " << name << "\n";

    std::string mlir_file = llvm::formatv("{0}.mlir", name);

    if (!llvm::sys::fs::exists(mlir_file))
      continue;

    auto file_module =
        mlir::parseSourceFile<mlir::ModuleOp>(mlir_file, module->getContext());

    // Rename globals to avoid collisions

    // file_module->dump();

    if (!file_module) {
      // Empty module; didn't find implementation
      continue;
    }

    for (auto &op : *file_module->getBody()) {
      auto global_op = llvm::dyn_cast<mlir::LLVM::GlobalOp>(op);
      if (!global_op)
        continue;
      auto new_name = mlir::StringAttr::get(
          file_module->getContext(),
          llvm::formatv("{0}__{1}", func_name, global_op.getSymName()));

      auto new_symbol_ref = mlir::SymbolRefAttr::get(new_name);

      replaceUses(global_op.getSymName(), new_symbol_ref, *file_module);
      global_op->setAttr("sym_name", new_name);
    }

    for (auto &op : *file_module->getBody()) {
      if (auto func_op = llvm::dyn_cast<mlir::LLVM::LLVMFuncOp>(op)) {
        if (lookupSymbol(all_sources_module, func_op.getName()) != nullptr) {
          continue;
        }
      }
      auto new_op = op.clone();

      builder.insert(new_op);
    }
  }

  return all_sources_module;
}

mlir::LogicalResult replaceImplDeclsWithDefs(mlir::ModuleOp scheduled_graph,
                                             mlir::ModuleOp c_sources) {

  // Convert c_sources to LLVM to match the scheduled graph. We have to do it
  // at LLVM level because Polygeist generates llvm.ptr<>s instead of memrefs.

  using namespace mlir;
  auto context = c_sources.getContext();

  // c_sources.dump();

  // // lower affine
  {
    PassManager pm(context);
    pm.addPass(std::make_unique<iara::SDFScheduleToLLVMLoweringPass>());
    pm.enableVerifier();
    if (pm.run(c_sources).failed()) {
      c_sources->emitError() << "Failed to lower C sources to LLVM";
      // c_sources.dump();
      return failure();
    }
  }

  // c_sources.dump();

  auto builder = mlir::OpBuilder::atBlockEnd(scheduled_graph.getBody());

  for (auto &op : *c_sources.getBody()) {
    auto func = llvm::dyn_cast<mlir::LLVM::LLVMFuncOp>(op);
    if (func) {
      auto decl = lookupSymbol(scheduled_graph, func.getName());
      if (auto decl_op = llvm::dyn_cast_or_null<mlir::LLVM::LLVMFuncOp>(decl)) {
        builder.setInsertionPointAfter(decl);
        auto new_func = func->clone();
        // auto uses = decl_op.getSymbolUses(scheduled_graph);
        // if (uses) {
        //   for (auto use : *uses) {
        //     auto call_op = llvm::dyn_cast<mlir::LLVM::CallOp>(use.getUser());
        //     if (!call_op) {
        //       decl_op->emitError()
        //           << "Currently only normal calls are supported";
        //       return mlir::failure();
        //     }
        //     call_op.setCalleeAttr(mlir::SymbolRefAttr::get(new_func));
        //   }
        // }
        decl_op->erase();
        builder.insert(new_func);
        continue;
      }
    }
    builder.setInsertionPointToEnd(scheduled_graph.getBody());
    builder.insert(op.clone());
  }

  return mlir::success();
}
