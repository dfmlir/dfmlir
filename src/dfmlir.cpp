#include "DIF/mlirgen.h"
#include "mlir/Conversion/ControlFlowToLLVM/ControlFlowToLLVM.h"
#include "mlir/Conversion/SCFToControlFlow/SCFToControlFlow.h"
#include "mlir/Dialect/Affine/IR/AffineOps.h"
#include "mlir/Dialect/Arithmetic/IR/Arithmetic.h"
#include "mlir/Dialect/DLTI/DLTI.h"
#include "mlir/Dialect/Func/IR/FuncOps.h"
#include "mlir/Dialect/LLVMIR/LLVMDialect.h"
#include "mlir/Dialect/MemRef/IR/MemRef.h"
#include "mlir/Dialect/SCF/SCF.h"
#include "mlir/ExecutionEngine/ExecutionEngine.h"
#include "mlir/ExecutionEngine/OptUtils.h"
#include "mlir/IR/AsmState.h"
#include "mlir/IR/BuiltinDialect.h"
#include "mlir/IR/BuiltinOps.h"
#include "mlir/IR/Diagnostics.h"
#include "mlir/IR/MLIRContext.h"
#include "mlir/IR/Verifier.h"
#include "mlir/InitAllDialects.h"
#include "mlir/Parser/Parser.h"
#include "mlir/Pass/Pass.h"
#include "mlir/Pass/PassManager.h"
#include "mlir/Target/LLVMIR/Export.h"
#include "mlir/Transforms/Passes.h"
#include "util/graphutils.h"
#include "llvm/Support/Path.h"

#include "CFrontEnd/CFrontEnd.h"
#include "ImplOptimization/ImplOptimization.h"

#include "mlir/Target/LLVMIR/Dialect/LLVMIR/LLVMToLLVMIRTranslation.h"
#include "llvm/ADT/StringRef.h"
#include "llvm/IR/Module.h"
#include "llvm/Support/CommandLine.h"
#include "llvm/Support/ErrorOr.h"
#include "llvm/Support/MemoryBuffer.h"
#include "llvm/Support/SourceMgr.h"
#include "llvm/Support/TargetSelect.h"
#include "llvm/Support/raw_ostream.h"

#include "Dataflow/Dialect.h"
#include "Dataflow/Passes.h"
#include "Dataflow/SDFPass.h"

#include <memory>

namespace cl = llvm::cl;

using namespace iara;

static cl::opt<std::string> inputFilename(cl::Positional,
                                          cl::desc("<input dif file>"),
                                          cl::init("-"),
                                          cl::value_desc("filename"));

static cl::opt<bool> dumpTokens("dumpTokens");
static cl::opt<bool> dumpAST("dumpAST");
static cl::opt<bool> dumpDataflowDialect("dumpDataflowDialect");
static cl::opt<bool>
    dumpFlattenedDataflowDialect("dumpFlattenedDataflowDialect");
static cl::opt<bool> dumpStandardDialect("dumpStandardDialect");
static cl::opt<bool> dumpLLVMDialect("dumpLLVMDialect");
static cl::opt<bool> dumpLLVMIR("dumpLLVMIR");
static cl::opt<bool> hoistActors("hoistActors");
static cl::opt<bool> splitActors("splitActors");
static cl::opt<bool> bakeParams("bakeParams");
static cl::opt<bool> deadCodeElimination("deadCodeElimination");
static cl::opt<SDFSchedulePass::BufferizationStrategy> bufferizationStrategy(
    "bufferizationStrategy", cl::desc("Choose optimization level:"),
    cl::values(
        cl::OptionEnumValue{
            "naive", SDFSchedulePass::BufferizationStrategy::POOL,
            "Exclusive memory space for every token in an iteration"},
        cl::OptionEnumValue{"pool",
                            SDFSchedulePass::BufferizationStrategy::POOL,
                            "Memory Exclusion Graph-based allocation"}),
    cl::init(SDFSchedulePass::BufferizationStrategy::POOL));

int main(int argc, char **argv) {

  cl::ParseCommandLineOptions(argc, argv, "dif compiler\n");

  // Read source file

  auto fileOrErr = llvm::MemoryBuffer::getFileOrSTDIN(inputFilename);
  if (auto ec = fileOrErr.getError()) {
    llvm::errs() << "Could not open file: " << ec.message() << "\n";
    exit(1);
  }

  auto source_buffer = fileOrErr.get().release();
  auto source_buffer_ptr = std::shared_ptr<llvm::MemoryBuffer>(source_buffer);

  bool generateLLVMIR = dumpLLVMIR.getValue();
  bool generateLLVMD = generateLLVMIR || dumpLLVMDialect.getValue();
  bool generateSTD = generateLLVMD || dumpStandardDialect.getValue();
  bool generateDF = generateSTD || dumpDataflowDialect.getValue();
  bool generateAST = generateDF || dumpAST.getValue();

  if (!generateAST)
    return 0;

  // Generate AST from source file

  if (dumpTokens) {
    auto lexer = dif::Lexer(source_buffer_ptr, inputFilename.getValue());
    lexer.dumpTokens();
  }

  auto lexer = dif::Lexer(source_buffer_ptr, inputFilename.getValue());
  auto ast = lexer.readDIF();

  if (dumpAST) {
    for (auto &graph : ast) {
      llvm::outs() << graph.printTree(); // Dump AST
    }
  }

  if (!generateDF) {
    return 0;
  }

  // Generate top-level dataflow dialect from AST

  mlir::registerAsmPrinterCLOptions();
  mlir::registerMLIRContextCLOptions();
  mlir::registerPassManagerCLOptions();

  mlir::MLIRContext context;
  context.printStackTraceOnDiagnostic(true);
  context.printOpOnDiagnostic(true);
  context.getOrLoadDialect<mlir::memref::MemRefDialect>();
  context.getOrLoadDialect<mlir::LLVM::LLVMDialect>();
  context.getOrLoadDialect<mlir::func::FuncDialect>();
  context.getOrLoadDialect<mlir::AffineDialect>();
  context.getOrLoadDialect<mlir::arith::ArithmeticDialect>();
  context.getOrLoadDialect<mlir::cf::ControlFlowDialect>();
  context.getOrLoadDialect<mlir::DLTIDialect>();
  context.getOrLoadDialect<mlir::scf::SCFDialect>();
  context.getOrLoadDialect<mlir::math::MathDialect>();

  context.getOrLoadDialect<iara::DataflowDialect>();

  auto module_ref_or_f = dif::mlirGen(
      context, ast, llvm::sys::path::parent_path(inputFilename.getValue()));

  if (failed(module_ref_or_f)) {
    return 1;
  }

  mlir::OwningOpRef<mlir::ModuleOp> module_ref = module_ref_or_f.getValue();

  OwningOpRef<ModuleOp> c_sources;

  if (dumpDataflowDialect) {
    module_ref->print(llvm::outs());
  }

  if (!generateSTD)
    return 0;

  if (hoistActors) {
    // Find relevant functions in C files, generate MLIR with polygeist
    // and embed them in the module

    c_sources = getCSourceMLIR(*module_ref);

    if (!c_sources) {
      module_ref->emitError() << "C -> MLIR conversion failed.";
      return 1;
    }
  }

  if (splitActors) {
    if (iara::splitImpls(*module_ref, *c_sources).failed()) {
      c_sources->emitRemark() << "Failed to split some actors.";
    };
  }

  if (bakeParams) {
    if (iara::bakeParameters(*module_ref, *c_sources).failed()) {
      c_sources->emitRemark() << "Failed to bake some parameters.";
    }
  }

  if (deadCodeElimination) {
    if (iara::eliminateDeadCode(*module_ref, *c_sources).failed()) {
      c_sources->emitRemark() << "Failed to eliminate some dead code.";
    }
  }

  removeUnusedActortypes(*module_ref, *c_sources);

  // Schedule graph

  {
    // find graph named "main" and flatten it
    mlir::ModuleOp module_op = module_ref.get();
    auto main_op = [&]() -> GraphOp {
      for (auto &op : *module_op.getBody()) {
        auto graph = llvm::dyn_cast<GraphOp>(op);
        if (graph && graph.getName() == "main")
          return graph;
      }
      return nullptr;
    }();

    if (!main_op) {
      module_op.emitError() << "No 'main' graph found in DIF.";
    }

    if (flattenTopLevelGraph(main_op).failed()) {
      main_op->emitError() << "Failed to flatten top level graph.";
    };

    if (dumpFlattenedDataflowDialect) {
      module_ref->print(llvm::outs());
    }

    // Schedule "main"

    mlir::PassManager pm(main_op.getContext());
    mlir::applyPassManagerCLOptions(pm);

    auto schedule_pass = new SDFSchedulePass(bufferizationStrategy);

    pm.addPass(std::unique_ptr<SDFSchedulePass>(schedule_pass));

    if (failed(pm.run(module_op))) {
      main_op->emitError() << "SDF scheduling failed.";
      return 1;
    }
  }

  if (dumpStandardDialect) {
    module_ref->print(llvm::outs());
  }

  if (!generateLLVMD)
    return 0;

  // Lower to LLVM dialect

  {
    mlir::PassManager pm(&context);
    mlir::applyPassManagerCLOptions(pm);
    pm.addPass(std::make_unique<iara::SDFScheduleToLLVMLoweringPass>());
    pm.addPass(createStripDebugInfoPass());

    if (failed(pm.run(*module_ref))) {
      module_ref->emitError() << "Lowering to LLVM dialect failed.";
      return 1;
    }
  }

  if (hoistActors) {
    // c_sources->dump();
    if (replaceImplDeclsWithDefs(*module_ref, *c_sources).failed()) {
      module_ref->emitError() << "Failed to inject C sources.";
    }
    // llvm::errs() << "Hoisted\n";
  }

  if (dumpLLVMDialect) {
    module_ref->print(llvm::outs());
  }

  if (!generateLLVMIR)
    return 0;

  // Lower to LLVM IR

  {

    // Register the translation to LLVM IR with the MLIR context.
    mlir::registerLLVMDialectTranslation(context);

    llvm::LLVMContext llvmContext;
    std::unique_ptr<llvm::Module> llvmModule =
        mlir::translateModuleToLLVMIR(*module_ref, llvmContext);

    if (!llvmModule) {
      module_ref->emitError() << "Lowering to LLVM IR failed.";
      return 1;
    }

    // // Initialize LLVM targets.
    llvm::InitializeNativeTarget();
    llvm::InitializeNativeTargetAsmPrinter();
    // // mlir::ExecutionEngine::setupTargetTriple(llvmModule.get());

    // /// Optionally run an optimization pipeline over the llvm module.
    auto optPipeline = mlir::makeOptimizingTransformer(3, 0, nullptr);
    if (auto err = optPipeline(llvmModule.get())) {
      llvm::errs() << "Failed to optimize LLVM IR " << err << "\n";
      return 1;
    }

    if (dumpLLVMIR) {
      llvm::outs() << *llvmModule;
    }
  }

  return 0;
}