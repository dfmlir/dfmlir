#include "Dataflow/Dialect.h"
#include "llvm/Support/FormatVariadic.h"
#include <set>

namespace iara {

using namespace mlir;

struct FlattenContext {
  std::map<KernelOp, KernelOp> actortype_copies;
  std::set<NodeOp> flattened_nodes;
  GraphOp main_op;
  llvm::Optional<mlir::OpBuilder> builder;

  FlattenContext(GraphOp main_op) : main_op(main_op) {
    builder = mlir::OpBuilder(main_op).atBlockEnd(main_op.getBody());
  }
};

std::string prependGraphName(llvm::StringRef name, GraphOp graph) {
  return std::string(llvm::formatv("{0}::{1}", graph.getName(), name));
};

LogicalResult copy_default_params(GraphOp from, FlattenContext &ctx) {
  if (!ctx.main_op.param_defaults() && !from.param_defaults())
    return success();
  llvm::SmallVector<Attribute> params;
  if (ctx.main_op.param_defaults()) {
    for (auto &param : ctx.main_op.param_defaults().getValue()) {
      params.push_back(param.cast<ParameterAttr>());
    }
  }
  if (from.param_defaults()) {
    for (auto &attr : from.param_defaults().getValue()) {
      auto param = attr.cast<ParameterAttr>();
      auto new_name = prependGraphName(param.getName().getValue(), from);
      params.push_back(ParameterAttr::get(ctx.main_op.getContext(), new_name,
                                          param.getValue()));
    }
  }

  auto default_params = ArrayAttr::get(ctx.main_op.getContext(), params);

  ctx.main_op->setAttr("param_defaults", default_params);

  return success();
}

LogicalResult copy_actortypes(GraphOp from, FlattenContext &ctx) {
  for (auto &op : *from.getBody()) {
    if (auto at_op = llvm::dyn_cast<KernelOp>(op)) {
      if (ctx.actortype_copies.count(at_op) == 0) {

        // copy params

        llvm::SmallVector<Attribute> params;
        if (at_op.parameters())
          for (auto attr : *at_op.parameters()) {
            auto param = attr.cast<ParameterAttr>();
            auto new_name = prependGraphName(param.getName().getValue(), from);
            auto new_param = ParameterAttr::get(ctx.main_op.getContext(),
                                                new_name, param.getValue());
            params.push_back(new_param);
          }

        ArrayAttr params_attr = nullptr;

        if (!params.empty())
          params_attr = ArrayAttr::get(ctx.main_op->getContext(), params);

        auto new_name = prependGraphName(at_op.getName(), from);

        auto new_at_op = ctx.builder->create<KernelOp>(
            at_op.getLoc(),
            // KernelOp::build(, ,
            new_name, params_attr, at_op.inputs().getValueOr(nullptr),
            at_op.outputs().getValueOr(nullptr));
        ctx.actortype_copies[at_op] = new_at_op;
      }
    }
  }
  return success();
}

LogicalResult instance_subnodes(FlattenContext &ctx, NodeOp node_op,
                                GraphOp subgraph) {
  // For each subnode in the subgraph:
  for (auto &op : *subgraph.getBody()) {
    auto subnode = llvm::dyn_cast<NodeOp>(op);
    if (!subnode)
      continue;
    auto subnode_impl = lookupSymbol(subnode, subnode.implementation());
    if (llvm::isa_and_present<GraphOp>(subnode_impl)) {
      node_op->emitError()
          << "Unexpected level 2 hierarchy; flatten all sub-hierarchies "
             "first";
      return failure();
    }
    auto subnode_at = llvm::dyn_cast_if_present<KernelOp>(subnode_impl);

    llvm::SmallVector<Attribute> new_params;

    auto makeNewParam = [&](ParameterAttr old_param) {
      std::string new_param_name =
          prependGraphName(old_param.getName().getValue(), subgraph);
      return ParameterAttr::get(ctx.main_op->getContext(), new_param_name,
                                old_param.getValue());
    };

    if (subnode.param_overrides())
      for (auto subnode_param : subnode.param_overridesAttr()) {
        new_params.push_back(makeNewParam(subnode_param.cast<ParameterAttr>()));
      }

    if (node_op.param_overrides())
      for (auto attr : node_op.param_overridesAttr()) {
        auto param_attr = attr.cast<ParameterAttr>();

        // For each parameter override in the supernode,
        // if the subnode's implementation uses it,
        // and it's not in the subnode's overrides,
        // add it to the subnode's copy

        auto name = param_attr.getName().getValue();

        auto used_by_subnode_impl =
            subnode_at && findParamInArray(subnode_at.parameters(), name);

        auto used_by_subnode =
            findParamInArray(subnode.param_overrides(), name);

        if (used_by_subnode_impl && !used_by_subnode) {
          new_params.push_back(makeNewParam(param_attr));
        }
      }

    ArrayAttr new_overrides =
        new_params.empty() ? nullptr : ctx.builder->getArrayAttr(new_params);
    std::string new_name =
        llvm::formatv("{0}.{1}", node_op.sym_name(), subnode.sym_name());

    std::string new_at_name;
    if (subnode.implementation().getLeafReference().getValue() == "broadcast") {
      new_at_name = "broadcast";
    } else {
      new_at_name = prependGraphName(subnode_at.sym_name(), subgraph);
    }

    // NodeOp::build(, ,
    ctx.builder->create<NodeOp>(
        subnode->getLoc(), new_name,
        mlir::SymbolRefAttr::get(ctx.main_op->getContext(), new_at_name),
        new_overrides);
    ;
  }

  // For each edge in the subgraph

  for (auto &op : *subgraph.getBody()) {
    auto subedge = llvm::dyn_cast<EdgeOp>(op);
    if (!subedge)
      continue;

    auto producer = lookupSymbol(subedge, subedge.prod());
    auto consumer = lookupSymbol(subedge, subedge.cons());

    // if it's between two subnodes
    if (llvm::isa<NodeOp>(producer) && llvm::isa<NodeOp>(consumer)) {
      // generate matching edge between new nodes
      auto producer_op = llvm::cast<NodeOp>(producer);
      auto consumer_op = llvm::cast<NodeOp>(consumer);

      std::string new_prod_name =
          llvm::formatv("{0}.{1}", node_op.sym_name(), producer_op.sym_name());

      std::string new_cons_name =
          llvm::formatv("{0}.{1}", node_op.sym_name(), consumer_op.sym_name());
      std::string new_edge_name =
          llvm::formatv("{0}.{1}", node_op.sym_name(), subedge.sym_name());

      // EdgeOp::build(, ,
      auto new_edge = ctx.builder->create<EdgeOp>(
          subedge->getLoc(), new_edge_name,
          mlir::SymbolRefAttr::get(ctx.main_op->getContext(), new_prod_name),
          subedge.prod_port(),
          mlir::SymbolRefAttr::get(ctx.main_op->getContext(), new_cons_name),
          subedge.cons_port());
      for (auto attr_name : {"delay_duration", "delay_values"}) {
        if (auto attr_value = subedge->getAttr(attr_name)) {
          new_edge->setAttr(attr_name, attr_value);
        }
      }
    }

    // if it's a subgraph input:
    else if (producer == subgraph && llvm::isa<NodeOp>(consumer)) {

      // Look for the edge in the supergraph that maps to this subgraph input.
      EdgeOp superedge = [&]() -> EdgeOp {
        for (auto &op : *ctx.main_op.getBody()) {
          superedge = llvm::dyn_cast<EdgeOp>(op);
          if (!superedge)
            continue;
          if (lookupSymbol(superedge, superedge.cons()) != node_op)
            continue;
          if (superedge.cons_port() == subedge.prod_port())
            return superedge;
        }
        return nullptr;
      }();

      if (!superedge) {
        subedge->emitError()
            << "Can't find input for this instance of this subgraph";
        return failure();
      }

      auto subnode_consumer =
          llvm::cast<NodeOp>(lookupSymbol(subedge, subedge.cons()));

      std::string new_cons_name = llvm::formatv("{0}.{1}", node_op.sym_name(),
                                                subnode_consumer.sym_name());
      superedge->setAttr("cons", mlir::SymbolRefAttr::get(
                                     ctx.main_op->getContext(), new_cons_name));
      superedge->setAttr("cons_port", subedge.cons_portAttr());
    }
    // if it's a subgraph output:
    else if (consumer == subgraph && llvm::isa<NodeOp>(producer)) {

      // Look for the edge in the supergraph that maps to this subgraph
      // output.
      EdgeOp superedge = [&]() -> EdgeOp {
        for (auto &op : *ctx.main_op.getBody()) {
          superedge = llvm::dyn_cast<EdgeOp>(op);
          if (!superedge)
            continue;
          if (lookupSymbol(superedge, superedge.prod()) != node_op)
            continue;
          if (superedge.prod_port() == subedge.cons_port())
            return superedge;
        }
        return nullptr;
      }();

      if (!superedge) {
        subedge->emitError()
            << "Can't find output for this instance of this subgraph";
        return failure();
      }

      auto subnode_producer =
          llvm::cast<NodeOp>(lookupSymbol(subedge, subedge.prod()));

      std::string new_prod_name = llvm::formatv("{0}.{1}", node_op.sym_name(),
                                                subnode_producer.sym_name());
      superedge->setAttr("prod", mlir::SymbolRefAttr::get(
                                     ctx.main_op->getContext(), new_prod_name));
      superedge->setAttr("prod_port", subedge.prod_portAttr());
    }
    // if it's malformed:
    else {
      subedge->emitError() << "Malformed edge: must be either between local "
                              "ports or between an "
                              "interface port and a local port";
      return failure();
    }
  }
  return success();
}

// returns true if error
LogicalResult flattenNode(NodeOp node_op, FlattenContext &ctx) {
  auto impl_op = lookupSymbol(node_op, node_op.implementation());
  if (!impl_op) {
    node_op->emitError() << "Symbol " << node_op.implementation()
                         << "not found";
    return failure();
  }
  auto external_op = llvm::dyn_cast<GraphOp>(impl_op);
  if (!external_op)
    return failure();

  return success(copy_default_params(external_op, ctx).succeeded() &&
                 copy_actortypes(external_op, ctx).succeeded() &&
                 instance_subnodes(ctx, node_op, external_op).succeeded());
}

bool detectRecursiveHierarchy(GraphOp graph_op,
                              std::set<GraphOp> &currently_flattening) {
  if (currently_flattening.count(graph_op)) {
    graph_op->emitError() << "Recursive hierarchy!";
    return true;
  }
  currently_flattening.insert(graph_op);

  for (auto &op : *graph_op.getBody()) {
    auto node_op = llvm::dyn_cast<NodeOp>(op);
    if (!node_op)
      continue;
    auto subgraph = llvm::dyn_cast_or_null<GraphOp>(
        lookupSymbol(node_op, node_op.implementation()));
    if (!subgraph)
      continue;
    if (detectRecursiveHierarchy(subgraph, currently_flattening))
      return true;
  }

  currently_flattening.erase(graph_op);
  return false;
}

LogicalResult flattenGraph(GraphOp graph_op) {
  FlattenContext ctx(graph_op);

  llvm::SmallVector<NodeOp> to_delete;

  for (auto &op : *graph_op.getBody()) {
    auto node_op = llvm::dyn_cast<NodeOp>(op);
    if (!node_op)
      continue;
    auto subgraph = llvm::dyn_cast_or_null<GraphOp>(
        lookupSymbol(node_op, node_op.implementation()));
    if (!subgraph)
      continue;
    to_delete.push_back(node_op);
    if (flattenGraph(subgraph).failed())
      return failure();
    if (flattenNode(node_op, ctx).failed())
      return failure();
  }
  for (auto node : to_delete) {
    node->remove();
    node.erase();
  }
  return success();
}

LogicalResult flattenTopLevelGraph(GraphOp graph_op) {
  std::set<GraphOp> currently_flattening;
  if (detectRecursiveHierarchy(graph_op, currently_flattening)) {
    graph_op->emitError() << "Detected cyclic dependency.";
    return failure();
  }
  return flattenGraph(graph_op);
}
} // namespace iara