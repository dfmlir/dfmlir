
#include "Dataflow/Passes.h"
#include "Dataflow/SDFPass.h"
#include "mlir/Conversion/AffineToStandard/AffineToStandard.h"
#include "mlir/Conversion/ArithmeticToLLVM/ArithmeticToLLVM.h"
#include "mlir/Conversion/ControlFlowToLLVM/ControlFlowToLLVM.h"
#include "mlir/Conversion/FuncToLLVM/ConvertFuncToLLVM.h"
#include "mlir/Conversion/LLVMCommon/ConversionTarget.h"
#include "mlir/Conversion/LLVMCommon/TypeConverter.h"
#include "mlir/Conversion/MathToLLVM/MathToLLVM.h"
#include "mlir/Conversion/MemRefToLLVM/MemRefToLLVM.h"
#include "mlir/Conversion/SCFToControlFlow/SCFToControlFlow.h"
#include "mlir/Dialect/Func/Transforms/FuncConversions.h"
#include "llvm/Support/FormatVariadic.h"

namespace iara {
void SDFSchedulePass::runOnOperation() {

  auto module_op = getOperation();

  auto graph_op = llvm::cast<GraphOp>(lookupSymbol(module_op, "main"));

  // Delete all graphs other than Main.

  llvm::SmallVector<Operation *> to_delete;
  for (Operation &op : *module_op.getBody()) {
    if (llvm::isa<GraphOp>(op) && &op != graph_op)
      to_delete.push_back(&op);
  }
  for (auto op : to_delete) {
    op->erase();
  }

  graph_op->walk([&](GraphOp op) -> mlir::WalkResult {
    graph_op = op;
    return mlir::WalkResult::interrupt();
  });

  if (sdfPass(graph_op).failed()) {
    signalPassFailure();
    return;
  }
}

void SDFScheduleToLLVMLoweringPass::runOnOperation() {

  LowerToLLVMOptions options(&getContext());
  options.useBarePtrCallConv = true;
  LLVMConversionTarget target(getContext());
  target.addLegalOp<ModuleOp>();
  LLVMTypeConverter type_converter(&getContext(), options);
  RewritePatternSet patterns(&getContext());
  populateAffineToStdConversionPatterns(patterns);
  arith::populateArithmeticToLLVMConversionPatterns(type_converter, patterns);
  populateMathToLLVMConversionPatterns(type_converter, patterns);
  populateFuncToLLVMConversionPatterns(type_converter, patterns);
  populateFuncToLLVMFuncOpConversionPattern(type_converter, patterns);
  populateSCFToControlFlowConversionPatterns(patterns);
  mlir::cf::populateControlFlowToLLVMConversionPatterns(type_converter,
                                                        patterns);
  populateMemRefToLLVMConversionPatterns(type_converter, patterns);
  auto module = getOperation();
  if (failed(applyFullConversion(module, target, std::move(patterns))))
    signalPassFailure();
};

} // namespace iara
