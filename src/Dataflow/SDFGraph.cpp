#include "Dataflow/Dialect.h"
#include "Dataflow/SDFPass.h"
#include "mlir/Dialect/Arithmetic/IR/Arithmetic.h"
#include "mlir/Dialect/Func/IR/FuncOps.h"
#include "mlir/Dialect/MemRef/IR/MemRef.h"
#include "mlir/Dialect/SCF/SCF.h"
#include "mlir/IR/Builders.h"
#include "mlir/IR/BuiltinAttributes.h"
#include "mlir/IR/Diagnostics.h"
#include "mlir/IR/Operation.h"
#include "mlir/IR/Value.h"
// #include "mlir/Transforms/
#include "util/graphutils.h"
#include "util/ranges.h"
#include "util/rational.h"
#include "util/util.h"
#include <algorithm>
#include <list>
#include <queue>

namespace iara {

using namespace mlir;
using func::FuncOp;
struct SDFGraph : graphutils::Graph<NodeOp, EdgeOp> {

  // AUXILLIARY ENUMS

  enum Direction { INCOMING, OUTGOING };

  // AUXILLIARY STRUCTS

  struct EdgeInfo {
    Direction direction;
    StringRef this_port_name;
    StringRef other_port_name;
    EdgeOp edge_op;
    NodeOp this_node;
    NodeOp other_node;
    int64_t this_rate;
    int64_t other_rate;
    bool is_unused_output = false;

    // Returns token type (could be a tensor)
    mlir::Type getTokenType() {
      return getPortAttr(this_node, this_port_name).getValue().getDatatype();
    }

    size_t getTokenSize() {
      return getNumElementsToken() * getTokenElementSize();
    }

    // Returns element type (cannot be a tensor)
    mlir::Type getTokenElementType() {
      return iara::getTokenElementType(getTokenType());
    }

    // Returns number of tokens per firing of `this_node`
    size_t getRate() { return this_rate; }

    // Returns size of element, in bytes.
    size_t getTokenElementSize() {
      return iara::getTokenElementSize(getTokenType());
    }
    // Returns number of elements in a single token. 1 if it's not a tensor.
    size_t getNumElementsToken() {
      return iara::getNumElementsInToken(getTokenType());
    }

    // Returns size of chunk, in bytes.
    size_t getChunkSize() { return getRate() * getTokenSize(); }
  };

  EdgeInfo *getReverse(EdgeInfo &this_port) {
    auto table = (this_port.direction == SDFGraph::Direction::INCOMING)
                     ? &outputs
                     : &inputs;
    if (!this_port.other_node)
      return nullptr;
    auto &other_ports = (*table)[this_port.other_node];
    for (auto &port : other_ports) {
      if (port.edge_op == this_port.edge_op) {
        return &port;
      }
    }
    return nullptr;
  }

  // Represents a range of memory locations in the memory pool.
  struct PoolSlice {
    Type type;
    // PoolSlice *representative = nullptr;
    size_t size = 0; // measured in bytes
    // unsigned offset_in_chunk = 0; // measured in bytes
    size_t offset = 0; // measured in bytes
    // PoolSlice *adjacent_next = nullptr;
    // PoolSlice *adjacent_prev = nullptr;
    // EdgeInfo *edge_info;
    // //    DenseSet<std::shared_ptr<PoolSlice>> exclusions;
    // mlir::Value firing_argument;
    // bool has_been_consumed = false;
    // bool symb_exec_removed_from_pool = false;
    void setUnused() { offset = SIZE_MAX; }
    bool isUnused() { return offset == SIZE_MAX; }
    bool isVacancy() { return type == nullptr; }

    void dump() {
      llvm::errs() << llvm::formatv("|size: {0}, offset:{1}, type:{2}|", size,
                                    offset, type);
    }
  };

  // A node and a list of arguments for its function call.
  struct Firing {
    NodeOp node;
    SmallVector<Value> memory_args;
    SmallVector<PoolSlice> input_meg_objects;
    SmallVector<PoolSlice> output_meg_objects;
  };

  struct MemoryPool {

    // A linked list of MEGObjects. Vacancies are represented by MEGObjects with
    // type = nullptr. The last item of the list cannot be a vacancy.

    using PoolSlice = SDFGraph::PoolSlice;

    std::list<PoolSlice> nodes;
    size_t max_size = 0;

    using Iterator = std::list<PoolSlice>::iterator;

    Iterator insert_first_fit(PoolSlice object) {
      auto rv = nodes.begin();
      // Find first vacancy that fits object.
      auto fit = llvm::find_if(nodes, [&](PoolSlice vacancy) {
        return vacancy.type == nullptr && vacancy.size >= object.size;
      });
      if (fit == nodes.end()) { // no vacancy, add to end.
        size_t offset = 0;
        if (!nodes.empty())
          offset = nodes.back().offset + nodes.back().size;
        object.offset = offset;
        nodes.push_back(object);
        rv = --nodes.end();
        max_size = std::max(max_size, rv->offset + rv->size);
      } else {
        // Insert the new object at the start of the empty vacancy.
        object.offset = fit->offset;
        rv = nodes.insert(fit, object);
        fit->offset += object.size;
        fit->size -= object.size;
        if (fit->size == 0) {
          nodes.erase(fit);
        }
      }

      // llvm::errs() << "pushing pool:\n";
      // dump();
      return rv;
    }

    // Object must match the beginning of the allocated node.

    // |----------------|------------------------|-----------------|
    // |   NODE 1       |           NODE 2       |       NODE 3    |
    // +----------------+------------------------+-----------------+
    //                  |    OBJ   |
    //                  +----------+

    //                       |
    //                       V

    // |----------------|----------+-------------|-----------------|
    // |   NODE         |   EMPTY  |   NODE 4    |       NODE 4    |
    // |----------------|----------+-------------|-----------------|

    void free(PoolSlice to_free) {

      if (to_free.size == 0)
        return;

      auto node = llvm::find_if(nodes, [&](PoolSlice object) {
        return object.offset == to_free.offset;
      });

      if (node == nodes.end()) {
        llvm_unreachable("Tried to free unknown node");
      }

      assert(node->size >= to_free.size);

      size_t remaining_size = node->size - to_free.size;

      if (remaining_size == 0) {
        free(node);
      } else {
        auto new_obj = PoolSlice{nullptr, to_free.size, to_free.offset};
        auto new_node_it = nodes.insert(node, new_obj);
        node->offset += to_free.size;
        node->size -= to_free.size;
        free(new_node_it);
      }

      // llvm::errs() << "freeing pool:\n";
      // dump();
    }

    void free(std::list<PoolSlice>::iterator node) {
      node->type = nullptr;
      if (node != nodes.begin()) {
        auto predecessor = node;
        predecessor--;
        if (predecessor->type == nullptr) {
          node = merge(predecessor, node);
        }
      }
      if (node != --nodes.end()) {
        auto sucessor = node;
        sucessor++;
        if (sucessor->type == nullptr) {
          node = merge(node, sucessor);
        }
      }
      if (node == --nodes.end()) {
        nodes.erase(node); // last node can't be empty
      }
    }

    // Expects both nodes to be free, adjacent and in the given order. Updates
    // both iterators to point to the merged node.
    std::list<PoolSlice>::iterator merge(std::list<PoolSlice>::iterator l,
                                         std::list<PoolSlice>::iterator r) {
      assert(l->type == nullptr);
      assert(r->type == nullptr);
      auto l_successor = l;
      l_successor++;
      auto r_predecessor = r;
      r_predecessor--;
      assert(l == r_predecessor && r == l_successor);
      l->size += r->size;
      nodes.erase(r);
      return l;
    }

    void dump() {
      llvm::errs() << "Dumping memory pool:\n";
      for (auto node : nodes) {
        llvm::errs() << llvm::formatv("|size: {0}, offset:{1}, type:{2}|\n",
                                      node.size, node.offset, node.type);
      }
    }
  };

  struct MEGFifo {
    std::deque<SDFGraph::PoolSlice> buffer;
    size_t total_size = 0; // in bytes

    void push(SDFGraph::PoolSlice obj) {
      total_size += obj.size;
      if (!buffer.empty() &&
          obj.offset == buffer.back().offset + buffer.back().size) {
        buffer.back().size += obj.size;
        return;
      }
      buffer.push_back(obj);
    }

    // Attempts to provide a sequence of adjacent MEGObjects with given total
    // size.
    SDFGraph::PoolSlice pop(size_t size) {
      assert(size <= total_size);

      if (size == 0) {
        return SDFGraph::PoolSlice{};
      }

      SDFGraph::PoolSlice rv;
      unsigned remaining_size = size;

      assert(!buffer.empty());

      if (buffer.front().size < size) {
        llvm_unreachable("");
      }

      rv = buffer.front();
      rv.size = size;
      buffer.front().size -= size;
      buffer.front().offset += size;

      if (buffer.front().size == 0) {
        buffer.pop_front();
      }
      total_size -= size;

      return rv;
    }

    void dump() {
      llvm::errs() << "Dumping fifo:\n";
      for (auto &slice : buffer) {
        slice.dump();
        llvm::errs() << "\n";
      }
    }
  };

  // STORAGE

  ModuleOp module_op;
  GraphOp graph_op;
  OpBuilder globals_builder;
  OpBuilder module_builder;
  FuncOp run_function;
  OpBuilder run_builder;
  FuncOp init_function;
  OpBuilder init_builder;

  SmallVector<NodeOp> node_ops;
  SmallVector<EdgeOp> edge_ops;
  DenseMap<KernelOp, FuncOp> actor_impls;
  llvm::SmallVector<KernelOp> kernel_ops;
  DenseMap<NodeOp, util::Rational> activations;
  std::map<NodeOp, llvm::SmallVector<EdgeInfo>> inputs, outputs;
  std::map<EdgeOp, mlir::Type> edge_datatypes;
  std::map<NodeOp, llvm::SmallVector<arith::ConstantOp>> parameter_arguments;
  std::map<std::pair<NodeOp, StringRef>, SDFGraph::EdgeInfo> unused_data;
  DenseMap<NodeOp, FuncOp> node_impl_overrides;

  struct DelayData {
    memref::GlobalOp global_op;
    memref::GetGlobalOp get_global_op;
    int64_t num_tokens;
  };

  DenseMap<EdgeOp, DelayData> delay_data;

  using TokenRate = int64_t;
  using OutputCount = int64_t;
  DenseMap<std::tuple<mlir::Type, TokenRate, OutputCount>, mlir::func::FuncOp>
      simple_broadcast_impls;
  DenseMap<StringRef, mlir::func::FuncOp> complex_broadcast_impls;

  void runOnEachEdge(NodeOp node, EdgePred pred) {
    for (EdgeInfo edge : llvm::concat<EdgeInfo>(inputs[node], outputs[node])) {
      pred(node, edge.edge_op);
    }
  };

  NodeOp followEdge(NodeOp node, EdgeOp edge) {
    auto prod_node = llvm::dyn_cast<NodeOp>(lookupSymbol(edge, edge.prod()));
    auto cons_node = llvm::dyn_cast<NodeOp>(lookupSymbol(edge, edge.cons()));
    if (prod_node == node)
      return cons_node;
    if (cons_node == node)
      return prod_node;
    llvm_unreachable("Edge is not connected to node");
    return nullptr;
  };

  SmallVector<EdgeOp> getEdges(NodeOp node) {
    SmallVector<EdgeOp> rv;
    for (auto edge : util::concat(inputs[node], outputs[node])) {
      rv.push_back(edge.edge_op);
    }
    return rv;
  }

  mlir::Type getKernelPortType(int64_t rate, mlir::Type element_type) {
    if (auto tensor_type = element_type.dyn_cast<RankedTensorType>()) {
      auto buf_size = tensor_type.getNumElements() * rate;
      return mlir::MemRefType::get({buf_size}, tensor_type.getElementType());
    }
    return mlir::MemRefType::get({rate}, element_type);
  }

  SDFGraph(GraphOp graph)
      : graph_op(graph),
        globals_builder(OpBuilder::atBlockBegin(graph->getBlock())),
        module_builder(OpBuilder::atBlockEnd(graph->getBlock())),
        run_function(module_builder.create<mlir::func::FuncOp>(
            graph_op.getLoc(), "run", module_builder.getFunctionType({}, {}))),
        run_builder(OpBuilder::atBlockEnd(run_function.addEntryBlock())),
        init_function(module_builder.create<mlir::func::FuncOp>(
            graph_op.getLoc(), "init", module_builder.getFunctionType({}, {}))),
        init_builder(OpBuilder::atBlockEnd(init_function.addEntryBlock())) {

    module_op = graph->getParentOfType<ModuleOp>();

    auto run_block = &run_function.getBlocks().front();
    run_builder.create<func::ReturnOp>(graph_op.getLoc());
    run_builder = run_builder.atBlockBegin(run_block);

    auto init_block = &init_function.getBlocks().front();
    init_builder.create<func::ReturnOp>(graph_op.getLoc());
    init_builder = run_builder.atBlockBegin(init_block);

    for (auto &op : graph_op.getOps()) {
      auto actortype_op = dyn_cast<KernelOp>(&op);
      if (actortype_op)
        kernel_ops.push_back(actortype_op);
    }

    for (auto &op : graph_op.getOps()) {
      auto node_op = dyn_cast<NodeOp>(&op);
      if (node_op) {
        node_ops.push_back(node_op);
      }
    }

    for (auto &op : graph_op.getOps()) {
      auto edge_op = dyn_cast<EdgeOp>(&op);
      if (edge_op) {
        edge_ops.push_back(edge_op);
        auto graph_op = llvm::dyn_cast<GraphOp>(edge_op->getParentOp());
        auto prod_node = graph_op.lookupSymbol<NodeOp>(edge_op.prod());
        auto prod_port_name = edge_op.prod_port();
        auto cons_node = graph_op.lookupSymbol<NodeOp>(edge_op.cons());
        auto cons_port_name = edge_op.cons_port();

        auto prod_port = getPortAttr(prod_node, prod_port_name).getValue();
        auto cons_port = getPortAttr(cons_node, cons_port_name).getValue();

        int64_t prod_rate = getRateOfPort(prod_port);
        int64_t cons_rate = getRateOfPort(cons_port);

        inputs[cons_node].push_back(EdgeInfo{INCOMING, cons_port_name,
                                             prod_port_name, edge_op, cons_node,
                                             prod_node, cons_rate, prod_rate});
        outputs[prod_node].push_back(
            EdgeInfo{OUTGOING, prod_port_name, cons_port_name, edge_op,
                     prod_node, cons_node, prod_rate, cons_rate});
      }
    }

    instanceDelayConstants();

    // for (auto node : node_ops) {
    //   if (isSimpleBroadcast(node.implementation())) {
    // node->dump();
    //     llvm::errs() << "input rates:\n";
    //     for (auto input : inputs[node]) {
    //       llvm::errs() << input.this_rate << "\n";
    //     }
    //     llvm::errs() << "output rates:\n";
    //     for (auto output : outputs[node]) {
    //       llvm::errs() << output.this_rate << "\n";
    //     }
    //   }
    // }

    // order inputs and outputs as they are in the declarations

    for (auto node : node_ops) {
      llvm::SmallVector<EdgeInfo> ordered_inputs;
      llvm::SmallVector<EdgeInfo> ordered_outputs;
      llvm::SmallVector<llvm::StringRef> input_port_names;
      llvm::SmallVector<llvm::StringRef> output_port_names;

      // Order of outputs in broadcast node shouldn't matter.
      if (isSimpleBroadcast(node.implementation())) {
        assert(inputs[node].size() == 1 &&
               "Broadcast node must have 1 incoming edge");
        auto edge_info = inputs[node][0];
        ordered_inputs.push_back(edge_info);
        for (auto output : outputs[node]) {
          ordered_outputs.push_back(output);
        }
      }

      auto impl = lookupSymbol(node, node.implementation());

      if (impl) {
        auto actortype = llvm::cast<KernelOp>(impl);

        if (actortype.inputs()) {
          for (auto port : *actortype.inputs()) {
            auto name = port.cast<PortAttr>().getName().getValue();
            auto edge_info = llvm::find_if(inputs[node], [&](EdgeInfo &edge) {
              return edge.this_port_name == name;
            });
            if (edge_info != inputs[node].end()) {
              ordered_inputs.push_back(*edge_info);
            } else {
              node.emitError()
                  << "Input port <" << node.getName() << "." << name
                  << "> not connected to any edges; this function "
                     "can't be called.";
            }
          }
        }
        if (actortype.outputs()) {
          for (auto port : *actortype.outputs()) {
            auto name = port.cast<PortAttr>().getName().getValue();
            auto edge_info = llvm::find_if(outputs[node], [&](EdgeInfo &edge) {
              return edge.this_port_name == name;
            });
            if (edge_info != outputs[node].end()) {
              ordered_outputs.push_back(*edge_info);
            } else {
              node.emitWarning()
                  << "WARNING_UNUSED_OUTPUT: Output port <" << name
                  << "> not connected to any edges; the function "
                     "will write to scratch space.";
              auto unused_edge = EdgeInfo();
              unused_edge.direction = OUTGOING;
              unused_edge.this_node = node;
              unused_edge.this_port_name = name;
              unused_edge.this_rate = getRateOfPort(port.cast<PortAttr>());
              unused_edge.is_unused_output = true;
              ordered_outputs.push_back(unused_edge);
            }
          }
        }
      }
      inputs[node] = ordered_inputs;
      outputs[node] = ordered_outputs;
    }

    // create actor extern function declarations

    for (auto kernel_op : kernel_ops) {
      llvm::SmallVector<mlir::Type> inputs;

      if (lookupSymbol(module_op, kernel_op.sym_name()) or
          isComplexBroadcast(kernel_op)) {
        // function is not external.
        continue;
      }

      if (kernel_op.parameters())
        for (auto attr : *kernel_op.parameters()) {
          auto param = attr.dyn_cast<ParameterAttr>();
          if (!param) {
            continue;
          }
          auto name = param.getName();
          auto value = param.getValue();
          mlir::Type type = nullptr;
          if (value) {
            auto typeattr = value.dyn_cast<TypeAttr>();
            if (typeattr) {
              type = typeattr.getValue();
            } else {
              type = value.getType();
            }
          } else {
            // type is null; check graph defaults
            if (graph_op.param_defaults()) {
              for (auto attr : graph_op.param_defaults().getValue()) {
                auto param = attr.dyn_cast<ParameterAttr>();
                if (param.getName() != name) {
                  continue;
                }
                auto value = param.getValue();
                type = value.getType();
                break;
              }
            }
          }
          if (!type) {
            continue;
          }
          inputs.push_back(type);
        }

      for (auto &list : {kernel_op.inputs(), kernel_op.outputs()})
        if (list)
          for (Attribute const attr : *list) {
            auto port = attr.dyn_cast<PortAttr>();
            if (!port) {
              kernel_op->emitError() << "Unrecognized port";
              continue;
            }
            // TODO: handle cases where the token is a fixed-size tensor
            auto type = port.getDatatype();
            int64_t rate = port.getRate()
                               .dyn_cast<IntegerAttr>()
                               .getValue()
                               .getLimitedValue();
            if (!type) {
              kernel_op->emitError() << "Unrecognized port datatype";
              continue;
            }
            llvm::SmallVector<int64_t> shape = {rate};
            // llvm::errs() << "<<Type>>: " << type << "\n";
            inputs.push_back(getKernelPortType(rate, type));
          }

      auto name = getNamespaceSuffix(kernel_op.getName());

      // llvm::outs() << name << "\n";

      auto attrs = util::makeDictAttr(
          globals_builder,
          {{"sym_name", globals_builder.getStringAttr(name)},
           {"sym_visibility", globals_builder.getStringAttr("private")},
           {"function_type",
            mlir::TypeAttr::get(
                globals_builder.getFunctionType(mlir::TypeRange(inputs), {}))},
           {"llvm.emit_c_interface", globals_builder.getUnitAttr()}});

      auto func_op = //
          globals_builder.create<mlir::func::FuncOp>(
              kernel_op.getLoc(),
              // mlir::FuncOp::build(, ,
              //
              mlir::TypeRange(), mlir::ValueRange(), attrs.getValue());

      actor_impls[kernel_op] = func_op;
    }
  };

  FuncOp getOrCreateSimpleBroadcastKernel(NodeOp node) {
    assert(isSimpleBroadcast(node.implementation()));
    auto port_attr = getSimpleBroadcastPortAttr(node);
    TokenRate rate = getRateOfPort(port_attr);
    mlir::Type element_type = port_attr.getDatatype();
    OutputCount num_outputs = outputs[node].size();

    if (auto existing =
            simple_broadcast_impls.lookup({element_type, rate, num_outputs})) {
      return existing;
    }

    auto func_name = std::string{};
    auto ss = llvm::raw_string_ostream(func_name);
    ss << "BROADCAST_" << element_type << "x" << rate << "_" << num_outputs
       << "_OUTPUTS";

    auto memref_type = MemRefType::get({rate}, element_type);

    SmallVector<Type> input_types{};

    for (int i = 0; i < num_outputs + 1; i++) {
      input_types.push_back(memref_type);
    }

    auto func_type = module_builder.getFunctionType(input_types, {});

    FuncOp rv = module_builder.create<mlir::func::FuncOp>(
        // mlir::func::FuncOp::build(,
        node.getLoc(), StringRef(func_name), func_type,
        module_builder.getStringAttr("private"));

    simple_broadcast_impls[{element_type, rate, num_outputs}] = rv;

    SmallVector<mlir::Location> arg_locs;

    assert(inputs[node].size() == 1);
    arg_locs.push_back(inputs[node][0].edge_op.getLoc());

    for (auto edge_info : outputs[node]) {
      arg_locs.push_back(edge_info.edge_op.getLoc());
    }

    auto block = module_builder.createBlock(&rv.getBody(), rv.getBody().end(),
                                            input_types, arg_locs);

    auto block_builder = OpBuilder::atBlockBegin(block);

    auto input_arg = rv.getArguments().front();
    for (auto output_arg : rv.getArguments().drop_front(1)) {
      auto copyop = block_builder.create<mlir::memref::CopyOp>(
          output_arg.getLoc(),
          // memref::CopyOp::build(, ,
          input_arg, output_arg);
      // copyop.dump();
    }

    block_builder.create<func::ReturnOp>(node.getLoc());

    return rv;
  }

  LogicalResult generateBroadcastKernels() {
    for (auto node : node_ops) {
      if (isSimpleBroadcast(node.implementation())) {
        node_impl_overrides[node] = getOrCreateSimpleBroadcastKernel(node);
        continue;
      }
      auto kernel = cast<KernelOp>(lookupSymbol(node, node.implementation()));

      if (kernel && isComplexBroadcast(kernel)) {
        node_impl_overrides[node] =
            getOrCreateComplexBroadcastKernelImpl(kernel);
      }
    }
    return success();
  }

  LogicalResult precomputeParameters() {
    for (auto node : node_ops) {
      if (isSimpleBroadcast(node.implementation())) {
        return success();
      }

      auto impl = lookupSymbol(node, node.implementation());
      auto kernel = llvm::cast<KernelOp>(impl);

      if (kernel.parameters())
        for (auto param_attr : *kernel.parameters()) {
          auto param = param_attr.dyn_cast<ParameterAttr>();

          IntegerAttr value =
              resolveParameter(node, param).dyn_cast_or_null<IntegerAttr>();
          if (!value) {
            return util::error(node.emitError()
                               << "Can't resolve parameter " << param);
          }

          auto constant =
              run_builder.create<arith::ConstantOp>(node.getLoc(), value);
          parameter_arguments[node].push_back(constant);
        }
    }
    return success();
  }

  SmallVector<Value> getParameterArguments(NodeOp node_op) {
    SmallVector<Value> rv;
    for (auto op : parameter_arguments[node_op])
      rv.push_back(op.getResult());
    return rv;
  }

  // Check if graph is admissible: every port has a constant rate, and the rates
  // are consistent in the case of cycles.
  // TODO: implement delays.
  LogicalResult checkAdmissible() {

    // Check that every port has a constant rate

    for (auto actortype_op : kernel_ops) {

      auto has_constant_rate = [&](Attribute attr) -> bool {
        auto port = attr.dyn_cast<PortAttr>();
        if (!port) {
          actortype_op->emitError() << "Invalid port attribute";
          return false;
        }
        if (!port.getRate()) {
          actortype_op->emitError() << "Port is missing rate.";
          return false;
        }
        if (port.getRate().isa<IntegerAttr>()) {
          return true;
        }
        actortype_op->emitError()
            << "Port " << port.getName() << " does not have constant rate";
        return false;
      };

      for (auto &array : {actortype_op.inputs(), actortype_op.outputs()}) {
        if (array && !llvm::all_of(*array, has_constant_rate)) {
          return util::error(actortype_op->emitError()
                             << "Some of the ports of kernel <"
                             << actortype_op.getName() << "> are invalid.");
        }
      }
    }
    // find a source
    NodeOp source;

    for (auto node : node_ops) {
      auto actortype = llvm::dyn_cast<GraphOp>(node->getParentOp())
                           .lookupSymbol<KernelOp>(node.implementation());
      if (!actortype.inputs() || actortype.inputs()->empty()) {
        source = node;
        break;
      }
      continue;
    }

    if (!source) {
      graph_op->emitError() << "Graph has no source nodes";
    }

    // Set this source's number of activations per period to 1.

    activations[source] = 1;

    // Walk graph, setting activations based on ratios of production and
    // consumption rates.
    //
    // Here, a node's activation rate is how often it is fired, per period, in
    // proportion to the source node.
    //
    // If the DFS finds a neighbor with an already-set activation that doesn't
    // match the one expected by the ratio of the edge's ports, the graph is
    // inconsistent.

    std::vector<NodeOp> stack = {source}; // DFS
    std::set<int64_t> denominators;

    while (!stack.empty()) {
      auto current_node = stack.back();
      stack.pop_back();
      // llvm::errs() << llvm::formatv("Visiting node {0}\n",
      //                               current_node.getName());

      auto edges = util::concat(inputs[current_node], outputs[current_node]);

      for (SDFGraph::EdgeInfo &edge : edges) {
        if (edge.is_unused_output) {
          continue;
        }
        // llvm::errs() << llvm::formatv("Visiting neighbor {0}\n",
        //                               edge.other_node.getName());
        auto neighbor_activation =
            (activations[current_node] *
             util::Rational{edge.this_rate, edge.other_rate})
                .normalized();
        auto existing_activation = activations.find(edge.other_node);
        if (existing_activation == activations.end()) {
          activations[edge.other_node] = neighbor_activation;
          // llvm::errs() << llvm::formatv(
          //     "activation of {0} ({1}): {2}\n", edge.other_node.getName(),
          //     edge.other_node.implementation().getLeafReference(),
          //     neighbor_activation);
          denominators.emplace(neighbor_activation.denom);
          stack.push_back(edge.other_node);
        } else if (existing_activation->second != neighbor_activation) {
          llvm::errs() << "Error!\n";
          for (auto &[k, v] : activations) {
            llvm::errs() << llvm::formatv(
                "activation of {0} ({1}): {2}\n", NodeOp(k).getName(),
                NodeOp(k).implementation().getLeafReference(), v);
          }
          return util::error(
              graph_op->emitError() << llvm::formatv(
                  "SDF Graph has inconsistent rates: expected node {0} to have "
                  "activation {1}; found activation {2} through port {3}",
                  edge.other_node.getName(), existing_activation->second,
                  neighbor_activation, edge.other_port_name));
        }
      };
    }

    // The activations are currently normalized to the first node.
    // Multiply all of them so that all of the activations are whole numbers.

    int64_t multiplier =
        std::accumulate(denominators.begin(), denominators.end(), 1, mlir::lcm);

    for (auto &[node, activation] : activations) {
      activation = activation * multiplier;
      activation = activation.normalized();
      assert(activation.denom == 1);
      // llvm::errs() << "Activation of " << node.getName() << " "
      //              << activation.num << "\n";
    }

    // Count number of nodes in the graph to assert connectedness.

    size_t node_count = node_ops.size();

    if (activations.size() != node_count) {
      return util::error(graph_op->emitError() << "SDF graph is unconnected.");
    }

    return success();
  }

  FuncOp getOrCreateComplexBroadcastKernelImpl(KernelOp kernel) {
    assert(isComplexBroadcast(kernel));

    assert(kernel.inputs().getValue().size() == 1);

    auto input_port = kernel.inputs().getValue()[0].cast<PortAttr>();

    auto input_rate = getRateOfPort(input_port);
    auto token_type = input_port.getDatatype();
    auto element_type = getTokenElementType(token_type);
    auto token_size = getTokenSize(token_type);
    auto num_elems_in_token = getNumElementsInToken(token_type);
    int64_t num_elems_in_chunk = num_elems_in_token * input_rate;

    std::string name = "__BROADCAST_";
    SmallVector<int64_t> output_multipliers;

    auto ss = llvm::raw_string_ostream(name);
    ss << llvm::formatv("{0}", element_type) << "x"
       << (num_elems_in_token * input_rate);
    for (auto &output : kernel.outputs().getValue()) {
      auto output_port = output.cast<PortAttr>();
      auto output_rate = getRateOfPort(output_port);
      assert(output_rate % input_rate == 0);
      auto multiplier = output_rate / input_rate;
      output_multipliers.push_back(multiplier);
      ss << "_" << multiplier;
    }

    if (auto existing = complex_broadcast_impls.lookup(name)) {
      return existing;
    }

    auto [func, body_builder] =
        std::move(createEmptyFunc(kernel, name, module_builder));

    module_builder = module_builder.atBlockEnd(module_op.getBody());

    auto input_arg = func.getArguments().front();

    auto input_memref_type = input_arg.getType().cast<MemRefType>();

    for (auto [output_arg, multiplier] :
         llvm::zip(func.getArguments().drop_front(1), output_multipliers)) {
      auto chunk_size = num_elems_in_token;
      for (int i = 0; i < multiplier; i++) {
        SmallVector<int64_t> offsets = {num_elems_in_chunk * i};
        SmallVector<int64_t> sizes = {num_elems_in_chunk};
        SmallVector<int64_t> strides = {1};
        auto subview = body_builder.create<memref::SubViewOp>(
            kernel.getLoc(), output_arg, offsets, sizes, strides);
        ;

        auto copy_op = body_builder.create<mlir::memref::CopyOp>(
            output_arg.getLoc(),
            // memref::CopyOp::build(, ,
            input_arg, subview.getResult());
        // copy_op.dump();
      }
    }

    complex_broadcast_impls[name] = func;
    actor_impls[kernel] = func;
    // func.dump();
    return func;
  }

  void instanceDelayConstants() {
    for (auto edge_op : edge_ops) {
      if (auto duration_attr = edge_op->getAttr("delay_duration")
                                   .dyn_cast_or_null<IntegerAttr>()) {
        auto data = DelayData{};
        data.num_tokens = duration_attr.getInt();

        auto init_values_str =
            edge_op->getAttr("delay_values").dyn_cast_or_null<ArrayAttr>();

        if (init_values_str) {

          SmallVector<Attribute> init_values;
          EdgeInfo *edge_info_p;
          for (auto &input :
               inputs[cast<NodeOp>(lookupSymbol(edge_op, edge_op.cons()))]) {
            if (input.edge_op == edge_op)
              edge_info_p = &input;
          }

          EdgeInfo &edge_info = *edge_info_p;

          for (auto attr : init_values_str) {
            auto str = attr.dyn_cast<StringAttr>().getValue();
            init_values.push_back(util::stringToAttr(
                str, edge_info.getTokenElementType(), globals_builder));
          }

          auto global_memref_type = mlir::MemRefType::get(
              {data.num_tokens * (int64_t)edge_info.getNumElementsToken()},
              edge_info.getTokenElementType());

          auto tensor_type = mlir::RankedTensorType::get(
              {data.num_tokens * (int64_t)edge_info.getNumElementsToken()},
              edge_info.getTokenElementType());

          auto init_value =
              mlir::DenseElementsAttr::get(tensor_type, init_values);

          std::string name = llvm::formatv("{0}_DELAYS", edge_op.getName());

          data.global_op = globals_builder.create<mlir::memref::GlobalOp>(
              // mlir::memref::GlobalOp::build(
              graph_op.getLoc(), name, globals_builder.getStringAttr("private"),
              global_memref_type, init_value, false, nullptr);
          data.get_global_op = init_builder.create<mlir::memref::GetGlobalOp>(
              graph_op.getLoc(), global_memref_type, data.global_op.sym_name());
        }
        delay_data[edge_op] = data;
      }
    }
  }

  std::string opToString(Operation *op) { return llvm::formatv("{0}", op); }

  // MEG bufferization: Bufferize by reusing memory of past firings. Uses
  // algorithm from Karol Desnos's PHD thesis
  // (https://www.theses.fr/2014ISAR0004, chapter 4).
  //
  // If it runs without conversion to Single Rate, overlapping firings will be
  // allocated to the same memory object.
  // For instance, on the graph
  //
  // +---+                    +---+
  // | A | (2) ---------> (3) | B |
  // +---+                    +---+
  //
  //             ||
  //             ||
  //            \  /
  //             \/
  // +----+
  // | A1 |                    +----+
  // +----+ (2) ---------> (2) | B1 |
  // +----+ (1) ---------> (1) +----+
  // | A2 |
  // +----+ (1) ---------> (1) +----+
  // +----+                    | B2 |
  // | A3 | (2) ---------> (2) +----+
  // +----+
  //
  // all tokens will be assigned to the same memory object, as A1A2->B1 overlaps
  // with A2A3->B2 (A2 requires that its two output tokens be contiguous.)

  LogicalResult scheduleAndBufferizeMemoryPool() {

    std::map<NodeOp, int64_t> remaining_executions;
    DenseMap<EdgeOp, MEGFifo> meg_fifos;
    DenseMap<EdgeOp, MEGFifo> prealloc_buffer_fifos;
    DenseMap<EdgeOp, int64_t> remaining_delay;

    auto dumpNodeStates = [&]() {
      for (auto &[node, inputs] : inputs) {
        auto n = node;
        llvm::errs() << "Node " << n.getName() << ":\n";
        for (auto &input : inputs) {
          llvm::errs() << llvm::formatv(
              "   {0}: {1}/{2}\n", input.this_port_name,
              meg_fifos[input.edge_op].total_size, input.getChunkSize());
        }
      }
    };

    for (auto &[edge_op, data] : delay_data) {
      remaining_delay[edge_op] = data.num_tokens;
    }

    for (auto &[_, edges] : outputs) {
      for (auto &info : edges) {
        if (not info.is_unused_output) {
          meg_fifos[info.edge_op] = {};
          prealloc_buffer_fifos[info.edge_op] = {};
        }
      }
    }

    MemoryPool pool;

    size_t total_token_count = 0;

    SmallVector<Firing> firings;

    for (auto &[node, activation] : activations) {
      for (auto output : outputs[node]) {
        total_token_count += output.this_rate;
      }
      remaining_executions[node] = activation.num;

      // Check and remove activations that are completely covered by delays.

      SmallVector<int64_t> output_firings;
      int64_t active_outputs = 0;
      for (auto &edge : outputs[node]) {
        if (edge.is_unused_output) {
          continue;
        }
        active_outputs++;
        if (auto it = delay_data.find(edge.edge_op); it != delay_data.end()) {
          auto &[node_op, delay] = *it;
          assert(edge.getRate() > 0);
          auto firings = delay.num_tokens / edge.getRate();
          output_firings.push_back(firings);
        } else {
          break;
        }
      }
      if (output_firings.size() < active_outputs) {
        continue;
      }
      if (output_firings.empty()) {
        continue;
      }
      auto firings_to_save =
          *std::min_element(output_firings.begin(), output_firings.end());
      remaining_executions[node] -= firings_to_save;
      // llvm::errs() << "Activations of " << node.getName() << " lowered from "
      //              << (remaining_executions[node] + firings_to_save) << " to
      //              "
      //              << remaining_executions[node] << "\n";
    }

    auto can_execute = [&](NodeOp node) {
      if (remaining_executions.count(node) == 0) {
        return false;
      }
      for (auto &incoming_port : inputs[node]) {
        auto edge = incoming_port.edge_op;
        auto chunk_size = incoming_port.getChunkSize();
        if (meg_fifos[edge].total_size < chunk_size) {
          return false; // has unavailable input
        }
      }
      return true;
    };

    auto grow_prealloc_fifo = [&](SDFGraph::EdgeInfo &edge_info) {
      auto new_chunk =
          PoolSlice{edge_info.getTokenElementType(),
                    (size_t)edge_info.getTokenSize() *
                        std::lcm(edge_info.this_rate, edge_info.other_rate),
                    0};
      // llvm::errs() << llvm::formatv("Allocating chunk for edge {0}:\n",
      //                               edge_info.edge_op);
      auto fit = pool.insert_first_fit(new_chunk);
      prealloc_buffer_fifos[edge_info.edge_op].push(*fit);
    };

    // Copy delays into fifos.

    SmallVector<std::pair<EdgeInfo, PoolSlice>> delays_to_copy;

    for (auto &[node_op, edges] : outputs) {
      for (auto &edge_data : edges) {
        if (auto delay_it = delay_data.find(edge_data.edge_op);
            delay_it != delay_data.end()) {
          auto edge_op = edge_data.edge_op;
          auto delay = delay_it->second;
          auto delay_size = delay.num_tokens * edge_data.getTokenSize();
          assert(not edge_data.is_unused_output);
          auto edge = edge_data.edge_op;
          auto &prealloc_fifo = prealloc_buffer_fifos[edge];
          while (prealloc_fifo.total_size < delay_size) {
            grow_prealloc_fifo(edge_data);
          }
          auto meg = prealloc_fifo.pop(delay_size);
          delays_to_copy.push_back({edge_data, meg});
          meg_fifos[edge_op].push(meg);
        }
      }
    }

    NodeOp last_node;

    auto choose_node = [&]() -> NodeOp {
      // Give precedence to nodes with inputs.
      // dumpNodeStates();

      auto choose_if_last_or_any = [&](SmallVector<NodeOp> &nodes) -> NodeOp {
        for (auto node : nodes) {
          if (last_node == node) {
            return node;
          }
        }
        return nodes.front();
      };

      SmallVector<NodeOp> nodes_with_inputs;
      SmallVector<NodeOp> nodes_with_incomplete_outputs;
      SmallVector<NodeOp> nodes_with_outputs;

      for (auto &[node, edges] : inputs) {
        if (!edges.empty() and can_execute(node)) {
          nodes_with_inputs.push_back(node);
        }
      }

      if (not nodes_with_inputs.empty())
        return choose_if_last_or_any(nodes_with_inputs);

      for (auto &[node, edges] : outputs) {
        if (edges.empty() || not can_execute(node))
          continue;
        auto has_incomplete_outputs = false;
        for (auto &edge : edges) {
          if (edge.is_unused_output)
            continue;
          if (meg_fifos[edge.edge_op].total_size <
              getReverse(edge)->getChunkSize()) {
            has_incomplete_outputs = true;
            break;
          }
        }
        if (has_incomplete_outputs) {
          nodes_with_incomplete_outputs.push_back(node);
        }
      }

      if (not nodes_with_incomplete_outputs.empty())
        return choose_if_last_or_any(nodes_with_incomplete_outputs);

      for (auto &[node, edges] : outputs) {
        if (can_execute(node)) {
          nodes_with_outputs.push_back(node);
        }
      }

      if (not nodes_with_outputs.empty())
        return choose_if_last_or_any(nodes_with_outputs);

      util::error(graph_op.emitError() << "No node can be executed.");
      llvm_unreachable("error");
      return nullptr;
    };

    int64_t total_activations = 0;
    int64_t activations_scheduled = 0;
    for (auto activation : activations) {
      total_activations += activation.second.num;
    }

    while (!remaining_executions.empty()) {
      auto node = choose_node();
      auto node_name = node.getName();
      last_node = node;
      // llvm::errs() << "Chose node ";
      // node->dump();

      // for (auto &[edge_op, fifo] : meg_fifos) {
      //   llvm::errs() << "Fifo for ";
      //   edge_op.dump();
      //   fifo.dump();
      // }

      SmallVector<Value> args = getParameterArguments(node);

      remaining_executions[node]--;
      if (remaining_executions[node] == 0) {
        remaining_executions.erase(node);
      }

      SmallVector<PoolSlice> input_meg_objs, output_meg_objs,
          unused_output_meg_objs;

      llvm::errs() << "Execute " << node.getName() << "( ";

      // Insert output objects into pool before removing input objects.

      // OUTPUT OBJECTS
      for (auto &outgoing_port : outputs[node]) {
        auto chunk_size = outgoing_port.getChunkSize();
        auto edge = outgoing_port.edge_op;
        if (not outgoing_port.is_unused_output) {
          auto &output_fifo = prealloc_buffer_fifos[edge];
          auto &meg_fifo = meg_fifos[edge];
          // Objects are pool-allocated in chunks of size = the lcm of the
          // production and consumption rates, to make sure that memory is
          // contiguous when overlapping
          if (output_fifo.total_size == 0) {
            grow_prealloc_fifo(outgoing_port);
          }
          assert(output_fifo.total_size >= chunk_size);
          auto meg = output_fifo.pop(chunk_size);
          meg_fifos[edge].push(meg);
          output_meg_objs.push_back(meg);
          llvm::errs() << outgoing_port.this_port_name << " " << meg.size << "@"
                       << meg.offset << " ";
        } else {
          auto fit = pool.insert_first_fit(
              PoolSlice{outgoing_port.getTokenElementType(), chunk_size, 0});
          unused_output_meg_objs.push_back(*fit);
          output_meg_objs.push_back(*fit);
        }
      }

      // INPUT OBJECTS
      for (auto &incoming_port : inputs[node]) {
        auto edge = incoming_port.edge_op;
        auto rate = incoming_port.getRate();
        auto chunk_size = incoming_port.getChunkSize();
        auto obj = meg_fifos[edge].pop(chunk_size);
        llvm::errs() << incoming_port.this_port_name << " " << obj.size << "@"
                     << obj.offset << " ";

        input_meg_objs.push_back(obj);
        pool.free(obj);
      }
      llvm::errs() << " )\n";
      // Free unused output ports.
      for (auto obj : unused_output_meg_objs) {
        pool.free(obj);
      }

      firings.push_back(Firing{node, args, input_meg_objs, output_meg_objs});
      activations_scheduled++;
      llvm::errs() << "Schedule: " << activations_scheduled << "/"
                   << total_activations << "\n";
    }

    // create global buffer

    auto global_tensor_type = mlir::RankedTensorType::get(
        {(int64_t)pool.max_size}, run_builder.getIntegerType(8));
    auto global_memref_type = mlir::MemRefType::get(
        {(int64_t)pool.max_size}, run_builder.getIntegerType(8));

    std::string memory_pool_name = "__SharedMemoryPool__";
    auto pool_global_op = globals_builder.create<mlir::memref::GlobalOp>(
        graph_op.getLoc(), memory_pool_name,
        globals_builder.getStringAttr("private"), global_memref_type,
        globals_builder.getUnitAttr(), false, nullptr);
    auto run_pool_get_global_op = run_builder.create<mlir::memref::GetGlobalOp>(
        graph_op.getLoc(), global_memref_type, pool_global_op.sym_name());
    auto init_pool_get_global_op =
        init_builder.create<mlir::memref::GetGlobalOp>(
            graph_op.getLoc(), global_memref_type, pool_global_op.sym_name());

    auto view_from_pool_slice = [&](PoolSlice ref, mlir::Location loc,
                                    memref::GetGlobalOp pool_get_global_op,
                                    OpBuilder &builder) -> memref::ViewOp {
      int64_t size = ref.size;
      int64_t offset = ref.offset;
      Type element_type = ref.type;

      assert(!element_type.isa<RankedTensorType>());

      SmallVector<int64_t> offsets = {offset};
      SmallVector<int64_t> strides = {1};
      SmallVector<int64_t> sizes_in_bytes = {size};
      SmallVector<int64_t> sizes_in_elements = {
          size / (element_type.getIntOrFloatBitWidth() / 8)};

      auto byteshift_op = builder.create<arith::ConstantIndexOp>(loc, offset);
      // auto const_size_op = run_builder.create<arith::ConstantIndexOp>(
      //     firing.node.getLoc(), size / token_size);

      auto view_op = builder.create<memref::ViewOp>(
          loc, MemRefType::get(sizes_in_elements, element_type),
          pool_get_global_op.getResult(), byteshift_op.getResult(),
          mlir::ValueRange(/*const_size_op.getResult()*/));

      return view_op;
    };

    for (auto &firing : firings) {

      auto kernel = dyn_cast_or_null<KernelOp>(
          lookupSymbol(firing.node, firing.node.implementation()));

      size_t num_parameters = 0;
      if (kernel && kernel.parameters())
        num_parameters = kernel.parametersAttr().size();

      size_t num_input_ports = 0;
      if (kernel && kernel.inputs())
        num_input_ports += kernel.inputs()->size();
      else if (isSimpleBroadcast(firing.node.implementation())) {
        num_input_ports = 1;
      }

      assert(firing.memory_args.size() == num_parameters);

      // Create view ops for offsets.

      for (auto &port_tokens :
           util::concat(firing.input_meg_objects, firing.output_meg_objects)) {

        auto view_op =
            view_from_pool_slice(port_tokens, firing.node.getLoc(),
                                 run_pool_get_global_op, run_builder);
        firing.memory_args.push_back(view_op.getResult());
      }
    }

    // Copy delays into global buffer.

    // TODO: Generate constants directly in place to avoid copy.
    for (auto &[edge, meg] : delays_to_copy) {
      // TODO: pass location of the delay in dif source code
      auto view_op = view_from_pool_slice(
          meg, edge.edge_op.getLoc(), init_pool_get_global_op, init_builder);

      if (delay_data[edge.edge_op].get_global_op) {
        auto copy_op = init_builder.create<memref::CopyOp>(
            edge.edge_op.getLoc(),
            delay_data[edge.edge_op].get_global_op.getResult(), view_op);
      } else {
        // If no data was provided, set everything to zero.
        auto &data = delay_data[edge.edge_op];
        auto start = init_builder.create<arith::ConstantIndexOp>(
            // arith::ConstantIndexOp::build(,
            edge.edge_op.getLoc(), 0);
        auto end = init_builder.create<arith::ConstantIndexOp>(
            edge.edge_op.getLoc(),
            edge.getNumElementsToken() * data.num_tokens);
        auto step = init_builder.create<arith::ConstantIndexOp>(
            edge.edge_op.getLoc(), 1);
        auto zero = init_builder.create<arith::ConstantOp>(
            edge.edge_op.getLoc(), edge.getTokenElementType(),
            util::stringToAttr("0", edge.getTokenElementType(), init_builder));
        auto loop = init_builder.create<scf::ForOp>(
            // scf::ForOp::build(,
            edge.edge_op.getLoc(), start.getResult(), end.getResult(),
            step.getResult());
        auto memset_builder = init_builder.atBlockBegin(loop.getBody());
        // view_op.dump();
        // llvm::errs() << loop.getNumOperands() << "\n";

        auto store = init_builder.atBlockBegin(loop.getBody())
                         .create<memref::StoreOp>(
                             edge.edge_op.getLoc(), zero.getResult(), view_op,
                             loop.getBody()->getArguments());
      }
    }

    // Create call ops for firings.

    for (auto firing : firings) {
      auto node = firing.node;

      auto arguments = firing.memory_args;

      auto func = node_impl_overrides[node];

      if (!func) {
        // get parameter values
        auto impl = lookupSymbol(node, node.implementation());
        auto kernel = llvm::cast<KernelOp>(impl);
        func = actor_impls.lookup(kernel);
        assert(func);
      }
      run_builder.create<func::CallOp>(node.getLoc(), func, arguments);
    }

    // Copy remaining values in feedback edges back to start of nodes,
    // to preserve loop invariant

    for (auto &[edge, target_meg] : delays_to_copy) {

      // TODO: check properly if it's a feedback edge
      // assert(meg_fifos[edge.edge_op].total_size == target_meg.size);
      if (meg_fifos[edge.edge_op].total_size != target_meg.size) {
        continue;
      }

      // TODO: pass location of the delay in dif source code
      auto target_view_op =
          view_from_pool_slice(target_meg, edge.edge_op.getLoc(),
                               run_pool_get_global_op, run_builder);
      auto source_meg = meg_fifos[edge.edge_op].pop(target_meg.size);
      assert(meg_fifos[edge.edge_op].total_size == 0);
      meg_fifos[edge.edge_op].push(target_meg);
      auto source_view_op =
          view_from_pool_slice(source_meg, edge.edge_op.getLoc(),
                               run_pool_get_global_op, run_builder);
      auto copy_op = run_builder.create<memref::CopyOp>(
          edge.edge_op.getLoc(), source_view_op, target_view_op);
      // copy_op.dump();
    }
    // init_function.dump();
    return success();
  }
};

LogicalResult sdfPass(GraphOp graph_op) {

  auto graph = SDFGraph(graph_op);

  if (graph.generateBroadcastKernels().failed()) {
    return failure();
  }

  if (graph.precomputeParameters().failed()) {
    return failure();
  }

  if (graph.checkAdmissible().failed()) {
    graph_op.emitError("Graph is not admissible.");
    return failure();
  }

  if (graph.scheduleAndBufferizeMemoryPool().failed()) {
    return failure();
  }

  graph_op->remove();
  graph_op.erase();
  return success();
}

} // namespace iara