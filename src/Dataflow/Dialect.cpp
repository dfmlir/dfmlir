#include "Dataflow/Dialect.h"
#include "Dataflow/DataflowDialect.cpp.inc"
#include "mlir/Dialect/Func/IR/FuncOps.h"
#include "mlir/IR/Builders.h"
#include "mlir/IR/BuiltinAttributes.h"
#include "mlir/IR/DialectImplementation.h"
#include "mlir/IR/OpImplementation.h"
#include "util/ranges.h"
#include "util/util.h"
#include "llvm/ADT/TypeSwitch.h"

#define GET_ATTRDEF_CLASSES
#include "Dataflow/DataflowAttrs.cpp.inc"

#define GET_TYPEDEF_CLASSES
#include "Dataflow/DataflowTypes.cpp.inc"

#define GET_OP_CLASSES
#include "Dataflow/DataflowOps.cpp.inc"

namespace iara {
using namespace mlir;

RegionKind GraphOp::getRegionKind(unsigned index) { return RegionKind::Graph; }

LogicalResult NodeOp::verifySymbolUses(SymbolTableCollection &) {
  return success();
}

LogicalResult EdgeOp::verifySymbolUses(SymbolTableCollection &) {
  return success();
}

void DataflowDialect::initialize() {

  addAttributes<
#define GET_ATTRDEF_LIST
#include "Dataflow/DataflowAttrs.cpp.inc"
      >();

  addTypes<
#define GET_TYPEDEF_LIST
#include "Dataflow/DataflowTypes.cpp.inc"
      >();

  addOperations<
#define GET_OP_LIST
#include "Dataflow/DataflowOps.cpp.inc"
      >();
}

Attribute resolveParameter(iara::NodeOp node, iara::ParameterAttr param_decl) {
  using namespace iara;
  using namespace mlir;

  Attribute rv;

  auto param_decl_value = param_decl.getValue();
  auto param_decl_as_type = param_decl_value.dyn_cast_or_null<TypeAttr>();
  IntegerType declared_type = nullptr;
  if (param_decl_as_type)
    declared_type = param_decl_as_type.getValue().dyn_cast<IntegerType>();

  if (param_decl_as_type && !declared_type) {
    node.emitError() << "Parameters must be integers, got " << param_decl;
    return nullptr;
  }

  if (node.implementation().getLeafReference().getValue() == "broadcast") {
    node.emitError() << "Broadcast nodes can't have parameters.";
    return nullptr;
  }

  auto actortype = llvm::dyn_cast_or_null<KernelOp>(
      lookupSymbol(node, node.implementation()));

  auto graph_op = node->getParentOfType<GraphOp>();

  if (!actortype) {
    node.emitError()
        << "Cannot resolve parameter of subgraph node; flatten graph first";
    return nullptr;
  }

  Location loc = actortype.getLoc();
  Attribute node_override;
  if (node.param_overrides()) {
    auto it =
        llvm::find_if(node.param_overridesAttr(), [&](Attribute override_attr) {
          auto override = override_attr.cast<ParameterAttr>();
          return override.getName().getValue() ==
                 param_decl.getName().getValue();
        });
    node_override = (it == node.param_overridesAttr().end()) ? nullptr : *it;
  }
  if (node_override) {
    // Node declaration attempts to overrides the actortype parameter
    // declaration.

    Attribute node_override_value = nullptr;

    if (auto override_param_attr = node_override.dyn_cast<ParameterAttr>()) {
      node_override_value = override_param_attr.getValue();
    }

    auto override_as_int = node_override_value.dyn_cast<IntegerAttr>();

    if (!override_as_int) {
      node.emitError() << "Parameter override is not an integer:"
                       << node_override;
      return nullptr;
    }

    // If the type wasn't specified in the declaration, use the override's.
    if (!param_decl_value)
      return override_as_int;

    if (override_as_int.getType() == param_decl_as_type.getValue())
      return override_as_int;

    // Cast the override with the correct type.
    return IntegerAttr::get(
        declared_type,
        override_as_int.getValue().sextOrTrunc(declared_type.getWidth()));
  }

  // If the actortype declaration doesn't include a default value for the
  // parameter, look for the graph defaults.
  if (!param_decl_value || param_decl_as_type) {
    auto param_name = param_decl.getName().getValue();
    if (graph_op.param_defaults()) {
      ParameterAttr default_param = nullptr;
      for (Attribute attr : graph_op.param_defaultsAttr()) {
        auto param_attr = attr.cast<ParameterAttr>();
        if (param_attr.getName().getValue() == param_name) {
          default_param = param_attr;
          break;
        }
      }

      if (!default_param) {
        // graph_op.dump();
        node->emitError() << "Can't find value for parameter" << param_decl;
        return nullptr;
      }

      Attribute graph_default_value = default_param.getValue();
      IntegerAttr default_as_int = graph_default_value.dyn_cast<IntegerAttr>();

      if (!default_as_int) {
        node.emitError() << "Parameter default is not an integer:"
                         << default_param;
        return nullptr;
      }

      // If the type wasn't specified in the declaration, use the default's.
      if (!param_decl_value)
        return graph_default_value;

      if (graph_default_value.getType() == param_decl_as_type.getValue())
        return graph_default_value;

      // if the type of the default is wrong, cast it.

      return IntegerAttr::get(
          declared_type,
          default_as_int.getValue().sextOrTrunc(declared_type.getWidth()));
    }
  }
  auto int_attr = param_decl_value.dyn_cast<IntegerAttr>();
  if (!int_attr) {
    emitError(loc) << "Parameters must be unsigned integers: " << param_decl;
    return nullptr;
  }
  if (int_attr.getType().isSignedInteger() && int_attr.getSInt() < 0) {
    emitError(loc) << "Parameters must be unsigned integers: " << param_decl;
    return nullptr;
  }
  return int_attr;
}

// Gets the last name in a string of `::` separated names.
llvm::StringRef getNamespaceSuffix(llvm::StringRef name) {
  size_t after_last_double_colon = 0;
  for (size_t i = 1; i < name.size(); i++) {
    if (name[i] == ':' && name[i - 1] == ':') {
      after_last_double_colon = i + 1;
    }
  }
  return name.slice(after_last_double_colon, name.size());
}

llvm::Optional<PortAttr> getPortAttr(NodeOp node, StringRef port_name) {

  if (isSimpleBroadcast(node.implementation())) {
    return getSimpleBroadcastPortAttr(node);
  }

  auto implementation = lookupSymbol(node, node.implementation());

  auto actortype = llvm::dyn_cast_or_null<KernelOp>(implementation);
  if (!actortype) {
    util::error(node->emitError()
                << "Can't get ports from subgraph or broadcast");
    return llvm::None;
  }
  auto port_has_name = [&](Attribute attr) {
    auto port = attr.dyn_cast<PortAttr>();
    return port && port.getName().getValue() == port_name;
  };
  if (actortype.inputs()) {
    auto input_port =
        llvm::find_if(actortype.inputs().getValue(), port_has_name);
    if (input_port != actortype.inputs()->end())
      return input_port->dyn_cast<PortAttr>();
  }
  if (actortype.outputs()) {
    auto output_port =
        llvm::find_if(actortype.outputs().getValue(), port_has_name);
    if (output_port != actortype.outputs()->end())
      return output_port->dyn_cast<PortAttr>();
  }
  return llvm::None;
}

llvm::Optional<EdgeOp> getEdgeForPort(NodeOp node, StringRef port_name) {
  auto graph_op = cast<GraphOp>(node->getParentOp());
  for (auto &op : graph_op) {
    if (auto edge_op = dyn_cast<EdgeOp>(&op)) {
      if (edge_op.cons().getLeafReference() == node.getName() &&
          edge_op.cons_port() == port_name) {
        return edge_op;
      }
      if (edge_op.prod().getLeafReference() == node.getName() &&
          edge_op.prod_port() == port_name) {
        return edge_op;
      }
    }
  }
  return {};
}

std::pair<Operation *, StringRef> getOppositePortAttr(NodeOp node,
                                                      StringRef port_name) {
  auto edge = getEdgeForPort(node, port_name);
  if (node.getName() == edge->cons().getLeafReference())
    return {lookupSymbol(node, edge->prod()), edge->prod_port()};
  if (node.getName() == edge->prod().getLeafReference())
    return {lookupSymbol(node, edge->cons()), edge->cons_port()};
  return {};
}

PortAttr getSimpleBroadcastPortAttr(NodeOp node) {
  if (!isSimpleBroadcast(node.implementation())) {
    llvm_unreachable("Node is not broadcast.");
  }
  auto incoming_edges = getIncomingEdges(node);
  if (incoming_edges.size() != 1) {
    util::error(node->emitError()
                << "Broadcast node does not have exactly one input.");
    exit(1);
  }
  auto [cons_port_name, edge_op] = incoming_edges[0];
  auto prod_node =
      dyn_cast_or_null<NodeOp>(lookupSymbol(edge_op, edge_op.prod()));
  if (prod_node && isSimpleBroadcast(prod_node.implementation())) {
    util::error(mlir::emitError(node.getLoc())
                << "Broadcast nodes can't connect to each other currently.");
    exit(1);
  }

  if (prod_node) {
    return getPortAttr(prod_node, edge_op.prod_port()).getValue();
  }

  util::error(mlir::emitError(node.getLoc())
              << "Source of broadcast must be concrete node currently.");
  return nullptr;
}

int64_t getSimpleBroadcastRate(NodeOp node) {
  return getRateOfPort(getSimpleBroadcastPortAttr(node));
}

int64_t getRateOfPort(PortAttr port) {
  auto rate = port.getRate().dyn_cast<IntegerAttr>();
  return rate.getInt();
}

int64_t getRateOfPort(NodeOp node, StringRef port_name) {
  if (auto port_attr = getPortAttr(node, port_name)) {
    return getRateOfPort(port_attr.getValue());
  }
  if (isSimpleBroadcast(node.implementation())) {
    return getSimpleBroadcastRate(node);
  }
  llvm_unreachable("No port.");
}

ParameterAttr findParamInArray(llvm::Optional<ArrayAttr> array,
                               StringRef name) {
  if (!array)
    return nullptr;
  for (auto attr : *array) {
    if (auto param = attr.dyn_cast<ParameterAttr>()) {
      return param;
    }
  }
  return nullptr;
}

size_t getTokenSize(mlir::Type type) {
  if (auto tensor_type = type.dyn_cast<RankedTensorType>()) {
    return tensor_type.getElementType().getIntOrFloatBitWidth() / 8;
  }
  return type.getIntOrFloatBitWidth() / 8;
}

mlir::Type getTokenElementType(mlir::Type token_type) {
  if (auto tensortype = token_type.dyn_cast<mlir::RankedTensorType>()) {
    return tensortype.getElementType();
  }
  return token_type;
}

std::pair<func::FuncOp, OpBuilder>
createEmptyFunc(KernelOp kernel, StringRef func_name, OpBuilder &opbuilder) {

  if (kernel.parameters()) {
    llvm_unreachable("UNIMPLEMENTED");
  }

  SmallVector<mlir::Location> arg_locs;
  SmallVector<Type> arg_types{};
  for (auto &attr : util::concat(kernel.inputsAttr(), kernel.outputsAttr())) {
    auto port = attr.cast<PortAttr>();
    auto token_type = port.getDatatype();
    auto rate = getRateOfPort(port);
    auto num_elems_in_chunk = getNumElementsInToken(token_type) * rate;
    auto elem_type = getTokenElementType(token_type);

    arg_locs.push_back(kernel.getLoc());
    auto memref_type =
        MemRefType::get({num_elems_in_chunk}, getTokenElementType(token_type));
    arg_types.push_back(memref_type);
  }

  auto func_type = opbuilder.getFunctionType(arg_types, {});

  func::FuncOp rv = opbuilder.create<mlir::func::FuncOp>(
      kernel.getLoc(), StringRef(func_name), func_type,
      opbuilder.getStringAttr("private"));

  auto block = opbuilder.createBlock(&rv.getBody(), rv.getBody().end(),
                                     arg_types, arg_locs);

  auto block_builder = mlir::OpBuilder::atBlockBegin(block);

  block_builder.create<func::ReturnOp>(kernel.getLoc());

  return std::make_pair(rv, OpBuilder::atBlockBegin(block));
}

size_t getNumElementsInToken(mlir::Type token_type) {
  if (auto tensortype = token_type.dyn_cast<mlir::RankedTensorType>()) {
    return tensortype.getNumElements();
  }
  return 1;
}

size_t getTokenElementSize(mlir::Type token_type) {
  return getTokenElementType(token_type).getIntOrFloatBitWidth() / 8;
}

size_t getChunkSize(mlir::Type token_type, int64_t rate) {
  return getTokenSize(token_type) * rate;
}

} // namespace iara