// IR generation for the DIF dataflow language.
#include "DIF/mlirgen.h"
#include "DIF/dif.h"
#include "Dataflow/Dialect.h"
// #include "mlir/IR/Identifier.h"
#include "mlir/IR/BuiltinAttributes.h"
#include "util/util.h"
// #include "util/graphutils.h"

// #include "DIF/graphutils_impl.h"
// #include "mlir/Dialect/StandardOps/IR/Ops.h"
#include "mlir/IR/Attributes.h"
#include "mlir/IR/Builders.h"
#include "mlir/IR/BuiltinOps.h"
#include "mlir/IR/BuiltinTypes.h"
#include "mlir/IR/Diagnostics.h"
#include "mlir/IR/MLIRContext.h"
#include "mlir/IR/OwningOpRef.h"
#include "mlir/IR/SymbolTable.h"
#include "mlir/IR/Verifier.h"

#include "llvm/ADT/ArrayRef.h"
#include "llvm/ADT/MapVector.h"
#include "llvm/ADT/STLExtras.h"
#include "llvm/ADT/ScopedHashTable.h"
#include "llvm/ADT/StringSet.h"
#include "llvm/ADT/StringSwitch.h"
#include "llvm/Support/Path.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/TableGen/Record.h"
#include <cstdlib>
#include <fstream>
#include <functional>
#include <memory>
#include <numeric>
#include <regex>
#include <sstream>
#include <system_error>
#include <type_traits>

using namespace std::placeholders;

namespace dif {

using namespace iara;

int64_t strToInt(llvm::StringRef str) {
  return std::strtol((const char *)str.bytes_begin(), nullptr, 10);
}

class MLIRGen {
private:
  struct GraphData {
    GraphOp op;
    llvm::Optional<mlir::OpBuilder> builder;
    llvm::StringMap<dif::Item const *> node_ast_table;
    llvm::StringMap<std::vector<llvm::StringRef>> incoming_edges_ast_table;
    llvm::StringMap<std::vector<llvm::StringRef>> outgoing_edges_ast_table;
    llvm::StringMap<dif::EdgeAST const *> edge_ast_table;
    llvm::StringMap<PortAttr> interface_inputs;
    llvm::StringMap<PortAttr> interface_outputs;
    llvm::StringMap<dif::ActorBlock const *> actor_ast_table;
    llvm::StringMap<dif::ActorTypeBlock const *> actortype_ast_table;
    llvm::StringMap<ParameterAttr> param_table;
    llvm::StringMap<KernelOp> actortype_op_table;
    llvm::StringMap<NodeOp> node_op_table;
    llvm::StringMap<EdgeOp> edge_op_table;
    llvm::StringMap<mlir::Type> datatype_table;
    llvm::DenseMap<llvm::StringRef, dif::Item> delay_ast_table;
  };

  struct Port {
    llvm::StringRef node_name;
    llvm::StringRef port_name;
  };

public:
  MLIRGen(mlir::MLIRContext &context) : m_builder(&context) {}

  mlir::FailureOr<mlir::ModuleOp> generateMLIR(std::vector<dif::Graph> &graphs,
                                               llvm::StringRef working_dir) {
    m_module = mlir::ModuleOp::create(m_builder.getUnknownLoc());
    m_builder = m_builder.atBlockEnd(m_module.getBody());
    m_working_dir = working_dir;

    for (auto &graph : graphs) {
      mlir::FailureOr<GraphOp> graph_op = registerGraph(graph);
      if (failed(graph_op))
        return util::error(m_module.emitError() << "Failed to register graph");
    }
    for (auto &graph : graph_data_table) {
      GraphData &data = graph.second;
      if (registerNodes(data).failed()) {
        return util::error(m_module.emitError() << "Failed to register nodes");
      }
    }
    return m_module;
  }

  mlir::FailureOr<GraphOp> registerGraph(dif::Graph &graph_ast) {

    GraphData data;

    mlir::ArrayAttr param_defaults = nullptr;

    InterfaceBlock *graph_interface = nullptr;

    // Populate maps

    for (auto &item : graph_ast.m_children) {
      auto block = llvm::cast<BlockAST>(&item);
      switch (block->type()) {
      case TokenKind::Tok_topology: {
        mlir::emitWarning(loc(item))
            << "Topology node ignored in this version.";
      } break;
      case TokenKind::Tok_actortype: {
        data.actortype_ast_table[block->m_name.m_text] =
            llvm::cast<ActorTypeBlock>(block);
      } break;
      case TokenKind::Tok_actor: {
        data.actor_ast_table[block->m_name.m_text] =
            llvm::cast<ActorBlock>(block);
      } break;
      case TokenKind::Tok_parameter: {
        param_defaults =
            registerGraphParameters(data, llvm::cast<ParameterBlock>(block));
      } break;
      case TokenKind::Tok_interface: {
        if (graph_interface) {
          return util::error(mlir::emitError(loc(item))
                             << "Duplicate interface block");
        }
        graph_interface = llvm::cast<InterfaceBlock>(block);
      } break;
      case TokenKind::Tok_datatype: {
        for (auto &child : item.m_children) {
          data.datatype_table[child.m_name.m_text] =
              mlir::RankedTensorType::get(
                  {strToInt(child.m_children[0].m_name.m_text)},
                  m_builder.getI8Type());
        }
      } break;
      case TokenKind::Tok_delay: {
        for (auto &edge : item.m_children) {
          data.delay_ast_table[edge.m_name.m_text] = edge;
        }
      }
      default:
        break;
      }
    }

    data.op = m_builder.create<GraphOp>(
        loc(graph_ast.m_type.m_location), graph_ast.m_name.m_text,
        graph_ast.m_type.m_text, param_defaults);

    auto graph_block =
        m_builder.atBlockEnd(m_module.getBody())
            .createBlock(&data.op.topology(), data.op.topology().end());

    data.builder = m_builder.atBlockBegin(graph_block);

    if (graph_interface)
      registerGraphInterface(data, graph_interface);

    for (auto &actortype : data.actortype_ast_table) {
      registerActortype(data, *(actortype.getValue()));
    }

    auto name = graph_ast.m_name.m_text;
    graph_data_table[name] = data;

    return data.op;
  }

private:
  mlir::ModuleOp m_module;
  mlir::OpBuilder m_builder;

  std::map<llvm::StringRef, GraphData> graph_data_table;

  llvm::StringRef m_working_dir;

  // values depend on key. when key finishes generating, pass through
  // its dependencies and update the actor's implementations
  std::map<GraphOp, llvm::SmallVector<GraphOp>> graph_dependencies;

  mlir::Location loc(dif::CodeLocation const &location) {
    return mlir::FileLineColLoc::get(m_module->getContext(),
                                     *location.m_filename, location.m_line,
                                     location.m_column);
  }

  // Converts string representation of types into an MLIR type.
  mlir::Type stringToMlirType(llvm::StringRef name,
                              llvm::StringMap<mlir::Type> &custom_datatypes) {

    static const llvm::StringMap<mlir::Type> types{
        {"void", m_builder.getNoneType()},
        {"int", m_builder.getIntegerType(32)},
        {"i8", m_builder.getIntegerType(8)},
        {"char", m_builder.getIntegerType(8)},
        {"i32", m_builder.getIntegerType(32)},
        {"i64", m_builder.getIntegerType(64)},
        {"u8", m_builder.getIntegerType(8)},
        {"u32", m_builder.getIntegerType(32)},
        {"f32", m_builder.getF32Type()},
        {"float", m_builder.getF32Type()}};

    mlir::Type rv = types.lookup(name);
    if (rv)
      return rv;

    rv = custom_datatypes.lookup(name);
    if (rv)
      return rv;

    m_module->emitError() << "Unknown C type: " << name;
    llvm_unreachable("Unknown C type");
  }

  mlir::Location loc(dif::Token const &tok) { return loc(tok.m_location); }
  mlir::Location loc(dif::Item const &item) {
    return loc(item.m_name.m_location);
  }

  ParameterAttr parameterFromASTAssignment(dif::Item const &item,
                                           GraphData &graph_data) {

    /*
      only name given: {name, Attribute()}
      name and type given: {name, TypeAttr}
      name and value given: {name, StringAttr}
      all given: {name, *appropriate attribute*}
    */

    bool was_type_given = item.m_type.m_kind != TokenKind::Tok_None;
    bool was_value_given = !item.m_children.empty();
    auto type_str = item.m_type.m_text;

    if (!was_type_given && !was_value_given) {
      auto attr = m_builder.getStringAttr(item.m_name.m_text);
      return ParameterAttr::get(m_builder.getContext(), attr,
                                mlir::Attribute());
    }
    if (!was_value_given) {
      auto attr = m_builder.getStringAttr(item.m_name.m_text);
      return ParameterAttr::get(m_builder.getContext(), attr,
                                mlir::TypeAttr::get(stringToMlirType(
                                    type_str, graph_data.datatype_table)));
    }
    if (!was_type_given) {
      // TODO: handle multiple values
      if (item.m_children[0].m_name.m_kind == TokenKind::Tok_Integer) {
        return ParameterAttr::get(m_builder.getContext(),
                                  m_builder.getStringAttr(item.m_name.m_text),
                                  m_builder.getI64IntegerAttr(strToInt(
                                      item.m_children[0].m_name.m_text)));
      }
      return ParameterAttr::get(
          m_builder.getContext(), m_builder.getStringAttr(item.m_name.m_text),
          m_builder.getStringAttr(item.m_children[0].m_name.m_text));
    }

    // get attribute builder.

    auto mlir_type = stringToMlirType(type_str, graph_data.datatype_table);
    if (mlir_type.isa<mlir::IntegerType>()) {
      auto value = strToInt(item.m_children[0].m_name.m_text);
      auto int_attr = m_builder.getIntegerAttr(mlir_type, value);
      return ParameterAttr::get(m_builder.getContext(),
                                m_builder.getStringAttr(item.m_name.m_text),
                                int_attr);
    } else {
      mlir::emitError(loc(item))
          << "Only simple types can be parameters: " << type_str;
      llvm_unreachable("Compilation failed.");
    }
    return nullptr;
  }

  llvm::SmallVector<PortAttr>
  getImplementationInputs(mlir::Operation *impl_op) {
    llvm::SmallVector<PortAttr> rv;
    auto actortype_op = llvm::dyn_cast<KernelOp>(impl_op);
    if (actortype_op) {
      if (actortype_op.inputs()) {
        for (auto port : actortype_op.inputs()->getValue()) {
          rv.push_back(port.cast<PortAttr>());
        }
      }
      return rv;
    }
    auto graph_op = llvm::dyn_cast<GraphOp>(impl_op);
    if (graph_op) {
      auto graph_name = graph_op.getName();
      for (auto &interface : graph_data_table[graph_name].interface_inputs) {
        rv.push_back(interface.getValue());
      }
      return rv;
    }
    llvm_unreachable("Implementation op is not actortype or graph");
    return rv;
  }

  llvm::SmallVector<PortAttr>
  getImplementationOutputs(mlir::Operation *impl_op) {
    llvm::SmallVector<PortAttr> rv;
    auto actortype_op = llvm::dyn_cast<KernelOp>(impl_op);
    if (actortype_op) {
      if (actortype_op.outputs()) {
        for (auto port : actortype_op.outputs()->getValue()) {
          rv.push_back(port.cast<PortAttr>());
        }
      }
      return rv;
    }
    auto graph_op = llvm::dyn_cast<GraphOp>(impl_op);
    if (graph_op) {
      auto graph_name = graph_op.getName();
      for (auto &interface : graph_data_table[graph_name].interface_outputs) {
        rv.push_back(interface.getValue());
      }
      return rv;
    }
    llvm_unreachable("Implementation op is not actortype or graph");
    return rv;
  }

  PortAttr portFromASTAssignment(dif::Item const &item, GraphData &graph_data) {
    auto rate = [&]() -> mlir::Attribute {
      if (item.m_children.size() == 1 &&
          item.m_children.front().m_name.m_kind == TokenKind::Tok_Integer) {
        return m_builder.getI64IntegerAttr(
            strToInt(item.m_children.front().m_name.m_text));
      }
      return {};
    }();
    auto type = [&]() -> mlir::Type {
      if (item.m_type.m_kind == TokenKind::Tok_Identifier) {
        return stringToMlirType(item.m_type.m_text, graph_data.datatype_table);
      }
      return {};
    }();
    return PortAttr::get(m_builder.getContext(),
                         m_builder.getStringAttr(item.m_name.m_text), type,
                         rate);
  }

  template <class Map> mlir::ArrayAttr makeArrayAttrFromValues(Map map) {
    auto it = llvm::make_second_range(std::move(map));
    auto it2 = llvm::map_range(
        it, [](auto x) { return llvm::cast<mlir::Attribute>(x); });
    auto attrs = llvm::SmallVector<mlir::Attribute>(it2);
    auto arrayref = llvm::makeArrayRef(attrs);
    return arrayref.empty() ? nullptr : m_builder.getArrayAttr(arrayref);
  };

  mlir::ArrayAttr registerGraphParameters(GraphData &graph_data,
                                          ParameterBlock const *block) {
    for (auto &parameter : block->m_children) {
      auto parameter_attr = parameterFromASTAssignment(parameter, graph_data);
      graph_data.param_table[parameter_attr.getName().getValue()] =
          parameter_attr;
    }
    return makeArrayAttrFromValues(graph_data.param_table);
  }

  void registerGraphInterface(GraphData &graph_data,
                              InterfaceBlock const *block) {
    for (auto line : block->m_children) {
      auto port_dict = [&]() {
        if (line.m_type.m_kind == TokenKind::Tok_consumption) {
          return &graph_data.interface_inputs;
        }
        if (line.m_type.m_kind == TokenKind::Tok_production) {
          return &graph_data.interface_outputs;
        }
        llvm_unreachable(
            "Interface block can only have consumption or production");
      }();
      for (auto &assignment : line.m_children) {
        (*port_dict)[assignment.m_name.m_text] =
            portFromASTAssignment(assignment, graph_data);
      }
    }
    llvm::SmallVector<mlir::Attribute> interface_inputs, interface_outputs;

    // llvm::outs() << "Graph interface inputs:\n";
    for (auto &pair : graph_data.interface_inputs) {
      interface_inputs.push_back(pair.second);
      // llvm::outs() << pair.second << "\n";
    }
    // llvm::outs() << "Graph interface outputs:\n";
    for (auto &pair : graph_data.interface_outputs) {
      interface_outputs.push_back(pair.second);
      // llvm::outs() << pair.second << "\n";
    }
    graph_data.op->setAttr(
        "interface_inputs",
        mlir::ArrayAttr::get(graph_data.builder->getContext(),
                             interface_inputs));
    graph_data.op->setAttr(
        "interface_outputs",
        mlir::ArrayAttr::get(graph_data.builder->getContext(),
                             interface_outputs));
  }

  void registerActortype(GraphData &graph_data, ActorTypeBlock const &block) {
    auto funcname = block.m_name.m_text;

    llvm::MapVector<llvm::StringRef, PortAttr> inputs{};
    llvm::MapVector<llvm::StringRef, PortAttr> outputs{};
    llvm::MapVector<llvm::StringRef, ParameterAttr> parameters{};

    // TODO: finish the C scraper?
    // auto interface = cparse::CFunctionInterface::findInterfaceByName(
    //     funcname, m_working_dir);

    // TODO: allow consumption to appear before inputs

    // Create a port attribute
    auto createPort = [&](llvm::MapVector<llvm::StringRef, PortAttr> &i_map,
                          Item const &item) {
      assert(item.m_children.size() == 1);
      // todo: support other types than i32
      i_map[item.m_name.m_text] = portFromASTAssignment(item, graph_data);
    };

    for (auto &i : block.m_children) {
      switch (i.m_type.m_kind) {
      case TokenKind::Tok_consumption: {
        for (auto &port : i.m_children) {
          createPort(inputs, port);
        }
      } break;
      case TokenKind::Tok_production: {
        for (auto &port : i.m_children) {
          createPort(outputs, port);
        }
      } break;
      case TokenKind::Tok_param: {
        for (auto &param : i.m_children) {
          auto name = param.m_name.m_text;
          ParameterAttr param_attr;
          if (param.m_children.size() <= 1) {
            param_attr = parameterFromASTAssignment(param, graph_data);
          } else {
            mlir::emitError(loc(param.m_name.m_location))
                << "Array parameters not supported yet";
          }
          parameters[name] = param_attr;
        }
      } break;
      default:
        break;
      }
    }

    auto parameter_array_attr = makeArrayAttrFromValues(parameters);
    auto input_array_attr = makeArrayAttrFromValues(inputs);
    auto output_array_attr = makeArrayAttrFromValues(outputs);

    auto actortype_op = graph_data.builder->create<KernelOp>(
        loc(block.getLocation()), funcname, parameter_array_attr,
        input_array_attr, output_array_attr);

    graph_data.actortype_op_table[funcname] = actortype_op;
  }

  mlir::LogicalResult registerNodes(GraphData &graph_data) {

    auto is_interface_port = [&](llvm::StringRef name) {
      return graph_data.interface_inputs.count(name) != 0 ||
             graph_data.interface_outputs.count(name) != 0;
    };

    llvm::StringMap<Port> edge_producers, edge_consumers;
    std::set<llvm::StringRef> non_interface_edges;
    std::set<llvm::StringRef> input_interfaces;
    std::set<llvm::StringRef> output_interfaces;

    llvm::SmallVector<ActorBlock const *> broadcast_nodes;

    for (auto &node_kvp : graph_data.actor_ast_table) { // kvp = key-value pair
      auto node_name = node_kvp.getKey();
      auto item = node_kvp.getValue();
      auto param_overrides = llvm::SmallVector<mlir::Attribute>();
      mlir::Operation *implementation_op;

      StringRef impl_name;

      // try to find the implementation first

      for (auto &line : item->m_children) {
        if (line.m_type.m_kind != TokenKind::Tok_type)
          continue;

        impl_name = line.m_name.m_text;

        if (impl_name == "broadcast") {
          break;
        }

        implementation_op = graph_data.actortype_op_table[impl_name];
        if (implementation_op) {
          // Found actortype with this name in current graph.
          break;
        }
        auto external_graph_data_it = graph_data_table.find(impl_name);

        if (external_graph_data_it == graph_data_table.end()) {
          return util::error(mlir::emitError(loc(*item))
                             << "Implementation with name " << impl_name
                             << " not found");
        }

        implementation_op = external_graph_data_it->second.op;
        break;
      }

      if (impl_name == "broadcast") {
        broadcast_nodes.push_back(item);
        continue;
      }

      if (impl_name != "broadcast" && !implementation_op) {
        return util::error(mlir::emitError(loc(*item))
                           << "DIF error: Actor block needs an "
                              "implementation (`type` attribute)");
      }

      assert(llvm::cast<mlir::SymbolOpInterface>(implementation_op).getName() ==
             impl_name);

      for (auto &line : item->m_children) {
        switch (line.m_type.m_kind) {
        case TokenKind::Tok_type: {
          continue;
        } break;
        case TokenKind::Tok_param: {
          for (auto &assignment : line.m_children) {
            param_overrides.push_back(
                parameterFromASTAssignment(assignment, graph_data));
          }
        } break;
        case TokenKind::Tok_interface: {

          for (auto &connection : line.m_children) {
            auto lhs = connection.m_name.m_text;
            auto rhs = connection.m_type.m_text;
            auto inputs = getImplementationInputs(implementation_op);
            auto outputs = getImplementationOutputs(implementation_op);

            bool lhs_in_node_outputs = false;
            bool rhs_in_node_inputs = false;

            for (auto attr : inputs) {
              if (attr.getName().getValue() == rhs) {
                rhs_in_node_inputs = true;
                break;
              }
            }

            for (auto attr : outputs) {
              if (attr.getName().getValue() == lhs) {
                lhs_in_node_outputs = true;
                break;
              }
            }

            llvm::StringRef port_name;
            llvm::StringRef edge_name;
            decltype(edge_producers) *edge_port_table;

            if (lhs_in_node_outputs) {
              port_name = lhs;
              edge_name = rhs;
              edge_port_table = &edge_producers;
            } else if (rhs_in_node_inputs) {
              port_name = rhs;
              edge_name = lhs;
              edge_port_table = &edge_consumers;
            } else {
              return util::error(mlir::emitError(loc(connection))
                                 << "Unknown input port " << lhs
                                 << " or output port " << lhs);
            }

            if (is_interface_port(edge_name)) {
              if (lhs_in_node_outputs) {
                output_interfaces.insert(edge_name);
              } else if (rhs_in_node_inputs) {
                input_interfaces.insert(edge_name);
              }
            } else {
              non_interface_edges.insert(edge_name);
            }

            bool has_inserted =
                edge_port_table->insert({edge_name, {node_name, port_name}})
                    .second;
            if (!has_inserted) {
              return util::error(
                  mlir::emitError(loc(connection))
                  << "Duplicate "
                  << (edge_port_table == &edge_producers ? "producer"
                                                         : "consumer")
                  << " connection on edge " << edge_name
                  << ": old = " << (((*edge_port_table)[edge_name]).node_name)
                  << "::" << (((*edge_port_table)[edge_name]).port_name)
                  << ", new = " << node_name << "::" << port_name);
            }
          }
        } break;
        default: {
        }
        }
      }

      graph_data.node_op_table[node_name] = graph_data.builder->create<NodeOp>(
          loc(item->m_name), node_name,
          mlir::SymbolRefAttr::get(graph_data.builder->getContext(), impl_name),
          param_overrides.empty()
              ? nullptr
              : graph_data.builder->getArrayAttr(param_overrides));
    }

    for (auto node : broadcast_nodes) {

      StringRef broadcast_node_name = node->m_name.m_text;
      // Edges should already exist.
      for (auto &line : node->m_children) {
        if (line.m_type.m_kind != TokenKind::Tok_interface) {
          continue;
        }

        mlir::Type datatype;

        for (auto &connection : line.m_children) {
          auto lhs = connection.m_name.m_text;
          auto rhs = connection.m_type.m_text;

          bool lhs_is_incoming_edge = edge_producers.count(lhs);
          bool rhs_is_outgoing_edge = edge_consumers.count(rhs);

          int incoming_port_count = 0;

          if (lhs_is_incoming_edge && rhs_is_outgoing_edge) {
            return util::error(mlir::emitError(loc(line))
                               << "Both sides of this connection are edges.");
          }

          if (!lhs_is_incoming_edge && !rhs_is_outgoing_edge) {
            return util::error(
                mlir::emitError(loc(line))
                << "Neither side of this connection is an edge.");
          }

          // TODO: add interface handling. For now assuming the broadcast will
          // not be in a subgraph boundary

          // Also assuming no broadcast nodes appear one after another.

          if (lhs_is_incoming_edge) {
            if (incoming_port_count > 0) {
              return util::error(mlir::emitError(loc(line))
                                 << "Only one incoming port is allowed in a "
                                    "broadcast edge.");
            }
            edge_consumers[lhs] = {broadcast_node_name, rhs};
            auto port_data = edge_producers.lookup(lhs);
            datatype =
                getPortAttr(graph_data.node_op_table[port_data.node_name],
                            port_data.port_name)
                    .getValue()
                    .getType();
          }

          if (rhs_is_outgoing_edge) {
            edge_producers[rhs] = {broadcast_node_name, lhs};
          }
        }
      }
      graph_data.node_op_table[broadcast_node_name] =
          graph_data.builder->create<NodeOp>(
              loc(node->m_name), broadcast_node_name,
              mlir::SymbolRefAttr::get(graph_data.builder->getContext(),
                                       "broadcast"),
              nullptr);
    }

    // Check that all edges are consistent:
    // all graph interface inputs only appear in edges_leaving_nodes,
    // all graph interface outputs only appear in edges_entering_nodes,
    // and all remaining edges appear in both

    for (auto &input_it : graph_data.interface_inputs) {
      auto input_name = input_it.getKey();
      if (edge_consumers.count(input_name) == 0) {
        graph_data.op->emitError() << "Graph interface input " << input_name
                                   << " never used as input for a node";
        return mlir::failure();
      }
      if (edge_producers.count(input_name) != 0) {
        graph_data.op->emitError()
            << "Graph interface input" << input_name << "used as an output";
        return mlir::failure();
      }
    }

    for (auto &output_it : graph_data.interface_outputs) {
      auto output_name = output_it.getKey();
      if (edge_producers.count(output_name) == 0) {
        graph_data.op->emitError() << "Graph interface output " << output_name
                                   << " never used as output for a node";
        return mlir::failure();
      }
      if (edge_consumers.count(output_name) != 0) {
        graph_data.op->emitError()
            << "Graph interface output" << output_name << "used as an input";
        return mlir::failure();
      }
    }

    for (auto &edge_name : non_interface_edges) {
      if (edge_consumers.count(edge_name) != 1) {
        graph_data.op->emitError()
            << "Edge " << edge_name << " has no consumer";
        return mlir::failure();
      }
      if (edge_producers.count(edge_name) != 1) {
        graph_data.op->emitError()
            << "Edge " << edge_name << " has no producer";
        return mlir::failure();
      }
    }

    for (auto edge_name : non_interface_edges) {
      auto producer = edge_producers[edge_name];
      auto consumer = edge_consumers[edge_name];

      auto producer_op = graph_data.op.lookupSymbol(producer.node_name);
      auto consumer_op = graph_data.op.lookupSymbol(consumer.node_name);

      llvm::SmallVector<mlir::Location> locs = {producer_op->getLoc(),
                                                consumer_op->getLoc()};

      auto location = graph_data.builder->getFusedLoc(locs);

      graph_data.edge_op_table[edge_name] = graph_data.builder->create<EdgeOp>(
          location, edge_name,
          mlir::SymbolRefAttr::get(graph_data.builder->getContext(),
                                   producer.node_name),
          producer.port_name,
          mlir::SymbolRefAttr::get(graph_data.builder->getContext(),
                                   consumer.node_name),
          consumer.port_name);
    }

    for (auto interface_name : input_interfaces) {
      auto consumer = edge_consumers[interface_name];

      auto consumer_op = graph_data.op.lookupSymbol(consumer.node_name);

      if (!consumer_op) {
        return util::error(graph_data.op.emitError()
                           << "Did not find consumer node for <"
                           << interface_name << "> input interface");
      }

      assert(graph_data.edge_op_table.count(interface_name) == 0);
      graph_data.edge_op_table[interface_name] =
          graph_data.builder->create<EdgeOp>(
              consumer_op->getLoc(), interface_name,
              mlir::SymbolRefAttr::get(graph_data.builder->getContext(),
                                       graph_data.op.getName()),
              interface_name,
              mlir::SymbolRefAttr::get(graph_data.builder->getContext(),
                                       consumer.node_name),
              consumer.port_name);
    }

    for (auto interface_name : output_interfaces) {
      auto producer = edge_producers[interface_name];

      auto producer_op = graph_data.op.lookupSymbol(producer.node_name);

      if (!producer_op) {
        return util::error(graph_data.op.emitError()
                           << "Did not find producer node for "
                           << interface_name << " output interface");
      }
      assert(graph_data.edge_op_table.count(interface_name) == 0);
      graph_data.edge_op_table[interface_name] =
          graph_data.builder->create<EdgeOp>(
              producer_op->getLoc(), interface_name,
              mlir::SymbolRefAttr::get(graph_data.builder->getContext(),
                                       producer.node_name),
              producer.port_name,
              mlir::SymbolRefAttr::get(graph_data.builder->getContext(),
                                       graph_data.op.getName()),
              interface_name);
    }

    for (auto &[edge_name, delay_line] : graph_data.delay_ast_table) {
      auto edge_op = graph_data.edge_op_table[edge_name];
      if (delay_line.m_type.m_kind != TokenKind::Tok_None) {
        auto duration = util::stringToIntegerAttr(delay_line.m_type.m_text,
                                                  *graph_data.builder);
        edge_op->setAttr("delay_duration", duration);
      }
      if (not delay_line.m_children.empty()) {
        llvm::SmallVector<llvm::StringRef> strings;
        for (auto item : delay_line.m_children) {
          strings.push_back(item.m_name.m_text);
        }
        auto vals = graph_data.builder->getStrArrayAttr(strings);
        edge_op->setAttr("delay_values", vals);
      }
    }

    return mlir::success();
  }
};

mlir::FailureOr<mlir::ModuleOp> mlirGen(mlir::MLIRContext &context,
                                        std::vector<dif::Graph> &ast,
                                        llvm::StringRef working_dir) {

  return MLIRGen(context).generateMLIR(ast, working_dir);
}

} // namespace dif
