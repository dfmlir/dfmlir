/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.


@ddblock_end copyright
*******************************************************************************/

grammar DIF;

graph_list
    : graph+ EOF
    ;

graph
    : Identifier Identifier '{' block* '}'
    ;

block
    : BASEDON basedon_body
    | TOPOLOGY topology_body
    | INTERFACE interface_body
    | PARAMETER parameter_body
    | REFINEMENT refinement_body
    | ATTRIBUTE  attribute_body
    | MODEL_ATTR  attribute_body
    | ACTOR_TYPE  actortype_body
    | ACTOR  actor_body
    | DELAY delay_body
    ;

delay_body
    : '{' ( delay_item ';')* '}'
    ;

delay_item
    : Identifier '=' IntegerLiteral
    ;

basedon_body
    : '{' Identifier ';' '}'
    ;

topology_body
    : '{' topology_list '}'
    ;

topology_list
    : (topology_item ';')+
    ;

topology_item
    :  NODES '=' Identifier ( ',' Identifier)*
    | EDGES '=' Identifier '(' Identifier ',' Identifier ')' (',' Identifier '(' Identifier ',' Identifier ')' )*
    ;

interface_body
    : '{' interface_list '}'
    ;

interface_list
    : (interface_item ';')*
    ;

interface_item
    : INPUTS '=' Identifier ':' Identifier (',' Identifier ':' Identifier)*
    | OUTPUTS '=' Identifier ':' Identifier (',' Identifier ':' Identifier)*
    ;

parameter_body
    : '{' parameter_list '}'
    ;

parameter_list
    : (parameter_item ';')*
    ;

parameter_item
    : Identifier '=' elementValueArray
    | Identifier '=' literal
    ;

refinement_body
    : '{' Identifier '=' Identifier ';' refinement_list '}'
    ;

refinement_list
    : (refinement_item ';')*
    ;

refinement_item
    : Identifier ':' Identifier
    ;

attribute_body
    : Identifier '{' attribute_list '}'
    ;

attribute_list
    : (attribute_item ';')*
    ;

attribute_item
    : Identifier '=' literal
    | Identifier '=' elementValueArray
    ;

actor_body
    : Identifier '{' actor_list '}'
    ;

actor_list
    : (actor_item ';')*
    ;

actor_item
    : TYPE ':' Identifier
    | INTERFACE Identifier PTR_OP Identifier (',' Identifier PTR_OP Identifier)*
    | PARAM actor_param_list
    | CODE_GEN actor_codegen_param_list
    ;

actor_codegen_param_list
    : '@' Identifier '(' Identifier ')' (',' '@' Identifier '(' Identifier ')' )*
    ;

actor_param_list
    : Identifier '=' literal
    | Identifier '=' elementValueArray
    | actor_param_list ',' Identifier '=' literal
    | actor_param_list ',' Identifier '=' elementValueArray
    ;

actortype_body
    : Identifier '{' actortype_list '}'
    ;

actortype_list
    : (actortype_defn ';')*
    ;

actortype_defn
    : INPUT Identifier (',' Identifier)*
    | OUTPUT Identifier (',' Identifier)*
    | PARAM Identifier (',' Identifier)*
    | MODE Identifier (',' Identifier)*
    | PRODUCTION Identifier ':' IntegerLiteral (',' Identifier ':' IntegerLiteral )*
    | CONSUMPTION Identifier ':' IntegerLiteral (',' Identifier ':' IntegerLiteral )*
    ;

// Array and literal

elementValueArray
    :	'{' elementValueList? ','? '}'
    ;

elementValueList
    :	literal (',' literal)*
    ;

literal
    :	IntegerLiteral
    |	FloatingPointLiteral
    |	BooleanLiteral
    |	CharacterLiteral
    |	StringLiteral
    |	NullLiteral
    ;

// LEXER

// Keywords
CODE_GEN : 'codegen';
MODEL_ATTR : 'msa';
ATTRIBUTE : 'attribute';
BASEDON : 'basedon';
TOPOLOGY : 'topology';
INTERFACE : 'interface';
PARAM : 'param';
PARAMETER : 'parameter';
REFINEMENT : 'refinement';
ACTOR : 'actor';
ACTOR_TYPE : 'actortype';
TYPE : 'type';
INPUT : 'input';
OUTPUT : 'output';
MODE : 'mode';
INPUTS : 'inputs';
OUTPUTS : 'outputs';
NODES : 'nodes';
EDGES : 'edges';
CHAIN : 'chain';
RING : 'ring';
BROADCAST : 'broadcast';
MERGE : 'merge';
PARALLEL : 'parallel';
BUTTERFLY : 'butterfly';
MULTIEDGE : 'multiedge';
PRODUCTION : 'production';
CONSUMPTION:   'consumption';
DELAY : 'delay';

// Integer Literals
IntegerLiteral
	:	DecimalIntegerLiteral
	|	HexIntegerLiteral
	|	OctalIntegerLiteral
	|	BinaryIntegerLiteral
	;

fragment
DecimalIntegerLiteral
	:	DecimalNumeral IntegerTypeSuffix?
	;

fragment
HexIntegerLiteral
	:	HexNumeral IntegerTypeSuffix?
	;

fragment
OctalIntegerLiteral
	:	OctalNumeral IntegerTypeSuffix?
	;

fragment
BinaryIntegerLiteral
	:	BinaryNumeral IntegerTypeSuffix?
	;

fragment
IntegerTypeSuffix
	:	[lL]
	;

fragment
DecimalNumeral
	:	'0'
	|	NonZeroDigit (Digits? | Underscores Digits)
	;

fragment
Digits
	:	Digit (DigitsAndUnderscores? Digit)?
	;

fragment
Digit
	:	'0'
	|	NonZeroDigit
	;

fragment
NonZeroDigit
	:	[1-9]
	;

fragment
DigitsAndUnderscores
	:	DigitOrUnderscore+
	;

fragment
DigitOrUnderscore
	:	Digit
	|	'_'
	;

fragment
Underscores
	:	'_'+
	;

fragment
HexNumeral
	:	'0' [xX] HexDigits
	;

fragment
HexDigits
	:	HexDigit (HexDigitsAndUnderscores? HexDigit)?
	;

fragment
HexDigit
	:	[0-9a-fA-F]
	;

fragment
HexDigitsAndUnderscores
	:	HexDigitOrUnderscore+
	;

fragment
HexDigitOrUnderscore
	:	HexDigit
	|	'_'
	;

fragment
OctalNumeral
	:	'0' Underscores? OctalDigits
	;

fragment
OctalDigits
	:	OctalDigit (OctalDigitsAndUnderscores? OctalDigit)?
	;

fragment
OctalDigit
	:	[0-7]
	;

fragment
OctalDigitsAndUnderscores
	:	OctalDigitOrUnderscore+
	;

fragment
OctalDigitOrUnderscore
	:	OctalDigit
	|	'_'
	;

fragment
BinaryNumeral
	:	'0' [bB] BinaryDigits
	;

fragment
BinaryDigits
	:	BinaryDigit (BinaryDigitsAndUnderscores? BinaryDigit)?
	;

fragment
BinaryDigit
	:	[01]
	;

fragment
BinaryDigitsAndUnderscores
	:	BinaryDigitOrUnderscore+
	;

fragment
BinaryDigitOrUnderscore
	:	BinaryDigit
	|	'_'
	;

// Floating-Point Literals

FloatingPointLiteral
	:	DecimalFloatingPointLiteral
	|	HexadecimalFloatingPointLiteral
	;

fragment
DecimalFloatingPointLiteral
	:	Digits '.' Digits? ExponentPart? FloatTypeSuffix?
	|	'.' Digits ExponentPart? FloatTypeSuffix?
	|	Digits ExponentPart FloatTypeSuffix?
	|	Digits FloatTypeSuffix
	;

fragment
ExponentPart
	:	ExponentIndicator SignedInteger
	;

fragment
ExponentIndicator
	:	[eE]
	;

fragment
SignedInteger
	:	Sign? Digits
	;

fragment
Sign
	:	[+-]
	;

fragment
FloatTypeSuffix
	:	[fFdD]
	;

fragment
HexadecimalFloatingPointLiteral
	:	HexSignificand BinaryExponent FloatTypeSuffix?
	;

fragment
HexSignificand
	:	HexNumeral '.'?
	|	'0' [xX] HexDigits? '.' HexDigits
	;

fragment
BinaryExponent
	:	BinaryExponentIndicator SignedInteger
	;

fragment
BinaryExponentIndicator
	:	[pP]
	;

// Boolean Literals

BooleanLiteral
	:	'true'
	|	'false'
	;

// Character Literals

CharacterLiteral
	:	'\'' SingleCharacter '\''
	|	'\'' EscapeSequence '\''
	;

fragment
SingleCharacter
	:	~['\\\r\n]
	;
// String Literals
StringLiteral
	:	'"' StringCharacters? '"'
	;
fragment
StringCharacters
	:	StringCharacter+
	;
fragment
StringCharacter
	:	~["\\\r\n]
	|	EscapeSequence
	;

//  Escape Sequences for Character and String Literals

fragment
EscapeSequence
	:	'\\' [btnfr"'\\]
	|	OctalEscape
    |   UnicodeEscape // This is not in the spec but prevents having to preprocess the input
	;

fragment
OctalEscape
	:	'\\' OctalDigit
	|	'\\' OctalDigit OctalDigit
	|	'\\' ZeroToThree OctalDigit OctalDigit
	;

fragment
ZeroToThree
	:	[0-3]
	;

// This is not in the spec but prevents having to preprocess the input
fragment
UnicodeEscape
    :   '\\' 'u'+  HexDigit HexDigit HexDigit HexDigit
    ;

// The Null Literal

NullLiteral
	:	'null'
	;

// Separators

LPAREN : '(';
RPAREN : ')';
LBRACE : '{';
RBRACE : '}';
LBRACK : '[';
RBRACK : ']';
SEMI : ';';
COMMA : ',';
DOT : '.';

// Operators
PTR_OP : '->';
ASSIGN : '=';
GT : '>';
LT : '<';
BANG : '!';
TILDE : '~';
QUESTION : '?';
COLON : ':';
EQUAL : '==';
LE : '<=';
GE : '>=';
NOTEQUAL : '!=';
AND : '&&';
OR : '||';
INC : '++';
DEC : '--';
ADD : '+';
SUB : '-';
MUL : '*';
DIV : '/';
BITAND : '&';
BITOR : '|';
CARET : '^';
MOD : '%';
ARROW : '->';
COLONCOLON : '::';

ADD_ASSIGN : '+=';
SUB_ASSIGN : '-=';
MUL_ASSIGN : '*=';
DIV_ASSIGN : '/=';
AND_ASSIGN : '&=';
OR_ASSIGN : '|=';
XOR_ASSIGN : '^=';
MOD_ASSIGN : '%=';
LSHIFT_ASSIGN : '<<=';
RSHIFT_ASSIGN : '>>=';
URSHIFT_ASSIGN : '>>>=';

// Identifiers (must appear after all keywords in the grammar)

Identifier
	:	JavaLetter JavaLetterOrDigit*
	;

fragment
JavaLetter
	:	[a-zA-Z$_] // these are the "java letters" below 0x7F
	|	// covers all characters above 0x7F which are not a surrogate
		~[\u0000-\u007F\uD800-\uDBFF]
		{Character.isJavaIdentifierStart(_input.LA(-1))}?
	|	// covers UTF-16 surrogate pairs encodings for U+10000 to U+10FFFF
		[\uD800-\uDBFF] [\uDC00-\uDFFF]
		{Character.isJavaIdentifierStart(Character.toCodePoint((char)_input.LA(-2), (char)_input.LA(-1)))}?
	;

fragment
JavaLetterOrDigit
	:	[a-zA-Z0-9$_] // these are the "java letters or digits" below 0x7F
	|	// covers all characters above 0x7F which are not a surrogate
		~[\u0000-\u007F\uD800-\uDBFF]
		{Character.isJavaIdentifierPart(_input.LA(-1))}?
	|	// covers UTF-16 surrogate pairs encodings for U+10000 to U+10FFFF
		[\uD800-\uDBFF] [\uDC00-\uDFFF]
		{Character.isJavaIdentifierPart(Character.toCodePoint((char)_input.LA(-2), (char)_input.LA(-1)))}?
	;

// Additional symbols not defined in the lexical specification

AT : '@';
ELLIPSIS : '...';

// Whitespace and comments

WS  :  [ \t\r\n\u000C]+ -> skip
    ;

COMMENT
    :   '/*' .*? '*/' -> channel(HIDDEN)
    ;

LINE_COMMENT
    :   '//' ~[\r\n]* -> channel(HIDDEN)
    ;
