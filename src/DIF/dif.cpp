#include "DIF/dif.h"

using namespace graphutils;
namespace dif {

TokenTables g_tables{};

TokenKind charToToken(char c) {
  return g_tables.m_charToToken[static_cast<std::size_t>(c)];
}
TokenKind stringToToken(llvm::StringRef s) {
  for (std::size_t i = 0; i < (std::size_t)(TokenKind::Tok_Eof) + 1; i++) {
    if (s == g_tables.m_tokenStrings[i]) {
      return TokenKind(i);
    }
  }
  return TokenKind::Tok_None;
}

char tokenToChar(TokenKind kind) {
  return g_tables.m_tokenToChar[static_cast<std::size_t>(kind)];
}
llvm::StringRef getTokenName(TokenKind kind) {
  return g_tables.m_tokenNames[static_cast<std::size_t>(kind)];
}

// Token

std::string Token::str() const {
  std::string rv;
  llvm::raw_string_ostream ss(rv);
  if (m_kind == Kind::Tok_None)
    ss << "none";
  else if (isKeyword())
    ss << getTokenName(m_kind);
  else if (isPunctuation())
    ss << tokenToChar(m_kind);
  else
    ss << getTokenName(m_kind) << "(" << m_text << ")";
  return rv;
}

bool Token::isKeyword() const {
  return m_kind > Kind::Tok_KeywordsBegin && m_kind < Kind::Tok_KeywordsEnd;
}

bool Token::isPunctuation() const {
  return m_kind > Kind::Tok_PunctuationBegin &&
         m_kind < Kind::Tok_PunctuationEnd;
}

bool Token::isBlockname() const {
  return m_kind > Kind::Tok_BlocknamesBegin && m_kind < Kind::Tok_BlocknamesEnd;
}

bool Token::isLiteral() const {
  return m_kind > Kind::Tok_LiteralsBegin && m_kind < Kind::Tok_LiteralsEnd;
}

// Lexer

void Lexer::skipWhitespaceAndComments() {
  // Skip whitespace and comments

  do {
    while (isspace(m_lastChar)) {
      m_lastChar = getNextChar();
    }

    // Skip comments
    if (m_lastChar == '/') {
      m_lastChar = getNextChar();
      char closingChar = 0;
      switch (m_lastChar) {
      case '/': // single-line comment
        closingChar = '\n';
        break;
      case '*': // multi-line comment
        closingChar = '*';
        break;
      default: // the '/' character by itself.
        llvm_unreachable("The / operator is not allowed in DIF.");
      }

      while (true) {
        m_lastChar = getNextChar();
        if (m_lastChar == EOF)
          break;
        if (m_lastChar != closingChar && m_lastChar != '\r')
          continue;
        if (m_lastChar == '\n' || m_lastChar == '\r') {
          // end single-line comment
          m_lastChar = getNextChar();
          break;
        }
        if (m_lastChar == '*') {
          m_lastChar = getNextChar();
          if (m_lastChar == EOF)
            break;
          if (m_lastChar == '/') {
            m_lastChar = getNextChar();
            break;
          }
        }
      }
      continue; // re-skip whitespace
    } else
      break;
  } while (true);
}

TokenKind Lexer::readKeywordOrIdentifier() {
  // If it starts with a letter, it's a keyword or identifier.
  if (isalpha(m_lastChar)) {
    auto string_start = m_bufferSlice.begin() - 1;
    auto size = 1;
    while (isalnum((m_lastChar = getNextChar())) || m_lastChar == '_') {
      size += 1;
    }
    m_tokenText = llvm::StringRef(string_start, size);
    // Check for keywords.
    int keyword_index = 0;
    for (auto i = Kind::Tok_BlocknamesBegin; i < Kind::Tok_KeywordsEnd;
         i = Kind(std::size_t(i) + 1)) {
      if (getTokenName(i) == m_tokenText) {
        return i;
      }
      keyword_index += 1;
    }
    return Kind::Tok_Identifier;
  }
  return Kind::Tok_None;
}

TokenKind Lexer::readNumber() {
  // TODO: implement floats (like '-45.27e-9'), octals, hex

  if (isdigit(m_lastChar)) {
    auto string_start = m_bufferSlice.begin() - 1;
    auto size = 0;
    do {
      size += 1;
      m_lastChar = getNextChar();
    } while (isdigit(m_lastChar));
    m_tokenText = llvm::StringRef(string_start, size);
    return Kind::Tok_Integer;
  } else
    return Kind::Tok_None;
}

TokenKind Lexer::readString() {
  // Todo: add support for $ strings
  if (m_lastChar == '"') {
    auto string_start = m_bufferSlice.begin() - 1;
    auto size = 1;
    while (true) {
      m_lastChar = getNextChar();
      if (m_lastChar == EOF)
        fail(m_location, "Reached end of file before closing string.");
      size += 1;
      if (m_lastChar != '\\' && m_lastChar != '\"')
        continue;
      if (m_lastChar == '\\') {
        // TODO: add long escape sequences.
        getNextChar();
        size += 1;
        continue;
      }
      // Found closing quote
      m_lastChar = getNextChar();
      break;
    };
    m_tokenText = llvm::StringRef(string_start, size);
    return TokenKind::Tok_String;
  } else
    return TokenKind::Tok_None;
}

struct PunctuationTable {
  bool is_punct[256] = {false};

  constexpr PunctuationTable() {
    for (int i = 0; i < 256; i++)
      is_punct[i] = false;
    for (auto i : "(){}[];,.-+=<>!~?:&|*/^%") {
      is_punct[(int)i] = true;
    }
  }
};

constexpr PunctuationTable PUNCT_TABLE = PunctuationTable();

TokenKind Lexer::readSymbol() {
  if (!PUNCT_TABLE.is_punct[(std::size_t)m_lastChar]) {
    return TokenKind::Tok_None;
  }
  auto string_start = m_bufferSlice.begin() - 1;

  // Check for long operators. They are sorted in decreasing order by length,
  // so we check for the longest ones first.

  for (auto entry : g_tables.m_strings) {
    auto kind = entry.first;
    auto string = entry.second;

    auto current_slice =
        llvm::StringRef(string_start, m_bufferSlice.size() + 1);
    if (current_slice >= string) {
      auto is_different =
          strncmp(string.begin(), current_slice.begin(), string.size());
      if (is_different)
        continue;

      // Accept this token

      m_tokenText = llvm::StringRef(string_start, string.size());
      for (size_t i = 0; i < string.size(); i++) {
        m_lastChar = getNextChar();
      }
      return kind;
    }
  }

  // Check for single-character operators.

  if (PUNCT_TABLE.is_punct[(std::size_t)m_lastChar]) {
    m_tokenText = llvm::StringRef(string_start, 1);
    m_lastChar = getNextChar();
    return charToToken(*string_start);
  }

  fail(m_location, llvm::formatv("Tried to read a symbol, got {}", m_lastChar));
  return TokenKind::Tok_None;
}

int Lexer::getNextChar() {
  if (m_bufferSlice.empty()) {
    return EOF;
  }
  auto size = m_bufferSlice.size();
  auto nextChar = m_bufferSlice.front();
  m_bufferSlice = m_bufferSlice.drop_front();
  assert(size == m_bufferSlice.size() + 1);
  if (m_lastChar == '\n') {
    m_location.m_line += 1;
    m_location.m_column = 1;
  }
  return nextChar;
}

Token Lexer::getToken() {

  skipWhitespaceAndComments();

  auto tokenLocation = m_location;

  // Word_like tokens

  {
    auto keyword = readKeywordOrIdentifier();
    if (keyword != Kind::Tok_None)
      return Token(keyword, tokenLocation, m_tokenText);
  }

  // Check for numbers.
  {
    auto number = readNumber();
    if (number != Kind::Tok_None)
      return Token(number, tokenLocation, m_tokenText);
  }

  // Check for strings.
  {
    auto string = readString();
    if (string != Kind::Tok_None)
      return Token(string, tokenLocation, m_tokenText);
  }

  if (m_lastChar == EOF)
    return Token(Kind::Tok_Eof, tokenLocation, m_tokenText);

  // If it is a symbol, read until the next non-symbol.

  auto symbol = readSymbol();
  if (symbol != Kind::Tok_None) {
    return Token(symbol, tokenLocation, m_tokenText);
  }

  fail(tokenLocation, "Failed to read token");
}

int Lexer::readList(std::function<void()> doThis, TokenKind separator,
                    TokenKind terminator,
                    ListLastSeparatorBehavior lastSeparator) {

  if (lastSeparator == ListLastSeparatorBehavior::REQUIRED &&
      separator == TokenKind::Tok_None) {
    fail("Cannot use REQUIRED mode with no separator.");
  }

  if (m_currentToken.m_kind == terminator) {
    // No items!
    consume();
    return 0;
  }

  enum State { AFTER_PREDICATE, AFTER_SEPARATOR } state = AFTER_SEPARATOR;

  int times_read = 0;

  while (true) {
    switch (state) {
    case AFTER_SEPARATOR: {
      if (m_currentToken.m_kind == terminator) {
        if (separator != TokenKind::Tok_None &&
            lastSeparator == ListLastSeparatorBehavior::FORBIDDEN) {
          fail(m_currentToken.m_location,
               llvm::formatv("Expected item before {0}", m_currentToken.str()));
        }
        consume();
        return times_read;
      }
      if (m_currentToken.m_kind == separator) {
        fail(m_currentToken.m_location,
             llvm::formatv("Expected item before {0}", m_currentToken.str()));
      }
      doThis();
      times_read += 1;
      state = AFTER_PREDICATE;
      continue;
    } break;
    case AFTER_PREDICATE: {
      if (m_currentToken.m_kind == terminator) {
        if (lastSeparator == ListLastSeparatorBehavior::REQUIRED) {
          fail(m_currentToken.m_location,
               llvm::formatv("Expected {0} before {1}", getTokenName(separator),
                             m_currentToken.str()));
        };
        consume();
        return times_read;
      };
      if (separator == TokenKind::Tok_None) {
        state = AFTER_SEPARATOR;
        continue;
      }
      if (m_currentToken.m_kind == separator) {
        consume();
        state = AFTER_SEPARATOR;
        continue;
      }
      fail(m_currentToken.m_location,
           llvm::formatv("Expected {0} or {1}", getTokenName(separator),
                         getTokenName(terminator)));
      return times_read;
    } break;
    default:
      llvm_unreachable("");
      break;
    }
  }
}

int Lexer::readList(std::function<void()> doThis, char separator,
                    char terminator, ListLastSeparatorBehavior lastSeparator) {
  return readList(doThis, charToToken(separator), charToToken(terminator),
                  lastSeparator);
}

Token Lexer::consumeLiteral() {
  if (!m_currentToken.isLiteral()) {
    fail(m_currentToken.m_location,
         llvm::formatv("Expected a literal; found {0}", m_currentToken.str()));
  }
  return consume();
}

Item Lexer::readExpression() {
  // TODO: more operations

  auto consumeValue = [&]() {
    if (m_currentToken.m_kind == TokenKind::Tok_Integer ||
        m_currentToken.m_kind == TokenKind::Tok_Identifier) {
      return consume();
    }
    fail(m_currentToken.m_location,
         llvm::formatv("Expected a literal; found {0}", m_currentToken.str()));
    llvm_unreachable("");
    return Token();
  };

  auto isBinOp = [](auto x) { return x == TokenKind::Tok_Multiply; };

  Item rv{consumeValue()};

  auto root = &rv;

  while (isBinOp(m_currentToken.m_kind)) {
    auto binop = Item{consume()};
    binop.m_children.push_back(*root);
    binop.m_children.push_back(consumeValue());
    root = &binop;
  }
  return *root;
}

Item Lexer::readAssignment() {
  dif::Item name = consume(TokenKind::Tok_Identifier);
  if (m_currentToken.m_kind == TokenKind::Tok_Colon) {
    consume(TokenKind::Tok_Colon);
    name.m_type = consume(TokenKind::Tok_Identifier);
  }
  if (m_currentToken.m_kind == TokenKind::Tok_Assign) {
    consume(TokenKind::Tok_Assign);
    name.m_children.emplace_back(consume());
  }
  return name;
}

// Item

// Assume single Integer child and convert it to APInt
llvm::APInt Item::toAPInt() const {
  assert(m_children.size() == 1);
  assert(m_children[0].m_name.m_kind == TokenKind::Tok_Integer);
  llvm::APInt rv;
  assert(!m_children[0].m_name.m_text.getAsInteger(10, rv));
  return rv;
}

NodePointer *Item::get_outgoing_node() { return this; }
EdgePointer *Item::get_edge_ptr() { return this; }
void Item::for_each_outgoing_edge(std::function<void(EdgePointer &)> do_this) {
  for (auto child : m_children) {
    do_this(child);
  }
}

NodePointer *Item::get_node_ptr() { return this; }
std::string Item::line_repr() {
  if (m_type == m_name) {
    return std::string(m_name.m_text);
  }
  return llvm::formatv("{0}:{1}", m_name.m_text, m_type.m_text);
}
size_t Item::get_unique_id() { return reinterpret_cast<std::size_t>(this); }

CodeLocation Item::getLocation() const {
  if (m_name) {
    return m_name.m_location;
  }
  if (m_type)
    return m_type.m_location;
  for (auto child : m_children) {
    return child.getLocation();
  }
  llvm_unreachable("Empty Item!");
}

void rec_printtree(Item &item, int indent, std::string &out) {
  for (int i = 0; i < indent; i++)
    out += " ";
  out += llvm::formatv("{0}:{1}\n", item.m_name.str(), item.m_type.str());
  for (auto child : item.m_children) {
    rec_printtree(child, indent + 1, out);
  }
}

std::string Item::printTree() {
  std::string out;
  rec_printtree(*this, 0, out);
  return out;
}

// Blocks

template <class Item> Item readItem(Lexer &lexer);

// DelayBlock

template <> DelayBlock readItem<DelayBlock>(Lexer &lexer) {
  DelayBlock rv;
  rv.m_name = lexer.consume(TokenKind::Tok_delay);
  rv.m_type = rv.m_name;

  lexer.consume('{');
  lexer.readList(
      [&]() {
        auto identifier = lexer.consume(TokenKind::Tok_Identifier);
        auto delays = Item(identifier);
        if (lexer.m_currentToken.m_kind == TokenKind::Tok_Colon) {
          lexer.consume(':');
          delays.m_type = lexer.consume(TokenKind::Tok_Integer);
        }
        if (lexer.m_currentToken.m_kind == TokenKind::Tok_Assign) {
          lexer.consume('=');
          lexer.readList(
              [&]() {
                auto value = lexer.consumeLiteral();
                delays.m_children.push_back(value);
              },
              ',', ';', Lexer::ListLastSeparatorBehavior::FORBIDDEN);
        } else {
          lexer.consume(';');
        }
        rv.m_children.push_back(delays);
      },
      TokenKind::Tok_None, TokenKind::Tok_BraceClose,
      Lexer::ListLastSeparatorBehavior::FORBIDDEN);
  return rv;
}

// BasedonBlock

template <> BasedonBlock readItem<BasedonBlock>(Lexer &lexer) {
  BasedonBlock rv;
  rv.m_name = lexer.consume(TokenKind::Tok_basedon);
  rv.m_type = rv.m_name;
  lexer.consume('{');
  lexer.readList(
      [&]() {
        rv.m_children.push_back(lexer.consume(TokenKind::Tok_Identifier));
      },
      ';', '}', Lexer::ListLastSeparatorBehavior::REQUIRED);
  return rv;
}

// TopologyBlock

template <> TopologyBlock readItem<TopologyBlock>(Lexer &lexer) {
  using Kind = TokenKind;

  TopologyBlock rv;
  rv.m_name = rv.m_type = lexer.consume(Kind::Tok_topology);
  lexer.consume('{');

  int node_count = 0;
  int edge_count = 0;

  while (node_count == 0 || edge_count == 0) {
    switch (lexer.m_currentToken.m_kind) {
    case Kind::Tok_nodes: {
      if (node_count > 0) {
        fail(lexer.m_currentToken.m_location,
             "Repeated `nodes` field is not allowed");
      }
      Item node_decls;
      node_decls.m_type = node_decls.m_name = lexer.consume();
      lexer.consume('=');
      lexer.readList(
          [&]() {
            node_count += 1;
            node_decls.m_children.push_back(
                {lexer.consume(Kind::Tok_Identifier), Token(), {}});
          },
          ',', ';');
      rv.m_children.push_back(node_decls);
    } break;
    case Kind::Tok_edges: {
      if (edge_count > 0) {
        fail(lexer.m_currentToken.m_location,
             "Repeated `edges` field is not allowed");
      }
      Item edge_decls;
      edge_decls.m_type = edge_decls.m_name = lexer.consume();
      lexer.consume('=');
      lexer.readList(
          [&]() {
            edge_count += 1;
            auto name = lexer.consume(Kind::Tok_Identifier);
            auto producer = lexer.consume('(', Kind::Tok_Identifier);
            auto consumer = lexer.consume(',', Kind::Tok_Identifier);

            lexer.consume(')');

            auto edge = Item(name, {Item(producer, consumer)});
            edge_decls.m_children.push_back(edge);
          },
          ',', ';');
      rv.m_children.push_back(edge_decls);
    } break;
    default: {
      fail(lexer.m_currentToken.m_location,
           llvm::formatv("Expected `nodes` or `edges`, got {0}",
                         lexer.m_currentToken.str()));
    }
    }
  }
  lexer.consume('}');
  return rv;
}

// InterfaceBlock

template <> InterfaceBlock readItem<InterfaceBlock>(Lexer &lexer) {
  InterfaceBlock rv;
  rv.m_name = lexer.consume(TokenKind::Tok_interface);
  rv.m_type = rv.m_name;
  lexer.consume('{');
  // bool has_read_consumption = false;
  // bool has_read_production = false;
  do {
    switch (lexer.m_currentToken.m_kind) {
    case TokenKind::Tok_consumption:
      // if (has_read_consumption) {
      //   fail(lexer.m_currentToken.m_location, "Duplicate `consumption`
      //   field.");
      // }
      // has_read_consumption = true;
      break;
    case TokenKind::Tok_production:
      // if (has_read_production) {
      //   fail(lexer.m_currentToken.m_location, "Duplicate `production`
      //   field.");
      // }
      // has_read_production = true;
      break;
    default:
      fail(lexer.m_currentToken.m_location,
           llvm::formatv("Expected `consumption` or `production`; found {0}",
                         lexer.m_currentToken.str()));
      break;
    }
    auto header = lexer.consume();
    auto item = Item(header, header);
    lexer.readList(
        [&]() {
          auto port = lexer.readAssignment();
          item.m_children.push_back(port);
        },
        charToToken(','), charToToken(';'));
    rv.m_children.push_back(item);
  } while (lexer.m_currentToken.m_kind != charToToken('}'));
  lexer.consume('}');
  return rv;
}

// DatatypeBlock

template <> DatatypeBlock readItem<DatatypeBlock>(Lexer &lexer) {
  DatatypeBlock rv;
  rv.m_name = lexer.consume(TokenKind::Tok_datatype);
  rv.m_type = rv.m_name;
  lexer.consume('{');
  // bool has_read_consumption = false;
  // bool has_read_production = false;
  lexer.readList(
      [&]() {
        auto newtype = lexer.readAssignment();
        rv.m_children.push_back(newtype);
      },
      charToToken(';'), charToToken('}'),
      Lexer::ListLastSeparatorBehavior::REQUIRED);
  return rv;
}

// ParameterBlock

template <> ParameterBlock readItem<ParameterBlock>(Lexer &lexer) {
  ParameterBlock rv;
  rv.m_name = lexer.consume(TokenKind::Tok_parameter);
  rv.m_type = rv.m_name;
  lexer.consume('{');
  do {
    rv.m_children.push_back(lexer.readAssignment());
    lexer.consume(';');
  } while (lexer.m_currentToken.m_kind != charToToken('}'));
  lexer.consume('}');
  return rv;
}

// RefinementBlock

template <> RefinementBlock readItem<RefinementBlock>(Lexer &lexer) {
  RefinementBlock rv;
  rv.m_type = rv.m_name = lexer.consume(TokenKind::Tok_refinement);
  lexer.consume('{');
  auto lhs = lexer.consume(TokenKind::Tok_Identifier);
  lexer.consume('=');
  auto rhs = lexer.consume(TokenKind::Tok_Identifier);
  rv.m_children.push_back(Item(lhs, std::vector<Item>{rhs}));
  lexer.readList(
      [&]() {
        auto name = lexer.consume(TokenKind::Tok_Identifier);
        lexer.consume(':');
        auto type = lexer.consume(TokenKind::Tok_Identifier);
        rv.m_children.push_back(Item(name, type));
      },
      ';', '}', Lexer::ListLastSeparatorBehavior::REQUIRED);
  return rv;
}

// AttributeBlock

template <> AttributeBlock readItem<AttributeBlock>(Lexer &lexer) {
  AttributeBlock rv;
  rv.m_type = lexer.consume(TokenKind::Tok_attribute);
  rv.m_name = lexer.consume(TokenKind::Tok_Identifier);
  lexer.consume('{');
  do {
    rv.m_children.push_back(lexer.readAssignment());
    lexer.consume(';');
  } while (lexer.m_currentToken.m_kind != charToToken('}'));
  lexer.consume('}');
  return rv;
}

// ModelAttrBlock

template <> ModelAttrBlock readItem<ModelAttrBlock>(Lexer &lexer) {
  ModelAttrBlock rv;
  rv.m_type = lexer.consume(TokenKind::Tok_attribute);
  rv.m_name = lexer.consume(TokenKind::Tok_Identifier);
  lexer.consume('{');
  do {
    rv.m_children.push_back(lexer.readAssignment());
    lexer.consume(';');
  } while (lexer.m_currentToken.m_kind != charToToken('}'));
  lexer.consume('}');
  return rv;
}

// ActorTypeBlock

template <> ActorTypeBlock readItem<ActorTypeBlock>(Lexer &lexer) {
  ActorTypeBlock rv;
  rv.m_type = lexer.consume(TokenKind::Tok_actortype);
  rv.m_name = lexer.consume(TokenKind::Tok_Identifier);
  lexer.consume('{');
  do {
    switch (lexer.m_currentToken.m_kind) {
    // case TokenKind::Tok_input:
    // case TokenKind::Tok_output:
    case TokenKind::Tok_param:
    case TokenKind::Tok_consumption:
    case TokenKind::Tok_production:
    case TokenKind::Tok_mode: {
      Item child;
      child.m_type = lexer.consume();
      child.m_name = child.m_type;
      lexer.readList(
          [&]() {
            auto assignment = lexer.readAssignment();
            child.m_children.push_back(assignment);
          },
          charToToken(','), charToToken(';'),
          Lexer::ListLastSeparatorBehavior::FORBIDDEN);
      rv.m_children.push_back(child);
    } break;
    // case TokenKind::Tok_production:
    // case TokenKind::Tok_consumption: {
    //   Item child;
    //   child.m_type = lexer.consume();
    //   child.m_name = child.m_type;
    //   lexer.readList(
    //       [&]() {
    //         auto name = lexer.consume(TokenKind::Tok_Identifier);
    //         auto value = lexer.consume(':', TokenKind::Tok_Integer);
    //         dif::Item port{name};
    //         port.m_children.push_back(value);
    //         child.m_children.push_back(port);
    //       },
    //       charToToken(','), charToToken(';'),
    //       Lexer::ClosingSeparator::FORBIDDEN);
    //   rv.m_children.push_back(child);
    // } break;
    default:
      fail(lexer.m_currentToken.m_location,
           llvm::formatv("Expected `input`, `output`, `param`, `mode`, "
                         "`production` or `consumption`; found {0}",
                         lexer.m_currentToken.str()));
      break;
    }
  } while (lexer.m_currentToken.m_kind != charToToken('}'));
  lexer.consume('}');
  return rv;
}

// ActorBlock

template <> ActorBlock readItem<ActorBlock>(Lexer &lexer) {
  ActorBlock rv;
  rv.m_type = lexer.consume(TokenKind::Tok_actor);
  rv.m_name = lexer.consume(TokenKind::Tok_Identifier);
  lexer.consume('{');
  do {
    Item item;
    switch (lexer.m_currentToken.m_kind) {
    case TokenKind::Tok_type: {
      item.m_type = lexer.consume();
      lexer.consume(':');
      item.m_name = lexer.consume(TokenKind::Tok_Identifier);
      lexer.consume(';');
    } break;
    case TokenKind::Tok_interface: {
      item.m_name = item.m_type = lexer.consume();
      lexer.readList(
          [&]() {
            auto lhs = lexer.consume(TokenKind::Tok_Identifier);
            auto arrow = lexer.consume(TokenKind::Tok_Arrow);
            auto rhs = lexer.consume(TokenKind::Tok_Identifier);
            item.m_children.push_back(Item(lhs, rhs));
          },
          ',', ';', Lexer::ListLastSeparatorBehavior::FORBIDDEN);
    } break;
    case TokenKind::Tok_param: {
      item.m_type = lexer.consume();
      item.m_name = item.m_type;
      lexer.readList(
          [&]() { item.m_children.push_back(lexer.readAssignment()); }, ',',
          ';', Lexer::ListLastSeparatorBehavior::FORBIDDEN);
    } break;
    case TokenKind::Tok_codegen: {
      item.m_type = item.m_name = lexer.consume();
      lexer.readList(
          [&]() {
            Item item_;
            lexer.consume('@');
            item_.m_name = lexer.consume(TokenKind::Tok_Identifier);
            lexer.consume('(');
            item_.m_type = lexer.consume(TokenKind::Tok_Identifier);
            lexer.consume(')');
            item.m_children.push_back(item_);
          },
          ',', ';', Lexer::ListLastSeparatorBehavior::FORBIDDEN);
    } break;
    default: {
      fail(lexer.m_currentToken.m_location,
           llvm::formatv("Expected one of `type`, `interface`, `param`, "
                         "`codegen`, received {0}",
                         lexer.m_currentToken.str()));
    } break;
    }
    rv.m_children.push_back(item);
  } while (lexer.m_currentToken.m_kind != charToToken('}'));
  lexer.consume('}');
  return rv;
}

// Graph

template <> Graph readItem<Graph>(Lexer &lexer) {
  Graph rv;
  rv.m_type = lexer.consume(TokenKind::Tok_Identifier);
  rv.m_name = lexer.consume(TokenKind::Tok_Identifier);
  lexer.consume('{');

  while (lexer.m_currentToken.m_kind != charToToken('}')) {
    switch (lexer.m_currentToken.m_kind) {

    case TokenKind::Tok_basedon:
      rv.m_children.push_back(readItem<BasedonBlock>(lexer));
      break;
    case TokenKind::Tok_topology:
      rv.m_children.push_back(readItem<TopologyBlock>(lexer));
      break;
    case TokenKind::Tok_parameter:
      rv.m_children.push_back(readItem<ParameterBlock>(lexer));
      break;
    case TokenKind::Tok_refinement:
      rv.m_children.push_back(readItem<RefinementBlock>(lexer));
      break;
    case TokenKind::Tok_attribute:
      rv.m_children.push_back(readItem<AttributeBlock>(lexer));
      break;
    case TokenKind::Tok_msa:
      rv.m_children.push_back(readItem<ModelAttrBlock>(lexer));
      break;
    case TokenKind::Tok_actortype:
      rv.m_children.push_back(readItem<ActorTypeBlock>(lexer));
      break;
    case TokenKind::Tok_actor:
      rv.m_children.push_back(readItem<ActorBlock>(lexer));
      break;
    case TokenKind::Tok_interface:
      rv.m_children.push_back(readItem<InterfaceBlock>(lexer));
      break;
    case TokenKind::Tok_delay:
      rv.m_children.push_back(readItem<DelayBlock>(lexer));
      break;
    case TokenKind::Tok_datatype:
      rv.m_children.push_back(readItem<DatatypeBlock>(lexer));
      break;

    default:
      fail(lexer.m_currentToken.m_location,
           llvm::formatv("Expected `topology`, `parameter`, `production`, "
                         "`consumption`, `attribute`, `actor`, `interface` or "
                         "`datatype`; found {0}",
                         lexer.m_currentToken.str()));
    }
  }
  lexer.consume('}');
  return rv;
}

std::vector<Graph> Lexer::readDIF() {
  std::vector<Graph> rv;
  while (m_currentToken.m_kind != TokenKind::Tok_Eof) {
    rv.push_back(readItem<Graph>(*this));
  }
  return rv;
}

void Lexer::dumpTokens() {
  while (true) {
    llvm::outs() << m_currentToken.str() << "\n";
    if (m_currentToken.m_kind == TokenKind::Tok_Eof)
      break;
    consume();
  }
}

} // namespace dif