#include "ImplOptimization/ImplOptimization.h"
#include "Dataflow/Dialect.h"
#include "mlir/Dialect/Arithmetic/IR/Arithmetic.h"
#include "mlir/Dialect/Func/IR/FuncOps.h"
#include "mlir/Dialect/LLVMIR/LLVMDialect.h"
#include "mlir/IR/BuiltinOps.h"
#include "mlir/IR/Diagnostics.h"
#include "mlir/IR/FunctionInterfaces.h"
#include "mlir/IR/Operation.h"
#include "mlir/IR/Value.h"
#include "mlir/Support/LogicalResult.h"
#include "util/ranges.h"
#include "util/unionfind.h"
#include "util/util.h"
#include "llvm/ADT/SmallVector.h"
#include "llvm/IR/Value.h"
#include "llvm/Support/Casting.h"
#include <algorithm>
#include <deque>

using GroupID = util::UnionFind::GroupID;
namespace iara {
using namespace mlir;
namespace {

using mlir::func::FuncOp;
struct MappingData {
  FuncOp func;
  KernelOp actortype;
  SmallVector<Value> operands;
  SmallVector<Value> input_operands, output_operands;
  DenseMap<Value, size_t> input_indices, output_indices;
  size_t num_parameters = 0;
  size_t num_inputs = 0;
  size_t num_outputs = 0;
  util::UnionFind unionfind;

  MappingData(FuncOp func, KernelOp actortype)
      : func(func), actortype(actortype),
        unionfind(func.getRegion().getNumArguments()) {

    size_t argument_index = 0;

    for (auto op : func.getRegion().getArguments()) {
      operands.push_back(op);
    }

    if (actortype.parameters()) {
      argument_index = num_parameters = actortype.parametersAttr().size();
    }
    if (actortype.inputs()) {
      num_inputs = actortype.inputsAttr().size();
      for (auto port : actortype.inputsAttr()) {
        auto index = argument_index++;
        input_operands.push_back(operands[index]);
        input_indices[operands[index]] = index;
      }
    }
    if (actortype.outputs()) {
      num_outputs = actortype.inputsAttr().size();
      for (auto port : actortype.outputsAttr()) {
        auto index = argument_index++;
        output_operands.push_back(operands[index]);
        output_indices[operands[index]] = index;
      }
    }
  }
};
} // namespace

// Get all operations that use the results of the given operation
SmallVector<Operation *> downstream(Operation *op) {
  SmallVector<Operation *> rv;
  for (auto result : op->getResults()) {
    for (auto user : result.getUsers()) {
      rv.push_back(user);
    }
  }
  return rv;
}

// Create a subgraph that has the same interface as this actortype.
GraphOp convertToSubgraph(KernelOp actortype) {
  auto module = actortype->getParentOfType<ModuleOp>();
  auto builder = OpBuilder::atBlockEnd(module.getBody());
  auto subgraph =
      builder.create<GraphOp>(actortype.getLoc(), actortype.sym_name(), "sdf",
                              actortype.parametersAttr());

  subgraph.getRegion().emplaceBlock();

  // populate interface

  if (actortype.inputs())
    subgraph->setAttr("interface_inputs", actortype.inputsAttr());

  if (actortype.outputs())
    subgraph->setAttr("interface_outputs", actortype.outputsAttr());

  return subgraph;
}

// Generate an actortype and a node for this newly-created function.
// All parameters and ports should be there.
void populateSubgraph(GraphOp subgraph, FuncOp new_func, MappingData &data,
                      GroupID id) {

  // Create new actortype

  auto parameters = data.actortype.parametersAttr();

  SmallVector<Attribute> input_ports, output_ports;

  for (size_t i = 0, e = data.num_inputs; i < e; i++) {
    auto operand_index = data.num_parameters + i;
    if (data.unionfind.getGroup(data.operands[operand_index].getImpl()) == id) {
      input_ports.push_back(data.actortype.inputsAttr()[i]);
    }
  }

  for (size_t i = 0, e = data.num_outputs; i < e; i++) {
    auto operand_index = data.num_parameters + data.num_inputs + i;
    if (data.unionfind.getGroup(data.operands[operand_index].getImpl()) == id) {
      output_ports.push_back(data.actortype.outputsAttr()[i]);
    }
  }

  auto builder = mlir::OpBuilder::atBlockEnd(subgraph.getBody());
  auto new_actortype =
      // KernelOp::build()
      builder.create<KernelOp>(new_func.getLoc(), new_func.getName(),
                               parameters, builder.getArrayAttr(input_ports),
                               builder.getArrayAttr(output_ports));

  std::string node_name = llvm::formatv("__{0}_node__", new_func.getName());

  auto new_actor = builder.create<NodeOp>(
      data.func.getLoc(), node_name,
      SymbolRefAttr::get(new_actortype.sym_nameAttr()), nullptr);

  auto subgraph_ref = SymbolRefAttr::get(subgraph.sym_nameAttr());
  auto actor_ref = SymbolRefAttr::get(new_actor.sym_nameAttr());

  for (auto input : input_ports) {
    auto port = input.cast<PortAttr>();
    std::string edge_name = llvm::formatv("__{0}_input_edge__", port.getName());
    builder.create<EdgeOp>(data.func.getLoc(), edge_name, subgraph_ref,
                           port.getName(), actor_ref, port.getName());
  }
  for (auto output : output_ports) {
    auto port = output.cast<PortAttr>();
    std::string edge_name =
        llvm::formatv("__{0}_output_edge__", port.getName());
    builder.create<EdgeOp>(data.func.getLoc(), edge_name, actor_ref,
                           port.getName(), subgraph_ref, port.getName());
  }
}

int splitImpl(KernelOp actortype_op, ModuleOp implementations) {
  if (actortype_op.inputs() && actortype_op.inputs()->size() <= 1) {
    // Can only split if there are more than one input.
    // TODO: split from output side as well
    return 1;
  }
  // find corresponding function in implementations
  auto impl = lookupSymbol(implementations, actortype_op.sym_name());
  if (!impl) {
    actortype_op->emitRemark()
        << "Did not find implementation for actortype "
        << actortype_op.sym_name() << " while trying to split it. Skipping.";
    return -1;
  }

  auto func = cast<FuncOp>(impl);
  if (!func) {
    impl->emitWarning() << "Actortype " << actortype_op.sym_name()
                        << " is not a function. Skipping.";
    return -1;
  }

  MappingData data(func, actortype_op);

  // From each input, find which operations depend on it
  {
    std::deque<Operation *> queue;
    std::set<Operation *> visited;

    // check if this operation refers to one of the function's output arguments
    auto checkForOutputs = [&](Operation *op) {
      for (auto operand : op->getOperands()) {
        if (llvm::count(data.output_operands, operand) == 1) {
          data.unionfind.setOrJoin(operand.getImpl(),
                                   data.unionfind.getGroup(op));
        }
      }
    };

    // Populate owners with uses
    for (size_t i = 0, end = data.input_operands.size(); i != end; i++) {
      data.unionfind.setOrJoin(data.input_operands[i].getImpl(), i);
      for (auto user : data.input_operands[i].getUsers()) {
        if (!data.unionfind.hasGroup(user)) {
          data.unionfind.setOrJoin(user, i);
          checkForOutputs(user);
          queue.push_back(user);
          visited.insert(user);
        }
      }
    }

    while (!queue.empty()) {
      auto op = queue.front();
      queue.pop_front();
      for (auto downstream_op : downstream(op)) {
        data.unionfind.setOrJoin(downstream_op, data.unionfind.getGroup(op));
        if (visited.count(downstream_op) == 0) {
          checkForOutputs(downstream_op);
          visited.insert(downstream_op);
          queue.push_back(downstream_op);
        }
      }
    }
  }

  // count all independent DAGs

  std::map<int, SmallVector<Operation *>> dags;
  SmallVector<Operation *> input_independent_ops;
  std::map<Operation *, size_t> op_indices;

  {
    size_t op_index = 0;
    for (Operation &op_ref : func.getRegion().getOps()) {
      Operation *op = &op_ref;
      op_indices[op] = op_index++;
      if (data.unionfind.hasGroup(op)) {
        dags[data.unionfind.getGroup(op)].push_back(op);
      } else {
        input_independent_ops.push_back(op);
      }
    }
  }

  if (dags.size() == 1) {
    // Do nothing. TODO: try to split starting from the outputs too.
    return 1;
  }

  // for (auto &[parent, ops] : dags) {
  //   // llvm::outs() << "Group " << parent << ":\n";
  //   // for (auto op : ops) {
  //   //   op->dump();
  //   // }
  // }

  // llvm::outs() << "Ops without input dependence:\n";

  // for (auto op : input_independent_ops) {
  //   op->dump();
  // }

  // create subgraphs and functions;

  auto topology = actortype_op->getParentOfType<ModuleOp>();
  auto impl_builder = OpBuilder::atBlockEnd(implementations.getBody());
  auto top_builder = OpBuilder::atBlockEnd(topology.getBody());

  auto subgraph_parameters =
      actortype_op.parameters() ? *actortype_op.parameters() : nullptr;

  GraphOp subgraph = convertToSubgraph(actortype_op);
  auto subgrap_builder = OpBuilder::atBlockEnd(subgraph.getBody());

  for (auto &[id, group_ops] : dags) {

    std::set<size_t> input_indices_to_keep;

    std::set<size_t> op_indices_to_keep;
    // delete functions in other components

    auto x = {input_independent_ops, group_ops};

    for (auto &range : {input_independent_ops, group_ops}) {
      for (auto op : range) {
        op_indices_to_keep.insert(op_indices[op]);
      }
    }

    auto new_function = data.func.clone();
    {
      SmallVector<Operation *> to_delete;

      {
        size_t index = 0;
        for (auto &op : new_function.getOps()) {
          if (op_indices_to_keep.count(index++) == 0) {
            to_delete.push_back(&op);
          }
        }
      }

      for (auto op : to_delete) {
        op->dropAllDefinedValueUses();
        op->dropAllReferences();
        op->dropAllUses();
        op->remove();
      }
    }

    // new_function.dump();

    // Remove other groups's inputs and outputs
    {
      BitVector port_indices_to_remove(data.operands.size());

      for (unsigned int index = data.num_parameters, end = data.operands.size();
           index < end; index++) {
        auto group = data.unionfind.getGroup(data.operands[index].getImpl());
        if (group != id) {
          port_indices_to_remove.set(index);
        }
      }

      new_function.eraseArguments(port_indices_to_remove);
    }

    // new_function.dump();

    // new name

    std::string new_name =
        llvm::formatv("__{0}_g{1}__", data.func.getName(), id);
    new_function.setName(new_name);
    impl_builder.insert(new_function);

    populateSubgraph(subgraph, new_function, data, id);
  }
  actortype_op->remove();
  // subgraph->getParentOfType<ModuleOp>().dump();
  // implementations.dump();
  return dags.size();
}

LogicalResult splitImpls(ModuleOp topology, ModuleOp impls) {
  LogicalResult result = success();
  // topology.dump();
  topology->walk([&](KernelOp op) {
    if (splitImpl(op, impls) == -1) {
      result = failure();
    };
  });
  return result;
}

// Return number of parameters baked, or -1 if error.
int bakeParameters(NodeOp node, ModuleOp impls) {
  auto impl = lookupSymbol(node, node.implementation());
  auto actortype = llvm::dyn_cast_or_null<KernelOp>(impl);
  if (!actortype) {
    return 0; // don't bake subgraphs, broadcast nodes don't have parameters.
  }

  auto func =
      dyn_cast_or_null<FuncOp>(lookupSymbol(impls, node.implementation()));

  if (!func || func->getNumRegions() != 1 ||
      func.getBody().getBlocks().size() != 1) {
    return 0; // Implementation not found.
  }

  if (!actortype.parameters()) {
    return 0; // no parameters to bake.
  }

  // create new function's name

  std::string new_name = llvm::formatv("{0}_baked", func.getName());

  SmallVector<IntegerAttr> resolved_parameters;

  for (auto attr : actortype.parametersAttr()) {
    Attribute resolved_parameter =
        resolveParameter(node, attr.cast<ParameterAttr>());
    auto param_as_int = resolved_parameter.dyn_cast_or_null<IntegerAttr>();
    if (!param_as_int) {
      return -1;
    }
    new_name = llvm::formatv("{0}_{1}", new_name, param_as_int.getInt());
    resolved_parameters.push_back(param_as_int);
  }

  auto existing_new_actortype =
      dyn_cast_or_null<KernelOp>(lookupSymbol(node, new_name));

  if (existing_new_actortype) {
    // A function with this combination of parameters has already been created.
    node->setAttr("implementation",
                  FlatSymbolRefAttr::get(existing_new_actortype.getNameAttr()));
    if (node->hasAttr("param_overrides")) {
      node->removeAttr("param_overrides");
    }
    return resolved_parameters.size();
  }

  // Create new function

  auto new_func = func.clone();
  new_func.setName(new_name);

  auto funcBuilder =
      OpBuilder::atBlockBegin(&new_func.getBody().getBlocks().front());

  for (auto attr : resolved_parameters) {
    auto const_op =
        funcBuilder.create<arith::ConstantOp>(actortype.getLoc(), attr);

    new_func.getArguments().front().replaceAllUsesWith(const_op.getResult());
    new_func.eraseArgument(0);
  }

  auto implsBuilder = OpBuilder::atBlockEnd(impls.getBody());

  implsBuilder.insert(new_func);

  // Create new actortype

  auto topologyBuilder = OpBuilder::atBlockBegin(actortype->getBlock());

  auto new_actortype = actortype.clone();
  new_actortype.removeParametersAttr();
  new_actortype.setName(new_name);

  topologyBuilder.setInsertionPointAfter(actortype);
  topologyBuilder.insert(new_actortype);

  // Set node to use new actortype

  node->setAttr("implementation",
                FlatSymbolRefAttr::get(new_actortype.getNameAttr()));
  if (node->hasAttr("param_overrides")) {
    node->removeAttr("param_overrides");
  }
  // impls.dump();
  // node->getParentOfType<ModuleOp>()->dump();

  return resolved_parameters.size();
}

size_t removeUnusedActortypes(ModuleOp topology, ModuleOp impls) {
  DenseSet<KernelOp> unused_actortypes;

  topology->walk(
      [&](KernelOp actortype) { unused_actortypes.insert(actortype); });

  topology->walk([&](NodeOp op) {
    auto actortype =
        dyn_cast_or_null<KernelOp>(lookupSymbol(op, op.implementation()));
    if (actortype) {
      unused_actortypes.erase(actortype);
    }
  });

  size_t removed = 0;

  for (auto unused : unused_actortypes) {
    auto func = lookupSymbol(impls, unused.getNameAttr());
    if (func)
      func->erase();
    unused.erase();
    removed++;
  }

  return removed;
};

LogicalResult bakeParameters(ModuleOp topology, ModuleOp impls) {
  LogicalResult result = success();
  // topology.dump();
  topology->walk([&](NodeOp op) {
    if (bakeParameters(op, impls) == -1) {
      result = failure();
    };
  });

  return result;
}

DenseMap<NodeOp, SmallVector<StringRef>>
get_unused_output_ports(GraphOp topology) {
  DenseSet<std::pair<NodeOp, StringRef>> used_output_ports;
  DenseMap<NodeOp, SmallVector<StringRef>> unused_output_ports;

  for (auto &op : topology.getOps()) {
    auto edge = dyn_cast<EdgeOp>(op);
    if (!edge)
      continue;
    auto prod_node = dyn_cast_or_null<NodeOp>(lookupSymbol(edge, edge.prod()));
    if (!prod_node)
      continue;
    used_output_ports.insert({prod_node, edge.prod_port()});
  }
  for (auto &op : topology.getOps()) {
    auto node = dyn_cast<NodeOp>(op);
    if (!node)
      continue;
    auto actortype =
        dyn_cast_or_null<KernelOp>(lookupSymbol(node, node.implementation()));
    if (!actortype)
      continue;
    if (actortype.outputs())
      for (auto attr : actortype.outputsAttr()) {
        auto port = attr.cast<PortAttr>().getName().getValue();
        auto pair = std::make_pair(node, port);
        if (used_output_ports.count(pair) == 0) {
          unused_output_ports[node].push_back(port);
        }
      }
  }
  return unused_output_ports;
}

// Duplicates an actortype and its corresponding implementation, and gives them
// a matching new name.
Optional<std::pair<FuncOp, KernelOp>>
duplicate_actortype(KernelOp actortype, ModuleOp impls, StringRef new_name) {
  auto old_function =
      dyn_cast_or_null<FuncOp>(lookupSymbol(impls, actortype.sym_name()));
  if (!old_function)
    return {};
  auto new_function = old_function.clone();
  new_function.setName(new_name);
  OpBuilder::atBlockEnd(impls.getBody()).insert(new_function);
  auto topology_builder = OpBuilder(actortype->getParentOfType<ModuleOp>());
  topology_builder.setInsertionPointAfter(actortype);
  auto new_actortype = actortype.clone();
  new_actortype.setName(new_name);
  topology_builder.insert(new_actortype);
  return {{new_function, new_actortype}};
}

// Returns the function's operands that correspond to the actortype's output
// ports.
DenseSet<Value> get_output_operands(FuncOp func, KernelOp actortype) {

  DenseSet<Value> rv;

  int offset = 0;
  if (actortype.parameters())
    offset += actortype.parametersAttr().size();
  if (actortype.inputs())
    offset += actortype.inputsAttr().size();
  auto arguments_list = func.getCallableRegion()->getArguments();
  auto l = arguments_list.begin();
  auto r = arguments_list.end();
  while (offset--)
    l++;
  for (; l != r; l++) {
    rv.insert(*l);
  }
  return rv;
}

struct DependencyData {
  // Output -> All operations that will necessarily be removed if it is removed.
  DenseMap<Value, DenseSet<Operation *>> strict_dependencies;
  // Operation -> Value: one of this operation's results flows into one of the
  // strict dependencies of the value.
  DenseMap<Operation *, DenseSet<Value>> lax_dependencies;
};

// Given a list of output operands, returns a dictionary that maps each
// operation in the function with a subset of the list. An output is in an
// operation's subset if one of the operation's results flows into an operation
// that would disappear if the output disappears.
DependencyData get_output_dependencies(FuncOp function,
                                       SmallVector<Value> outputs) {
  DependencyData rv;

  // First, get exclusive dependencies: everything that will necessarily
  // disappear if the operand disappears.

  for (auto output : outputs) {
    // get all ops that require this.

    auto populate_strict_dependencies =
        [&](Value value, auto &populate_strict_dependencies) -> void {
      for (auto op : value.getUsers()) {
        // op->dump();
        rv.strict_dependencies[output].insert(op);
        for (auto result : op->getResults()) {
          populate_strict_dependencies(result, populate_strict_dependencies);
        }
      }
    };
    populate_strict_dependencies(output, populate_strict_dependencies);

    // Any ops that flow into one of the strict dependencies should be marked.

    auto bubble_up = [&](Operation *operation, auto &bubble_up) -> void {
      if (!operation)
        return;
      rv.lax_dependencies[operation].insert(output);
      for (auto operand : operation->getOperands()) {
        bubble_up(operand.getDefiningOp(), bubble_up);
      }
    };

    for (auto strict_dep : rv.strict_dependencies[output]) {
      bubble_up(strict_dep, bubble_up);
    }
  }

  return rv;
}

// Returns vector with indices of the named parameters or ports.
SmallVector<unsigned> get_indices(KernelOp actortype,
                                  SmallVector<StringRef> names) {
  SmallVector<unsigned> rv;

  unsigned index = 0;

  if (actortype.parameters())
    for (auto &attr : actortype.parametersAttr()) {
      auto param = attr.cast<ParameterAttr>();
      if (llvm::is_contained(names, param.getName())) {
        rv.push_back(index);
      }
      index++;
    }

  if (actortype.inputs())
    for (auto &attr : actortype.inputsAttr()) {
      auto port = attr.cast<PortAttr>();
      if (llvm::is_contained(names, port.getName())) {
        rv.push_back(index);
      }
      index++;
    }

  if (actortype.outputs())
    for (auto &attr : actortype.outputsAttr()) {
      auto port = attr.cast<PortAttr>();
      if (llvm::is_contained(names, port.getName())) {
        rv.push_back(index);
      }
      index++;
    }

  return rv;
}

// Transforms function and actortype, removing outputs with given names.
LogicalResult remove_unused_outputs(FuncOp impl, KernelOp actortype,
                                    SmallVector<StringRef> &port_names) {
  if (!actortype.outputs())
    return success();

  auto indices_to_remove = get_indices(actortype, port_names);
  unsigned outputs_offset = 0;
  if (actortype.parameters())
    outputs_offset += actortype.parametersAttr().size();
  if (actortype.inputs())
    outputs_offset += actortype.inputsAttr().size();

  unsigned total_arg_count = outputs_offset;
  if (actortype.outputs())
    total_arg_count += actortype.outputsAttr().size();

  // Update actortype

  SmallVector<Attribute> new_attrs;

  unsigned index = 0;

  for (auto attr : actortype.outputsAttr()) {
    if (llvm::count(indices_to_remove, index + outputs_offset) == 0) {
      new_attrs.push_back(attr);
    }
    index++;
  }

  auto new_output_attrs = ArrayAttr::get(actortype.getContext(), new_attrs);

  if (new_output_attrs.size() > 0) {
    actortype->setAttr("outputs", new_output_attrs);
  } else {
    actortype->removeAttr("outputs");
  }

  // Update function

  SmallVector<Value> all_outputs;
  SmallVector<Value> unused_outputs;

  index = 0;
  for (auto arg : impl.getArguments()) {
    if (index >= outputs_offset) {
      all_outputs.push_back(arg);
      if (llvm::count(indices_to_remove, index) > 0) {
        unused_outputs.push_back(arg);
      }
    }
    index++;
  }

  auto dependencies = get_output_dependencies(impl, all_outputs);
  DenseSet<Operation *> ops_to_remove;

  // For now, we'll rely on Clang's built-in DCE. Just remove all
  // references to this pointer.
  for (auto output : unused_outputs) {
    for (auto strict_dep : dependencies.strict_dependencies[output]) {
      ops_to_remove.insert(strict_dep);
    }
  }

  // for (auto &[op, associated_outputs] : dependencies.lax_dependencies) {

  //   if (llvm::all_of(associated_outputs, [&](auto output) {
  //         return llvm::is_contained(unused_outputs, output);
  //       })) {
  //     ops_to_remove.insert(op);
  //   }
  // }

  // llvm::errs() << "\n\nOPS TO REMOVE \n\n";

  // for (auto op : ops_to_remove) {
  //   op->dump();
  //   for (auto lax_dep : dependencies.lax_dependencies[op]) {
  //     llvm::errs() << "          depends on ";
  //     lax_dep.dump();
  //     llvm::errs() << "\n";
  //   }
  // }

  // llvm::errs() << "\n\nOPS TO REMOVE END\n\n";

  for (auto op : ops_to_remove) {
    op->dropAllUses();
    op->erase();
  }

  BitVector indices_to_remove_bitvector(total_arg_count);
  for (auto i : indices_to_remove)
    indices_to_remove_bitvector[i] = true;

  impl.eraseArguments(indices_to_remove_bitvector);

  return success();
}

LogicalResult eliminateDeadCode(GraphOp topology, ModuleOp impls) {
  LogicalResult result = success();

  auto unused_output_ports = get_unused_output_ports(topology);

  for (auto &[node, unused_ports] : unused_output_ports) {
    auto actortype = cast<KernelOp>(lookupSymbol(node, node.implementation()));

    // Duplicate function and actortype.
    std::string new_func_name =
        llvm::formatv("{0}_RemoveOutputs", actortype.sym_name());

    for (auto &unused_port : unused_ports) {
      new_func_name += ("_" + unused_port).str();
    }

    auto duplicates = duplicate_actortype(actortype, impls, new_func_name);

    if (!duplicates) {
      // Could not duplicate actortype or function.
      continue;
    }

    auto [new_func, new_actortype] = *duplicates;

    remove_unused_outputs(new_func, new_actortype, unused_ports);

    // Change node to point to new actortype.

    node->setAttr("implementation",
                  FlatSymbolRefAttr::get(new_actortype.getNameAttr()));
  }

  return result;
}

// Removes operations that lead to unused output ports.
LogicalResult eliminateDeadCode(ModuleOp topology, ModuleOp impls) {
  for (auto &child : *topology.getBody()) {
    auto topology = dyn_cast<GraphOp>(child);
    if (!topology)
      continue;
    if (eliminateDeadCode(topology, impls).failed())
      return failure();
  }
  return success();
}

}; // namespace iara