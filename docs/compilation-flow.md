# Standardized C flow (stdcflow)

This is a specification of DFMLIR's compilation using plain DIF files that expects C files in a standardized format.

Basic compilation flow:

```mermaid
flowchart TD
    style a pre,text-align:left
    DIF -->
    dfmlir(dfmlir) -->
    a["dfmlir topology dialect"] -->
    b(Scheduler) -->
    d[standard MLIR] -->
    e[LLVM IR] -->
    compiler(clang/gcc) -->
    executable
    c[Standardized C source] --> compiler
```

Notes about terminology:

- `Actors` refer to the `actortype` block in DIF: the base class of a node, which includes name, ports (with names, types and rates) and a reference to the implementation, which can be a C function in the specified format or a sub-graph.
- `Nodes` refer to each instance of an actor. They include names and a reference to their respective actor

The DFMLIR topology dialect consists of a module with the following AST:

- Graph
  - Parameters (optional) (TODO)
  - Ports (optional) (TODO)
  - Actors
    - Name
    - Input ports
    - Output ports
    - Ports
      - Port name
      - Datatype
      - Rate info
        - Positive integer for SDF
    - Parameter value overrides
  - Nodes
    - Actor name
    - Parameter value overrides
  - Edges
    - Producer node and port name
    - Consumer node and port name

An example of valid MLIR would be:

```
"dataflow.graphDecl" graph_name : "dataflow.SDF" {
    "dataflow.actorDecl" @actor_name
        inputs {
            "dataflow.portDecl" inp_name : datatype : rate
        }
        outputs {
            "dataflow.portDecl" outp_name : datatype : rate
        }
        parameters {
            "dataflow.parameterDecl" par_name : datatype : value
        }
    "dataflow.nodeDecl" @node_name : @actor_name
    parameters {
        "dataflow.parameterDecl" par_name : datatype : value
    }
    "dataflow.edgeDecl" @node_name : @outp_name -> @node_name : @outp_name
}
```

The expected format for the implementation of the actors in the SDF Standardized C Flow is as follows:

If an actor has:

- name `actor_name`;
- an input port named `inp` of type `char` and rate 10;
- an output port named `outp` of type `float` and rate 100;
- a `height` paramter of type int
- a `width` parameter of type int

There should be:

- A `void actor_name_init(int const width, int const height)` function.
- A `void actor_name_impl(int const width, int const height, char const * inp, char * outp)` function.

The arguments should follow the order: parameters, input ports, output ports. `actor_name_init` only receives the actor parameters. The order should match the definition order in the source DIF or DFMLIR.

- Parameters have type `<datatype>`.
- Input ports have type `<datatype> const *`.
- Ouput ports have type `<datatype> *`.
