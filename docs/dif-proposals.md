# DIF Proposals
`Pedro Ciambra pedrociambra@gmail.com`

This is a document detailing a number of proposals for DIF functionality and/or
standards.

Not all of these would require changes to the grammar; some of these can be
currently provided with attribute blocks and could be published as "official
recommendations" for tool developers.

Refer to
[this example](https://github.com/jserot/hocl/tree/master/examples/working/apps/sobel1)
of the HoCL tool for an idea of the use case I have in mind.

## 1. Datatype notation and suppport

To connect actors implemented in different languages, a common notation for the
datatypes is desirable.

Dataflow is generally agnostic to the implementation details of an algorithm,
which means that tokens could represent any type and size of data. However, some
datatypes are ubiquitous in DSP applications. As supporting arbitrary structs
could add unnecessary complexity to the language, I propose the adoption of
standard notation for a number of common datatypes:

- boolean: `u1` or `bool`
- integers: `u8`, `u16`, `u32`, `u64`, `u128` and `i8`, `i16`, `i32` ,`i64`,
  `i128`
- floating-point numbers: `f16`, `f32`, `f64`, `f128`
- fixed-size memory-contiguous tensors: u8[10], u8[64*64],
  `i64[some_parameter]` etc.

These are different from the C specification because the actual sizes of `int`,
`long` etc varies between compilers and targets. These names, inspired by the
Rust language, aim to preserve data compatibility and some form of
future-proofing.

Other possible naming conventions are
[LLVM](https://llvm.org/docs/LangRef.html#t-firstclass)'s and
[MLIR](https://mlir.llvm.org/docs/Dialects/Builtin/#types)'s:
`ui64` for unsized integers, `si64` for sized integers, and `i64` for signless.

Parameters have fewer limitations: some C algorithms use strings in the
configuration phase. It could be useful to add a way to annotate different
encoding schemes and between size-prefixed and null-terminated strings, such as

`NullTerminatedAsciiString`,
`U64PrefixedUTF16String`,
etc.


## 2. Datatype aliases

Sometimes it is useful to define an alias to a datatype. For instance, an image
manipulation algorithm may use 3-byte tokens that represent an RGB color.
Instead of writing `u8[3]` everywhere, it could be useful to allow a new
`alias` block to declare aliases, such as:

```
alias {
    Color = u8[3];
}
```

```
attribute alias {
    "Color" = "u8[3]";
}
```

## 3. Datatype constraint in actortype block

For SDF static scheduling, channel (FIFO) sizes can be determined before linking
to actor implementations. However, the type (or at least the size) of the token
needs to be known before allocating memory. The same is useful for parameters.

The `actortype` block could allow for type specification with the `name: type`
syntax.

Examples:

```
actortype sobel {
    input inp: u8;
    output outp: u8;
    param width: u32, height: u32;
  }
```

```
attribute datatype {
    "sobel.inp" = u8;      // No member access syntax; use string instead
    "sobel.outp" = u8;
    "sobel.width" = u32;
    "sobel.height" = u32;
}
```

```
attribute datatype {
    e1 = u8;       // Alternatively, type the edges
    e2 = i64;
    e3 = f32[4];
}
```

## 4. Actor implementation specification

An actor's implementation could conceivably be provided in a variety of forms,
such as C/C++ source code, a Java class, a shell command, a REST interface (for
distributed computing), or another DIF graph (in the same or different DIF
file).

Here I propose a format for arbitrary C function linking, but similar
specifications could be developed for other implementation formats.

### Arbitrary C function specification

Supporting arbitrary C function calls would facilitate compatibility with most
programming languages and allow delegating the interface validation to the
linker.

This would allow for algorithms in existing C libraries to be used without
modifying any C code. It requires all of the information necessary to create a
function call:

- function signatures of actor, init and subinit implementations
- mapping of input ports and parameters to function signature. For instance,
  `input inp: u8` could be mapped:
    - as pointer to read buffer: `void f(unsigned char *_some_input, ...)`
    - as pointer to const read buffer:
      `void f( unsigned char const *_some_input, ...)`
    - as a by-value argument (for ports with rate 1):
      `void f(unsigned char _some_input, ...)`

- mapping the output ports. `output outp: u8` could be mapped to:
    - a pointer to buffer: `void f(..., unsigned char* _some_output)`
    - the return value (for ports with rate 1): `unsigned char f(...)`

To avoid repetition, it could be useful to explicitly allow deduction from the
actortype's name, ports and parameters.

Example:

```
  actortype sobel {
    input inp: u8;
    output outp: u8;
    param width: u32, height:u32;
    implementation C {
        init: void sobelInit (width, height);
        actor: void sobel (width, height, inp : char const * _i, outp : char * _o);
    }
    ...
  }

  actortype f {
    input inp: u8;
    output outp: u8;
    implementation C {
        actor: outp = f (inp);
    }
    ...
  }

```

In the first example, the signatures of width and height are deduced from the
DIF parameterss. The `inp` and `outp` ports are mapped to the `_i` and `_o`
parameters of the C function.

In the second example, `f()`'s signature is assumed to be
`unsigned char f(unsigned char inp)`, and the return value is mapped to `outp`.

As an attribute block:

```
  attribute implementation {
    "sobel::init" = "void sobelInit (width, height)";
    "sobel::actor" = "void sobel (width, height, inp : char const * _i, outp :
        char * _o);";

   "f::actor" = "outp = sobelInit(inp);";
  }
```

## 5. Allow parameter defaults

Allow default parameter definitions in the `parameter` and `actortype` blocks,
so that they can be omitted from the `actor` block.

Example:

```
parameter {
    width: u32 = 100;
    height: u32;
}

...

actortype sobel {
    ...
    param width;
    param height = 100;
    ...
}

...

actor sobel {
    type: sobel;
    // ... ports

    // missing `param` definition:
    // width is provided by `parameter` block,
    // height is provided by actortype block.
}
```

## 6. Expression deduction and intermediary parameters

Often, there is a relationship between parameters and port rates. In the Sobel
example, the rate of the ports equals `width * height`. To preserve code reuse,
it would be useful to define parameters as arithmetic expressions of the graph
parameters.

Allowing for temporary symbols in the `parameter`, `actortype` and `actor`
blocks could also be helpful.

Example:

```
actortype sobel {
    input inp: u8;
    output outp: u8;
    param width: u32, height:u32;
    ...
    consumption inp: (width*height);
    production outp: (width*heigth);
}
```

or 

```
parameter {
    witdh: u32 = 100;
    height: u32 = 100;
    img_size = width * height; // local parameter, not part of graph interface.
                               // Type is inferred as u32
}

actortype sobel {
    input inp: u8;
    output outp: u8;
    ...
    consumption inp: img_size;
    production outp: img_size;
}
```
