#!/usr/bin/bash

# Sets up a new environment from scratch (clones LLVM and Polygeist, sets up env.sh, etc.)

# Will create the folders in the current directory. I suggest you run it from `dffmlir/..` , so that the other repos will be separate.

PROJECTS_DIR=$(pwd)
LLVM_COMMIT=00a12585933ef63ff1204bf5cd265f0071d04642
POLYGEIST_COMMIT=e703db13174c60d590885ecc4b1a47dcc370c282

cd dfmlir

./generate_env.sh
source env.sh

cd ..

echo Checking for build dependencies.

for command in cmake ninja g++ ; do
  if ! which $command ; then
    echo "$command not found"
    exit 1
  fi
done


if [[ $(basename "$(pwd)") == dfmlir ]];
then
  echo "You should run this from another folder, or else the project folders will be contained within each other. Try the parent folder."
  exit 1
fi

echo Cloning LLVM.

if ! test -d llvm-project; then
  if ! (
  git clone --depth 1 https://github.com/llvm/llvm-project.git
  cd llvm-project
  pwd
  git pull origin main
  git fetch origin $LLVM_COMMIT
  git checkout $LLVM_COMMIT
  mkdir build
  cd build
  pwd
  cmake -GNinja $LLVM_PROJECT/llvm -DLLVM_ENABLE_PROJECTS="mlir;clang" -DLLVM_BUILD_EXAMPLES=ON -DLLVM_TARGETS_TO_BUILD="host;X86" -DCMAKE_BUILD_TYPE=Release -DLLVM_ENABLE_ASSERTIONS=ON -DLLVM_INSTALL_UTILS=ON -DLLVM_ENABLE_RTTI=ON
  ninja
  ) then
    echo Failed to setup LLVM.
    exit 1
  fi
fi

echo Cloning Polygeist.

if ! test -d Polygeist; then
  if ! (
  cd $PROJECTS_DIR
  pwd
  git clone https://github.com/wsmoses/Polygeist.git
  cd Polygeist
  pwd
  git pull origin main
  git fetch origin $POLYGEIST_COMMIT
  git checkout $POLYGEIST_COMMIT

  Link LLVM project to this folder
  rm -rf llvm-project
  ln -s ../llvm-project .
  mkdir build
  cd build
  pwd
  cmake -G Ninja .. -DMLIR_DIR=$LLVM_PROJECT/build/lib/cmake/mlir -DCLANG_DIR=$LLVM_PROJECT/build/lib/cmake/clang -DLLVM_TARGETS_TO_BUILD="host" -DLLVM_ENABLE_ASSERTIONS=ON -DCMAKE_BUILD_TYPE=DEBUG
  ninja
  ) then
    echo Failed to setup Polygeist.
  fi
fi

echo Building IaRa.

cd $PROJECTS_DIR/dfmlir
pwd


cd $DFMLIR_DIR
pwd
mkdir build
cd build
pwd
cmake -GNinja .. -DCMAKE_BUILD_TYPE=Debug -DMLIR_DIR=$DFMLIR_DIR/build-llvm/lib/cmake/mlir -DLLVM_DIR=$DFMLIR_DIR/build-llvm/lib/cmake/llvm
cmake -GNinja .. -DMLIR_DIR=$DFMLIR_DIR/build-llvm/lib/cmake/mlir -DLLVM_DIR=$DFMLIR_DIR/build-llvm/lib/cmake/llvm -DLLVM_EXTERNAL_LIT=$DFMLIR_DIR/build-llvm/bin/llvm-lit && cmake --build .
