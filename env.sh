export DFMLIR_BIN="/home/pferrazo/repos/dfmlir/build/bin/dfmlir"
export POLYGEIST_MLIR_CLANG_BIN="/home/pferrazo/repos/Polygeist/build/bin/cgeist"
export LLVM_PROJECT="/home/pferrazo/repos/llvm-project"
export CLANG_BIN="$LLVM_PROJECT/build/bin/clang"
export DFMLIR_DIR="/home/pferrazo/repos/dfmlir"
export PATH=$PATH:$LLVM_PROJECT/build/bin
export PS1="(dfmlir)$(echo $PS1 | sed 's/(dfmlir)//g') "
