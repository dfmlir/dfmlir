# Helper script that partially translates .pi files into our DIF.

from ast import Str
import math
import xml.etree.ElementTree as ET
import sys
from collections import defaultdict
from pprint import pprint

from requests import delete


def main():
    if len(sys.argv) < 2:
        print("Specify file.")
        exit(1)

    filename = sys.argv[1]

    mytree = ET.parse(filename)

    graph_name = ""
    nodes = {}
    edge_names_by_consumer = {}
    edge_names_by_producer = {}
    edge_types_by_consumer = {}
    edge_types_by_producer = {}
    param_inputs = {}
    params = {}
    input_interfaces = []
    output_interfaces = []
    edge_number = 1
    edges = []
    param_values_defaults = {}
    param_values_expr = {}
    param_values = {}
    port_types = defaultdict(lambda: defaultdict(lambda: "TYPE"))
    param_types = defaultdict(lambda: "TYPE")
    port_rates = {}
    delay_exprs_by_id = {}
    delay_ids_by_edge = {}
    delay_blocks = {}

    for i in mytree.getroot():
        if i.tag.endswith("graph"):
            graph = i

    for i in graph:
        tag = i.tag.split('}')[1]
        if tag == "data":
            if "key" in i.attrib and \
                    i.attrib["key"] == "name":
                graph_name = i.text
        if tag == "node":
            node_id = i.attrib['id']
            for loop in [j for j in i if j.tag.endswith("loop")]:
                if node_id not in port_types:
                    port_types[node_id] = defaultdict(lambda: "TYPE")
                for param in loop:
                    type_name = param.attrib['type'].strip()
                    port_types[node_id][param.attrib['name']
                                        ] = type_name
                    if param.attrib['isConfig'] == "true":
                        param_name = param.attrib['name']
                        if param_name in param_types and param_types[param_name] != type_name:
                            raise Exception(
                                f"Conflicting type for param {param_name}")
                        param_types[param_name] = type_name
            for port in [j for j in i if j.tag.endswith("port") and (j.attrib['kind'] == "input" or j.attrib['kind'] == "output") and "expr" in j.attrib]:
                if node_id not in port_rates:
                    port_rates[node_id] = defaultdict(lambda: "TYPE")
                port_rates[node_id][port.attrib['name']
                                    ] = port.attrib['expr']
            if i.attrib["kind"] == "cfg_in_iface":
                params[i.attrib["id"]] = i
                if "defaultValue" in i.attrib:
                    param_values_defaults[i.attrib["id"]
                                          ] = i.attrib["defaultValue"]
            if i.attrib["kind"] == "param":
                params[i.attrib["id"]] = i
                if "expr" in i.attrib:
                    param_values_expr[i.attrib["id"]] = i.attrib["expr"]
            if i.attrib["kind"] == "actor":
                nodes[i.attrib["id"]] = i
            if i.attrib["kind"] == "src":
                input_interfaces.append(i)
            if i.attrib["kind"] == "snk":
                output_interfaces.append(i)
            if i.attrib["kind"] == "broadcast":
                nodes[i.attrib["id"]] = i
            if i.attrib["kind"] == "delay":
                delay_exprs_by_id[i.attrib["id"]] = i.attrib["expr"]
        if tag == "edge":
            if i.attrib["kind"] == "fifo":
                for j in i:
                    j_tag = j.tag.split('}')[1]
                    if j_tag == "data" and j.attrib["key"] == "delay":
                        delay_ids_by_edge[i] = j.text
                edges.append(i)
            if i.attrib["kind"] == "dependency":
                if i.attrib["target"] not in param_inputs:
                    param_inputs[i.attrib["target"]] = []
                param_inputs[i.attrib["target"]].append(i)

    # eval params

    resolving = {}

    def _if(a, b, c):
        return b if a else c

    def pow_div_max(x, y):
        power = 0
        while y % x == 0:
            y /= x
            power += 1
        return power

# adapted from https://github.com/preesm/preesm/blob/3512ca2bbd123b640741cfd4dc1b3fc8a84c101a/plugins/org.preesm.commons/src/org/preesm/commons/math/functions/GeometricSum.java
    def geo_sum(a, r, i):
        if r == -1:
            if i % 2 == 0:
                return 0
            return a
        elif r == 1:
            return a * i
        elif r == 0:
            return 0
        elif r > 1 or r < -1:
            return (a - a * math.pow(r, i))/(i-r)
        sum = a
        reduceda = a*r
        while i > 1:
            sum += reduceda
            i -= 1
            reduceda * - r
        return sum

    def treat_expr(expr: str):
        return expr.replace('if(', '_if(').replace('^', '**')

    local = {"_if": _if, "sqrt": math.sqrt,
             "floor": math.floor, "ceil": math.ceil, "pow_div_max": pow_div_max,
             "geo_sum": geo_sum,
             "ln": math.log}

    def resolve_param(id):
        if id in resolving:
            raise Exception("Recursion error")
        if id in param_values:
            return param_values[id]
        if id in param_values_defaults:
            x = int(param_values_defaults[id])
            param_values[id] = x
            return x
        if id in param_values_expr:
            try:
                expr = treat_expr(param_values_expr[id])
                x = eval(expr, param_values, local)
                param_values[id] = x
                return x
            except NameError as e:
                resolving[id] = True
                unknown_id = str(e).split("'")[1]
                error = False
                try:
                    resolve_param(unknown_id)
                except Exception as e:
                    error = e
                finally:
                    del resolving[id]
                if error:
                    raise error
                else:
                    return resolve_param(id)
        raise Exception(f"Name not found: {id}")

    def resolve_param_or_placeholder(id):
        try:
            return resolve_param(id)
        except Exception as e:
            return "VALUE"

    for i in param_values_defaults.keys():
        resolve_param_or_placeholder(i)

    for i in param_values_expr.keys():
        resolve_param_or_placeholder(i)

    # eval rates

    for node, v in port_rates.items():
        for port, expr in v.items():
            try:
                x = int(eval(treat_expr(expr), param_values, local))
            except Exception as e:
                x = "RATE"
            v[port] = x

    for e in edges:
        if e.attrib["target"] not in edge_names_by_consumer:
            edge_names_by_consumer[e.attrib["target"]] = {}
            edge_types_by_consumer[e.attrib["target"]] = {}
        if e.attrib["source"] not in edge_names_by_producer:
            edge_names_by_producer[e.attrib["source"]] = {}
            edge_types_by_producer[e.attrib["source"]] = {}
        type_name = e.attrib['type'].strip()
        edge_names_by_consumer[e.attrib["target"]
                               ][e.attrib["targetport"]] = f"e{edge_number}"
        edge_names_by_producer[e.attrib["source"]
                               ][e.attrib["sourceport"]] = f"e{edge_number}"
        edge_types_by_consumer[e.attrib["target"]
                               ][e.attrib["targetport"]] = type_name
        edge_types_by_producer[e.attrib["source"]
                               ][e.attrib["sourceport"]] = type_name
        if e in delay_ids_by_edge:
            id = delay_ids_by_edge[e]
            expr = delay_exprs_by_id[id]
            try:
                rate = int(eval(treat_expr(expr), param_values, local))
            except Exception as e:
                rate = "RATE"
            delay_blocks[f"e{edge_number}"] = {"type": type_name, "rate": rate}
        edge_number += 1

    print(f"dif {graph_name} {{")
    print("  parameter {")
    for k in params.keys():
        print(
            f"    {k} : {param_types[k]} = {resolve_param_or_placeholder(k)};", end='')
        if k in param_values_expr:
            print(f" // {param_values_expr[k]}")
        else:
            print("")
    print("  }")
    print("  interface {")
    for i in input_interfaces:
        print(f"    consumption {i.attrib['id']};")
    for i in output_interfaces:
        print(f"    production {i.attrib['id']};")
    print("  }")

    print("  delay {")
    for edge_name, delay_info in delay_blocks.items():
        print(f"    {edge_name} = {delay_info['rate']};")
    print("  }")

    node_number = 1

    for k, v in nodes.items():
        if v.attrib["kind"] != "broadcast":
            print(f"  actortype {k} {{")
        else:
            print(f"  actortype {k}_BROADCAST {{")

        for p in [p for p in v if p.tag.endswith("port") and
                  p.attrib["kind"] == "cfg_input" and
                  v.attrib["kind"] != "broadcast"]:
            print(f"    param {p.attrib['name']};")

        for p in [p for p in v if p.tag.endswith("port") and
                  p.attrib["kind"] == "input"]:
            port_name = p.attrib['name']
            print(
                f"    consumption {port_name}: {edge_types_by_consumer[k][port_name]} = {port_rates[k][port_name]};")

        for p in [p for p in v if p.tag.endswith("port") and
                  p.attrib["kind"] == "output"]:
            port_name = p.attrib['name']
            print(
                f"    production {port_name}: {edge_types_by_producer[k][port_name]} = {port_rates[k][port_name]};")

        print("  }")
        print(f"  actor n{node_number}_{k} {{")
        node_number += 1
        if v.attrib["kind"] != "broadcast":
            print(f"    type: {k};")
        else:
            print(f"    type: {k}_BROADCAST;")
        for p in [p for p in v if p.tag.endswith("port") and
                  p.attrib["kind"] == "input"]:
            print(
                f"    interface {edge_names_by_consumer[k][p.attrib['name']]} -> {p.attrib['name']};")
        for p in [p for p in v if p.tag.endswith("port") and
                  p.attrib["kind"] == "output"]:
            print(
                f"    interface {p.attrib['name']} -> {edge_names_by_producer[k][p.attrib['name']]};")
        print("  }")

    print("}")


if __name__ == "__main__":
    main()
